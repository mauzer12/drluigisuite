﻿using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using Microsoft.Practices.Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public class MaterialOrderViewModel : ViewModel, IMaterialOrderViewModel
    {
        private readonly IDialogService dialogService;
        private readonly IEventAggregator eventAggregator;

        public MaterialOrderViewModel(IDialogService dialogService, IEventAggregator eventAggregator)
        {
            this.dialogService = dialogService;
            this.eventAggregator = eventAggregator;

            // On material offer selected event get all material offers
            eventAggregator.GetEvent<AddedToBasket>().Subscribe(OnMaterialOfferSelected);
        }

        private void OnMaterialOfferSelected(CratItem item)
        {
            if (item != null)
                InBasketCollection.Add(item);
        }

        public ObservableCollection<CratItem> InBasketCollection
        {
            get { return GetValue(() => InBasketCollection); }
            set { SetValue(value, () => InBasketCollection); }
        }

        public CratItem SelectedItem
        {
            get { return GetValue(() => SelectedItem); }
            set { SetValue(value, () => SelectedItem); }
        }

        public bool IsBusy
        {
            get { return GetValue(() => IsBusy); }
            set { SetValue(value, () => IsBusy); }
        }

        private ICommand clearOrdersCommand;
        public ICommand ClearOrdersCommand
        {
            get
            {
                if (clearOrdersCommand == null)
                {
                    clearOrdersCommand = new RelayCommand(
                        () => InBasketCollection.Clear(),
                        () => InBasketCollection != null && InBasketCollection.Any());
                }

                return clearOrdersCommand;
            }
        }

        private ICommand placeOrderCommand;
        public ICommand PlaceOrderCommand
        {
            get
            {
                if (placeOrderCommand == null)
                {
                    placeOrderCommand = new RelayCommand(
                        () => placeOrederExecute(),
                        () => InBasketCollection != null && InBasketCollection.Any());
                }

                return placeOrderCommand;
            }
        }

        private ICommand removeCartItemCommand;
        public ICommand RemoveCartItemCommand
        {
            get
            {
                if (removeCartItemCommand == null)
                {
                    removeCartItemCommand = new RelayCommand(
                        () => InBasketCollection.Remove(SelectedItem),
                        () => SelectedItem != null);
                }

                return removeCartItemCommand;
            }
        }

        private void placeOrederExecute()
        {
            dialogService.ShowMessageBox("Narudžba je uspješno postavljena!", "Narudžba", 
                System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
        }

        public override void Dispose()
        {
            eventAggregator.GetEvent<AddedToBasket>().Unsubscribe(OnMaterialOfferSelected);

            base.Dispose();
        }
    }
}
