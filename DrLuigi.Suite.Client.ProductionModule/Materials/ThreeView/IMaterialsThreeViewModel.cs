﻿using DrLuigi.Suite.Domain;
using System.Windows.Input;
using Telerik.Windows.Data;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public interface IMaterialsThreeViewModel 
    {
        QueryableCollectionView MaterialsView { get; set; }
        Material SelectedMaterial { get; set; }
        string SearchPhrase { get; set; }
        bool IsBusy { get; set; }
        ICommand NewMaterialCommand { get; }
        ICommand EditMaterialCommand { get; }
        ICommand RemoveMaterialCommand { get; }
        ICommand ReloadMaterialsCommand { get; }
    }
}
