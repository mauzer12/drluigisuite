﻿using DrLuigi.Suite.DAL;
using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using Microsoft.Practices.Prism.Events;
using System.Threading.Tasks;
using System.Windows.Input;
using Telerik.Windows.Data;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public class MaterialsThreeViewModel : ViewModel, IMaterialsThreeViewModel
    {
        private readonly IRepository repository;
        private readonly IDialogService dialogService;
        private readonly IEventAggregator eventAggregator;

        public MaterialsThreeViewModel(IRepository repo, IDialogService dialogService, IEventAggregator eventAggregator,
            IMaterialOffersViewModel materialOfferVM, IMaterialOrderViewModel materialOrderVM)
        {
            this.repository = repo;
            this.dialogService = dialogService;
            this.eventAggregator = eventAggregator;

            loadMaterials();

            MaterialOfferVM = materialOfferVM;
            MaterialOrderVM = materialOrderVM;
        }

        public override string Title
        {
            get
            {
                return "Proizvodnja >> Materijali";
            }
        }

        public QueryableCollectionView MaterialsView
        {
            get { return GetValue(() => MaterialsView); }
            set { SetValue(value, () => MaterialsView); }
        }

        public IMaterialOffersViewModel MaterialOfferVM
        {
            get { return GetValue(() => MaterialOfferVM); }
            set { SetValue(value, () => MaterialOfferVM); }
        }

        public IMaterialOrderViewModel MaterialOrderVM
        {
            get { return GetValue(() => MaterialOrderVM); }
            set { SetValue(value, () => MaterialOrderVM); }
        }

        public Material SelectedMaterial
        {
            get { return GetValue(() => SelectedMaterial); }
            set
            {
                SetValue(value, () => SelectedMaterial,
                    OnChanged: () =>
                    {
                        // let the world now that material is selected.
                        eventAggregator.GetEvent<MaterialSelected>().Publish(value);
                    });
            }
        }

        public string SearchPhrase
        {
            get { return GetValue(() => SearchPhrase); }
            set
            {
                SetValue(value, () => SearchPhrase, 
                    OnChanged:() => OnSearchPhraseChanged());
            }
        }

        protected void OnSearchPhraseChanged()
        {
            MaterialsView.FilterDescriptors.Clear();

            if (!string.IsNullOrWhiteSpace(SearchPhrase))
            {
                MaterialsView.FilterDescriptors.Add(new FilterDescriptor("Name", FilterOperator.Contains, SearchPhrase));
            }
        }

        public bool IsBusy
        {
            get { return GetValue(() => IsBusy); }
            set { SetValue(value, () => IsBusy); }
        }

        private ICommand newMaterialCommand;
        public ICommand NewMaterialCommand
        {
            get
            {
                if (newMaterialCommand == null)
                {
                    newMaterialCommand = new RelayCommand(() => newMaterialExecute());
                }

                return newMaterialCommand;
            }
        }

        public ICommand editMaterialCommand;
        public ICommand EditMaterialCommand
        {
            get
            {
                if (editMaterialCommand == null)
                {
                    editMaterialCommand = new RelayCommand(
                        () => editMaterialExecute(),
                        () => SelectedMaterial != null);
                }

                return editMaterialCommand;
            }
        }

        private ICommand removeMaterialCommand;
        public ICommand RemoveMaterialCommand
        {
            get
            {
                if (removeMaterialCommand == null)
                {
                    removeMaterialCommand = new RelayCommand(
                        () => removeMaterialExecute(),
                        () => SelectedMaterial != null);
                }

                return removeMaterialCommand;
            }
        }

        private ICommand reloadMaterialsCommand;
        public ICommand ReloadMaterialsCommand
        {
            get
            {
                if (reloadMaterialsCommand == null)
                {
                    reloadMaterialsCommand = new RelayCommand(
                        () => loadMaterials(),
                        () => !IsBusy);
                }

                return reloadMaterialsCommand;
            }
        }

        private void loadMaterials()
        {
            IsBusy = true;

            var materials = Task.Factory.StartNew(() =>
            {
                return repository.GetAll<Material>();
            }).Result;

            MaterialsView = new QueryableCollectionView(materials);
            
            IsBusy = false;
        }
        
        private void newMaterialExecute()
        {
            NavigationParameters p = new NavigationParameters
            {
                {"Repository", repository }
            };

            dialogService.ShowDialogView<IMaterialEditViewModel>(
                "Novi materijal", "default", p,
                onDialogClosing: (dialog, resultVM, e) =>
                {
                    e.Cancel = true;
                    if (dialog.DialogResult.HasValue && dialog.DialogResult.Value)
                    {
                        try
                        {
                            repository.Add<Material>(resultVM.Material);
                            SelectedMaterial = resultVM.Material;
                            e.Cancel = false;
                        }
                        catch (TransactionFailedException)
                        {
                            // report concurrency problem
                            System.Windows.MessageBoxResult result = dialogService.ShowMessageBox("Nastao je problem pri dodavanju materijala u bazu. Postoji mogućnost da je isti materijal u međuvremenu dodao neki drugi korisnik ili ista već postoji u bazi", "Greška",
                                System.Windows.MessageBoxButton.OKCancel, System.Windows.MessageBoxImage.Error);

                            if(result == System.Windows.MessageBoxResult.Cancel)
                                e.Cancel = false;
                        }
                    }
                    else if (dialog.DialogResult.HasValue && !dialog.DialogResult.Value)
                        e.Cancel = false;
                });
        }

        private void editMaterialExecute()
        {
            NavigationParameters p = new NavigationParameters
            {
                {"Material", SelectedMaterial },
                {"Repository", repository }
            };

            dialogService.ShowDialogView<IMaterialEditViewModel>(
                "Uredi materijal", "default", p,
                onDialogClosing: (dialog, resultVM, e) =>
                {
                    e.Cancel = true;
                    if (dialog.DialogResult.HasValue && dialog.DialogResult.Value)
                    {
                        try
                        {
                            repository.Update<Material>(resultVM.Material);
                            e.Cancel = false;
                        }
                        catch (TransactionFailedException)
                        {
                            // report concurrency problem
                            System.Windows.MessageBoxResult result = dialogService.ShowMessageBox("Nastao je problem pri ažuriranju materijala u bazi. Postoji mogućnost da je istu materijal u međuvremenu ažurirao drugi korisnik.", "Greška",
                                System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);

                            if (result == System.Windows.MessageBoxResult.Cancel)
                                e.Cancel = false;
                        }
                    }
                    else if (dialog.DialogResult.HasValue && !dialog.DialogResult.Value)
                        e.Cancel = false;
                });
        }

        private void removeMaterialExecute()
        {
            try
            {
                SelectedMaterial.SetGroup(null);
                SelectedMaterial.SetColor(null);
                SelectedMaterial.SetMeasurmentUnit(null);
                repository.Remove<Material>(SelectedMaterial);
                loadMaterials();
            }
            catch (TransactionFailedException)
            {
                // report concurrency problem
                dialogService.ShowMessageBox("Nastao je problem pri brisanju materiala iz baze. Postoji mogućnost da je materijal u međuvremenu obrisao neki drugi korisnik ili materijal ne postoji u bazi. Savjet: osvježite prikaz podataka.", "Greška",
                    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
        }
    }
}
