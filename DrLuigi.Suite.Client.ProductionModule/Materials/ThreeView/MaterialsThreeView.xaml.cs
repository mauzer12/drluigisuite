﻿using System.Windows.Controls;

namespace DrLuigi.Suite.Client.ProductionModule
{
    /// <summary>
    /// Interaction logic for MaterialsThreeView.xaml
    /// </summary>
    public partial class MaterialsThreeView : UserControl
    {
        public MaterialsThreeView()
        {
            InitializeComponent();
        }

        public MaterialsThreeView(IMaterialsThreeViewModel vm)
            :this()
        {
            this.DataContext = vm;
        }
    }
}
