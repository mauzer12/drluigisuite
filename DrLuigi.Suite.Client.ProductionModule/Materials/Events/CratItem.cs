﻿using DrLuigi.Suite.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public class CratItem
    {
        public Material Material { get; set; }
        public Partner Supplier { get; set; }
        public decimal Amount { get; set; }
        public decimal Price { get; set; }
    }
}
