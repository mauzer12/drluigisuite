﻿using DrLuigi.Suite.Domain;
using Microsoft.Practices.Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Client.ProductionModule
{
    internal class MaterialSelected : CompositePresentationEvent<Material>
    {
    }
}
