﻿using System.Windows.Controls;

namespace DrLuigi.Suite.Client.ProductionModule
{
    /// <summary>
    /// Interaction logic for MaterialEditView.xaml
    /// </summary>
    public partial class MaterialEditView : UserControl
    {
        public MaterialEditView()
        {
            InitializeComponent();

            this.Loaded += (s, e) =>
                MaterialNameBox.Focus();
        }
    }
}
