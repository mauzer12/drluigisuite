﻿using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public interface IMaterialEditViewModel : IViewModel, IDialogAware
    {
        Material Material { get; }
        ICommand NewCategoryCommand { get; }
        ICommand NewGroupCommand { get; }
    }
}
