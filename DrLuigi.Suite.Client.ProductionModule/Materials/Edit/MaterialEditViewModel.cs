﻿using DrLuigi.Suite.DAL;
using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public class MaterialEditViewModel : ViewModel, IMaterialEditViewModel
    {
        private readonly IDialogService dialogService;
        private IRepository repository;

        public MaterialEditViewModel(Material material)
        {
            Material = material;
        }

        [InjectionConstructor]
        public MaterialEditViewModel(IDialogService dialogService)
            :this(new Material { MeasurmentUnit = null, Color = null, Group = null})
        {
            this.dialogService = dialogService;
            //this.repository = repo;
        }

        public bool IsLoading
        {
            get { return GetValue(() => IsLoading); }
            set { SetValue(value, () => IsLoading); }
        }

        public Material Material { get; private set; }

        public ObservableCollection<MeasurmentUnit> MeasurmentUnits
        {
            get { return GetValue(() => MeasurmentUnits); }
            set { SetValue(value, () => MeasurmentUnits); }
        }

        public MeasurmentUnit SelectedMeasurmentUnit
        {
            get { return GetValue(() => SelectedMeasurmentUnit, Material.MeasurmentUnit); }
            set
            {
                SetValue(value, () => SelectedMeasurmentUnit,
                    OnChanged: () => Material.SetMeasurmentUnit(SelectedMeasurmentUnit));
            }
        }

        public ObservableCollection<MaterialCategory> Categories
        {
            get { return GetValue(() => Categories); }
            set { SetValue(value, () => Categories); }
        }

        public MaterialCategory SelectedCategory
        {
            get { return GetValue(() => SelectedCategory, Material.Category); }
            set
            {
                SetValue(value, () => SelectedCategory,
                    OnChanged: () => 
                    {
                        loadMaterialGroups();
                    });
            }
        } 

        public ObservableCollection<MaterialGroup> Groups
        {
            get { return GetValue(() => Groups); }
            set { SetValue(value, () => Groups); }
        }

        public MaterialGroup SelectedGroup
        {
            get { return GetValue(() => SelectedGroup, Material.Group); }
            set
            {
                SetValue(value, () => SelectedGroup,
                    OnChanged: () =>
                    {
                        if(SelectedGroup != null)
                            Material.Group = SelectedGroup;
                    });
            }
        }

        public ObservableCollection<MaterialKind> Kinds
        {
            get { return GetValue(() => Kinds); }
            set { SetValue(value, () => Kinds); }
        }

        public MaterialKind SelectedKind
        {
            get { return GetValue(() => SelectedKind, Material.Kind); }
            set { SetValue(value, () => SelectedKind); }
        }

        public ObservableCollection<Color> Colors
        {
            get { return GetValue(() => Colors); }
            set { SetValue(value, () => Colors); }
        }

        public bool IsComposite
        {
            get { return GetValue(() => IsComposite, isComposite()); }
            set
            {
                SetValue(value, () => IsComposite, 
                    OnChanged:() =>Components = new ObservableCollection<Material>(Material.ComposedOf));
            }
        }

        public ObservableCollection<Material> Components
        {
            get { return GetValue(() => Components); }
            set { SetValue(value, () => Components); }
        }

        private ICommand newCategoryCommand;
        public ICommand NewCategoryCommand
        {
            get
            {
                if(newCategoryCommand == null)
                {
                    newCategoryCommand = new RelayCommand(() => newCategoryExecute());
                }

                return newCategoryCommand;
            }
        }

        private ICommand newGroupCommand;
        public ICommand NewGroupCommand
        {
            get
            {
                if (newGroupCommand == null)
                {
                    newGroupCommand = new RelayCommand(() => newGroupExecute());
                }

                return newGroupCommand;
            }
        }

        private ICommand newKindCommand;
        public ICommand NewKindCommand
        {
            get
            {
                if (newKindCommand == null)
                {
                    newKindCommand = new RelayCommand(() => newKindExecute());
                }

                return newKindCommand;
            }
        }

        private ICommand removeComponentCommand;
        public ICommand RemoveComponentCommand
        {
            get
            {
                if (removeComponentCommand == null)
                {
                    removeComponentCommand = new RelayCommand<Material>((c) => removeComponentExecute(c));
                }

                return removeComponentCommand;
            }
        }

        public void OnDialogInitialized(NavigationParameters p)
        {
            if(p.Keys.Contains("Material"))
                Material = p["Material"] as Material;
            if (p.Keys.Contains("Repository"))
                this.repository = p["Repository"] as IRepository;

            loadMeasurmentUnits();
            loadMaterialCategories();
            loadMaterialGroups();
            loadColors();
            loadMaterialKinds();
        }

        private bool isComposite()
        {
            return Material.ComposedOf.Count() > 1;
        }

        public event EventHandler<DialogEventArgs> PendingClose;

        private void newCategoryExecute()
        {
            throw new NotImplementedException();
        }

        private void newGroupExecute()
        {
            throw new NotImplementedException();
        }

        private void newKindExecute()
        {
            throw new NotImplementedException();
        }

        private void removeComponentExecute(Material c)
        {
            throw new NotImplementedException();
        }

        private void loadMeasurmentUnits()
        {
            var mUnits = Task.Factory.StartNew(() =>
            {
                return repository.GetAll<MeasurmentUnit>().ToList();
            }).Result;

            MeasurmentUnits = new ObservableCollection<MeasurmentUnit>(mUnits);
        }

        private void loadMaterialCategories()
        {
            var categories = Task.Factory.StartNew(() =>
            {
                return repository.GetAll<MaterialCategory>().ToList();
            }).Result;

            Categories = new ObservableCollection<MaterialCategory>(categories);
        }

        private void loadMaterialGroups()
        {
            var groups = Task.Factory.StartNew(() =>
            {
                return repository.GetAll<MaterialGroup>().Where(x => x.Category == SelectedCategory).ToList();
            }).Result;

            Groups = new ObservableCollection<MaterialGroup>(groups);

            if (!Groups.Contains(SelectedGroup))
                SelectedGroup = null;
        }

        private void loadMaterialKinds()
        {
            var kinds = Task.Factory.StartNew(() =>
            {
                return repository.GetAll<MaterialKind>().ToList();
            }).Result;

            Kinds = new ObservableCollection<MaterialKind>(kinds);
        }

        private void loadColors()
        {
            var colors = Task.Factory.StartNew(() =>
            {
                return repository.GetAll<Color>().ToList();
            }).Result;

            Colors = new ObservableCollection<Color>(colors);
        }
    }
}
