﻿using DrLuigi.Suite.DAL;
using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using Microsoft.Practices.Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public class MaterialOffersViewModel : ViewModel, IMaterialOffersViewModel
    {
        private readonly IRepository repository;
        private readonly IEventAggregator eventAggregator;

        public MaterialOffersViewModel(IRepository repo, IEventAggregator eventAggregator)
        {
            this.repository = repo;
            this.eventAggregator = eventAggregator;

            // On material selection event get all material offers
            eventAggregator.GetEvent<MaterialSelected>().Subscribe(OnMaterialSelected);
        }

        private void OnMaterialSelected(Material material)
        {
            if (material == null)
            {
                OfferCollection.Clear();
                return;
            }
            

            IsBusy = true;
            
            var offers = Task.Factory.StartNew(() =>
            {
                return repository.GetAll<ReceiptItem>()
                    .Where(x => x.Material.Code == material.Code)
                    .OrderByDescending(x => x.Receipt.CreationDate)
                    .ThenBy(x => x.UnitPriceKN * (1 - x.Rebate/100))
                    .ToList();
            }).Result;

            OfferCollection = new ObservableCollection<ReceiptItem>(offers);

            IsBusy = false;
        }

        public ObservableCollection<ReceiptItem> OfferCollection
        {
            get { return GetValue(() => OfferCollection); }
            set { SetValue(value, () => OfferCollection); }
        }

        public ReceiptItem SelectedOffer
        {
            get { return GetValue(() => SelectedOffer); }
            set { SetValue(value, () => SelectedOffer); }
        }

        public decimal Amount
        {
            get { return GetValue(() => Amount); }
            set { SetValue(value, () => Amount); }
        }

        public bool IsBusy
        {
            get { return GetValue(() => IsBusy); }
            set { SetValue(value, () => IsBusy); }
        }

        private ICommand addToBasketCommand;
        public ICommand AddToBasketCommand
        {
            get
            {
                if (addToBasketCommand == null)
                {
                    addToBasketCommand = new RelayCommand(
                        () => addToBasketExecute(),
                        () => true);
                }

                return addToBasketCommand;
            }
        }

        private void addToBasketExecute()
        {
            CratItem item = new CratItem
            {
                Material = SelectedOffer.Material,
                Supplier = SelectedOffer.Receipt.Partner,
                Amount = 1,
                Price = SelectedOffer.UnitPriceKN * Amount
            };

            eventAggregator.GetEvent<AddedToBasket>().Publish(item);
        }

        public override void Dispose()
        {
            eventAggregator.GetEvent<MaterialSelected>().Unsubscribe(OnMaterialSelected);

            base.Dispose();
        }
    }
}
