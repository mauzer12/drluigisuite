﻿using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public interface ISupplierDetailsViewModel : IViewModel, IDialogAware
    {
        Partner Supplier { get; set; }
        ICommand ShowLocationCommand { get; }
    }
}
