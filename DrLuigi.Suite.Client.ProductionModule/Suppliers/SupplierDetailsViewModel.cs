﻿using DrLuigi.Suite.Infrastructure;
using System;
using System.Linq;
using System.Text;
using DrLuigi.Suite.Domain;
using System.Windows.Input;
using Microsoft.Practices.Unity;
using DrLuigi.Suite.Client.Common.Shared;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public class SupplierDetailsViewModel : ViewModel, ISupplierDetailsViewModel
    {
        private readonly IDialogService dialogService;

        public SupplierDetailsViewModel(Partner supplier)
        {
            Supplier = supplier;
        }

        [InjectionConstructor]
        public SupplierDetailsViewModel(IDialogService dialogService)
        {
            this.dialogService = dialogService;
        }

        public Partner Supplier
        {
            get { return GetValue(() => Supplier); }
            set { SetValue(value, () => Supplier); }
        }

        /// <summary>
        /// Gets partner full address in format of '[Address Line], [City] ([PostNumber]), [Country]'
        /// where each part is exclusive. Addreass is optional.
        /// </summary>
        public string FullAddress
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                if (!string.IsNullOrEmpty(Supplier.AddressLine))
                    sb.Append(Supplier.AddressLine);

                if (Supplier.City != null)
                {
                    if (!string.IsNullOrWhiteSpace(Supplier.City.Name))
                        sb.Append(", " + Supplier.City.Name);

                    if (!string.IsNullOrWhiteSpace(Supplier.City.PostNumber))
                        sb.Append(" (" + Supplier.City.PostNumber + ")");

                    if (Supplier.City != null && Supplier.City.Country != null && !string.IsNullOrWhiteSpace(Supplier.City.Name))
                        sb.Append(", " + Supplier.City.Country.Name);
                }

                return sb.ToString();
            }
        }

        public bool WorkWith { get; private set; }
        
        public event EventHandler<DialogEventArgs> PendingClose;

        private ICommand showLocationCommand;
        public ICommand ShowLocationCommand
        {
            get
            {
                if (showLocationCommand == null)
                {
                    showLocationCommand = new RelayCommand(
                        () => showLocationExecute(), 
                        () => Supplier.Longitude != null && Supplier.Latitude != null );
                    //|| !string.IsNullOrEmpty(Supplier.AddressLine) || !string.IsNullOrEmpty(Supplier.City.Name)
                }

                return showLocationCommand;
            }
        }

        public bool IsLoading { get; set; }

        private void showLocationExecute()
        {
            if(Supplier.Latitude != null && Supplier.Longitude != null)
            {
                NavigationParameters p = new NavigationParameters
                {
                    { "Location", Telerik.Windows.Controls.Map.Location.Parse("42.6957539183824, 23.3327663758679")}
                };

                // TODO: Try to resolve position according to addres line | city | country and take care on
                //       internet connection exception occuriance.
                dialogService.ShowDialogView<ILocationViewModel>("Lokacija", "default", p);
            }
            
        }

        public void OnDialogInitialized(NavigationParameters p)
        {
            Supplier = p["Supplier"] as Partner;

            if (p.Keys.Contains("WorkWith"))
                WorkWith = (bool)p["WorkWith"];
        }
    }
}
