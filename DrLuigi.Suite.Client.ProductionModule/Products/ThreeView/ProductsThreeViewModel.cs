﻿using DrLuigi.Suite.DAL;
using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public class ProductsThreeViewModel : ViewModel, IProductsThreeViewModel
    {
        private readonly IRepository repository;
        private readonly IDialogService dialogService;

        public ProductsThreeViewModel(IRepository repo, IDialogService dialogService)
        {
            this.repository = repo;
            this.dialogService = dialogService;

            loadProducts();
        }

        public override string Title
        {
            get
            {
                return "Proizvodnja >> Artikli";
            }
        }

        public ObservableCollection<Product> ProductsCollection
        {
            get { return GetValue(() => ProductsCollection); }
            set { SetValue(value, () => ProductsCollection); }
        }

        public Product SelectedProduct
        {
            get { return GetValue(() => SelectedProduct); }
            set { SetValue(value, () => SelectedProduct); }
        }

        public bool IsBusy
        {
            get { return GetValue(() => IsBusy); }
            set { SetValue(value, () => IsBusy); }
        }

        private ICommand newProductCommand;
        public ICommand NewProductCommand
        {
            get
            {
                if (newProductCommand == null)
                {
                    newProductCommand = new RelayCommand(() => newProductExecute());
                }

                return newProductCommand;
            }
        }

        private ICommand editProductCommand;
        public ICommand EditProductCommand
        {
            get
            {
                if (editProductCommand == null)
                {
                    editProductCommand = new RelayCommand(
                        () => editProductExecute(),
                        () => SelectedProduct != null);
                }

                return editProductCommand;
            }
        }

        private ICommand removeProductCommand;
        public ICommand RemoveProductCommand
        {
            get
            {
                if (removeProductCommand == null)
                {
                    removeProductCommand = new RelayCommand(
                        () => removeProductExecute(),
                        () => SelectedProduct != null);
                }

                return removeProductCommand;
            }
        }

        private ICommand reloadProductsCommand;
        public ICommand ReloadProductsCommand
        {
            get
            {
                if (reloadProductsCommand == null)
                {
                    reloadProductsCommand = new RelayCommand(
                        () => loadProducts(),
                        () => !IsBusy);
                }

                return reloadProductsCommand;
            }
        }

        private void loadProducts()
        {
            IsBusy = true;

            var products = Task.Factory.StartNew(() =>
            {
                return repository.GetAll<Product>().ToList();
            }).Result;

            ProductsCollection = new ObservableCollection<Product>(products);

            IsBusy = false;
        }

        private void newProductExecute()
        {
            dialogService.ShowDialogView<IProductEditViewModel>(
                "Novi artikal", "default", null,
                onDialogClosing: (dialog, resultVM, e) =>
                {
                    if (dialog.DialogResult.HasValue && dialog.DialogResult.Value)
                    {
                        try
                        {
                            repository.Add<Product>(resultVM.Product);
                            ProductsCollection.Add(resultVM.Product);
                            SelectedProduct = resultVM.Product;
                        }
                        catch (TransactionFailedException)
                        {
                            // report concurrency problem
                            dialogService.ShowMessageBox("Nastao je problem pri dodavanju artikla u bazu. Postoji mogućnost da je isti artikal u međuvremenu dodao neki drugi korisnik ili isti već postoji u bazi", "Greška",
                                System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                        }
                    }
                });
        }

        private void editProductExecute()
        {
            NavigationParameters p = new NavigationParameters
            {
                {"Product", SelectedProduct }
            };

            dialogService.ShowDialogView<IProductEditViewModel>(
                "Uredi artikal", "default", p,
                onDialogClosing: (dialog, resultVM, e) =>
                {
                    if (dialog.DialogResult.HasValue && dialog.DialogResult.Value)
                    {
                        try
                        {
                            repository.Update<Product>(resultVM.Product);
                        }
                        catch(TransactionFailedException)
                        {
                            // report concurrency problem
                            dialogService.ShowMessageBox("Nastao je problem pri ažuriranju artikla u bazi. Postoji mogućnost da je isti artikal u međuvremenu ažurirao drugi korisnik.", "Greška",
                                System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                        }
                    }
                });
        }

        private void removeProductExecute()
        {
            try
            {
                repository.Remove<Product>(SelectedProduct);
                ProductsCollection.Remove(SelectedProduct);
            }
            catch (TransactionFailedException)
            {
                // report concurrency problem
                dialogService.ShowMessageBox("Nastao je problem pri brisanju artikla iz baze. Postoji mogućnost da je artikal u međuvremenu obrisao neki drugi korisnik ili artikal ne postoji u bazi. Savjet: osvježite prikaz podataka.", "Greška",
                    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
        }
    }
}
