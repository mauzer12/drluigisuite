﻿using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public interface IProductsThreeViewModel : IViewModel
    {
        ObservableCollection<Product> ProductsCollection { get; set; }
        Product SelectedProduct { get; set; }
        bool IsBusy { get; set; }
        ICommand NewProductCommand { get; }
        ICommand EditProductCommand { get; }
        ICommand RemoveProductCommand { get; }
        ICommand ReloadProductsCommand { get; }
    }
}
