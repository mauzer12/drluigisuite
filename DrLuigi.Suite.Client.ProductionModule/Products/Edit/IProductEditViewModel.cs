﻿using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public interface IProductEditViewModel : IViewModel, IDialogAware
    {
        Product Product { get; set; }
    }
}
