﻿using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using Microsoft.Practices.Unity;
using System;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public class ProductEditViewModel : ViewModel, IProductEditViewModel
    {
        private readonly IDialogService dialogService;

        protected ProductEditViewModel(Product product)
        {
            Product = product;
        }

        [InjectionConstructor]
        public ProductEditViewModel(IDialogService dialogService)
            :this(new Product())
        {
            this.dialogService = dialogService;
        }

        public Product Product
        {
            get { return GetValue(() => Product); }
            set { SetValue(value, () => Product); }
        }

        public void OnDialogInitialized(NavigationParameters p)
        {
            Product = p["Product"] as Product;
        }

        public bool IsLoading
        {
            get { return GetValue(() => IsLoading); }
            set { SetValue(value, () => IsLoading); }
        }

        public event EventHandler<DialogEventArgs> PendingClose;
    }
}
