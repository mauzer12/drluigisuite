﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public class SupplierTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string[] parts = value.ToString().Split(new string[] { "#:#" }, StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length > 2)
                return "";          // Special case - split sequence used twice - 
                                    //do not throw exception just ignore and let user report problem.

            if (parameter.ToString() == "0")
            {
                return parts[0];
            }
            else if (parameter.ToString() == "1")
            {
                return parts[1];
            }
            else
                throw new InvalidOperationException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
