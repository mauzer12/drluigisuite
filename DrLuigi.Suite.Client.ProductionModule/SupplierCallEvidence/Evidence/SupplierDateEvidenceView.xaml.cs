﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.ScheduleView;

namespace DrLuigi.Suite.Client.ProductionModule
{
    /// <summary>
    /// Interaction logic for SupplierDateEvidenceView.xaml
    /// </summary>
    public partial class SupplierDateEvidenceView : UserControl
    {
        private ScrollBar mainScrollBar = null;
        private ScrollBar mapScrollBar = null;

        public SupplierDateEvidenceView()
        {
            InitializeComponent();
            this.Loaded += MaterialPurchasePlaner_Loaded;

            MainSchedule.AppointmentCreated += MainSchedule_AppointmentCreated;
            MainSchedule.AppointmentEdited += MainSchedule_AppointmentEdited;
            MainSchedule.AppointmentDeleted += MainSchedule_AppointmentDeleted;
        }

        public SupplierDateEvidenceView(ISuplierDateEvidenceViewModel vm)
            : this()
        {
            this.DataContext = vm;
        }

        private void MaterialPurchasePlaner_Loaded(object sender, RoutedEventArgs e)
        {
            mainScrollBar = MainSchedule.ChildrenOfType<ScrollBar>()
                    .Where(x => x.Orientation == Orientation.Vertical).ToArray()[0]; // set index to 1 in case of visible RadScheduleView navigation header.
            mapScrollBar = MinimapSchedule.ChildrenOfType<ScrollBar>()
                    .Where(x => x.Orientation == Orientation.Vertical).ToArray()[0];

            if (mainScrollBar != null && mapScrollBar != null)
                mainScrollBar.ValueChanged += MainScrollBar_ValueChanged;
        }

        private void MainScrollBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double rPos = e.NewValue / mainScrollBar.ActualHeight;
            mapScrollBar.Value = mapScrollBar.ActualHeight * rPos;
        }

        private void MainSchedule_AppointmentCreated(object sender, AppointmentCreatedEventArgs e)
        {
            var viewModel = (sender as RadScheduleView).DataContext as ISuplierDateEvidenceViewModel;
            viewModel.SaveAppointmentCommand.Execute(e.CreatedAppointment as Appointment);
        }

        private void MainSchedule_AppointmentEdited(object sender, AppointmentEditedEventArgs e)
        {
            var viewModel = (sender as RadScheduleView).DataContext as ISuplierDateEvidenceViewModel;
            viewModel.SaveAppointmentCommand.Execute(e.Appointment as Appointment);
        }

        private void MainSchedule_AppointmentDeleted(object sender, AppointmentDeletedEventArgs e)
        {
            var viewModel = (sender as RadScheduleView).DataContext as ISuplierDateEvidenceViewModel;
            viewModel.RemoveAppointmentCommand.Execute(e.Appointment as Appointment);
        }

        private void TimeBar_VisiblePeriodChanged(object sender, RadRoutedEventArgs e)
        {
            if (this.TimeBar != null)
            {
                DateTime roundedVisiblePeriodStart = this.TimeBar.VisiblePeriodStart.Date;
                DateTime roundedVisiblePeriodEnd = this.TimeBar.VisiblePeriodEnd.Date;

                if (roundedVisiblePeriodStart != this.TimeBar.VisiblePeriodStart)
                {
                    this.TimeBar.VisiblePeriodStart = roundedVisiblePeriodStart;
                }
                if (roundedVisiblePeriodEnd != this.TimeBar.VisiblePeriodEnd)
                {
                    this.TimeBar.VisiblePeriodEnd = roundedVisiblePeriodEnd;
                }

                this.UpdateMinimap();
            }
        }

        private void UpdateMinimap()
        {
            int visibleDays = (int)this.TimeBar.VisiblePeriodEnd.Date.Subtract(this.TimeBar.VisiblePeriodStart.Date).TotalDays;

            this.MinimapSchedule.ActiveViewDefinition.VisibleDays = visibleDays;
        }

        private void UserControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string tag = (sender as UserControl).Tag.ToString();

            if (!string.IsNullOrEmpty(tag))
            {
                var vm = this.DataContext as ISuplierDateEvidenceViewModel;
                vm.SupplierDetailsCommand.Execute(tag);
            }
        }

        private void RadMenuItem_Click(object sender, RadRoutedEventArgs e)
        {
            string tag = (sender as RadMenuItem).Tag.ToString();

            if (!string.IsNullOrEmpty(tag))
            {
                var vm = this.DataContext as ISuplierDateEvidenceViewModel;
                vm.SupplierDetailsCommand.Execute(tag);
            }
        }
    }
}
