﻿using DrLuigi.Suite.Infrastructure;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Telerik.Windows.Controls.ScheduleView;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public interface ISuplierDateEvidenceViewModel : IViewModel
    {
        ObservableCollection<Appointment> Appointments { get; set; }
        Appointment SelectedAppointment { get; set; }
        ObservableCollection<Appointment> MinimapAppointments { get; set; }
        ICommand SaveAppointmentCommand { get; }
        ICommand RemoveAppointmentCommand { get; }
        ICommand SupplierDetailsCommand { get; }
    }
}
