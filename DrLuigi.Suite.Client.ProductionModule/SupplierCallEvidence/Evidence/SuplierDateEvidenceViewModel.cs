﻿using DrLuigi.Suite.DAL;
using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.ScheduleView;

namespace DrLuigi.Suite.Client.ProductionModule
{
    public class SuplierDateEvidenceViewModel : ViewModel, ISuplierDateEvidenceViewModel
    {
        private readonly IRepository repository;
        private readonly IDialogService dialogService;

        public SuplierDateEvidenceViewModel(IRepository repo, IDialogService dialogService)
        {
            this.repository = repo;
            this.dialogService = dialogService;

            loadResourceTypes();
            loadAppointments();
        }

        public override string Title
        {
            get
            {
                return "Evidencija dobavljača";
            }
        }

        public ObservableCollection<Appointment> Appointments
        {
            get { return GetValue(() => Appointments); }
            set
            {
                SetValue(value, () => Appointments,
                    OnChanged: () => SetValue(value, () => MinimapAppointments));
            }
        }

        public Appointment SelectedAppointment
        {
            get { return GetValue(() => SelectedAppointment); }
            set
            {
                SetValue(value, () => SelectedAppointment,
                    OnChanged: () =>
                     {
                         if (SelectedAppointment != null)
                         {
                             if (SelectedAppointment.Category == null)
                             {
                                 SelectedAppointment.Category = this.Categories.First();
                             }
                             if (SelectedAppointment.TimeMarker == null)
                             {
                                 SelectedAppointment.TimeMarker = this.TimeMarkers.First();
                             }
                         }
                     });
            }
        }

        public ObservableCollection<Appointment> MinimapAppointments
        {
            get { return GetValue(() => MinimapAppointments); }
            set { SetValue(value, () => MinimapAppointments); }
        }

        public ObservableCollection<ResourceType> ResourceTypes
        {
            get { return GetValue(() => ResourceTypes); }
            set { SetValue(value, () => ResourceTypes); }
        }

        private List<string> partnerFilter = new List<string>
        {
            "- Svi -", "S kojima poslujemo", "S kojima ne poslujemo"
        };

        public List<string> PartnerFilters
        {
            get { return partnerFilter; }
        }

        public string SelectedPartnerFilter
        {
            get { return GetValue(() => SelectedPartnerFilter, "- Svi -"); }
            set
            {
                SetValue(value, () => SelectedPartnerFilter, 
                    OnChanged:() => {
                        loadResourceTypes();
                    });
            }
        }

        private TimeMarkerCollection timeMarkes;
        public TimeMarkerCollection TimeMarkers // nije potrebno -> postavlja se na apointmentima kao povlaka i warming
        {
            get
            {
                if (timeMarkes == null)
                {
                    this.timeMarkes = new TimeMarkerCollection();
                    this.timeMarkes.Add(new TimeMarker("Normal", new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Gray)));
                    this.timeMarkes.Add(new TimeMarker("High", new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Red)));
                }

                return timeMarkes;
            }
        }

        private CategoryCollection categories;
        public CategoryCollection Categories
        {
            get
            {
                if (categories == null)
                {
                    this.categories = new CategoryCollection();
                    this.Categories.Add(new Category("Drugo", new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.LightGray)));
                    this.Categories.Add(new Category("Poziv obavljen", new System.Windows.Media.SolidColorBrush(new System.Windows.Media.Color { R = 0x30, G = 0x9B, B = 0x46, A = 0xFF })));
                    this.Categories.Add(new Category("Poziv primljen", new System.Windows.Media.SolidColorBrush(new System.Windows.Media.Color { R = 0x00, G = 0xFF, B = 0x00, A = 0xFF })));
                    this.Categories.Add(new Category("Nedostupan", new System.Windows.Media.SolidColorBrush(new System.Windows.Media.Color { R = 0xFF, G = 0x66, B = 0x00, A = 0xFF })));
                    this.Categories.Add(new Category("Isporuka", new System.Windows.Media.SolidColorBrush(new System.Windows.Media.Color { R = 0x25, G = 0xA0, B = 0xDA, A = 0xFF })));
                    this.Categories.Add(new Category("Uzorci", new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 126, 81, 161))));
                    this.Categories.Add(new Category("Povrat", new System.Windows.Media.SolidColorBrush(new System.Windows.Media.Color { R = 0xD4, G = 0x00, B = 0xAA, A = 0xFF })));
                }

                return categories;
            }
        }

        public bool IsBusy
        {
            get { return GetValue(() => IsBusy); }
            set { SetValue(value, () => IsBusy); }
        }

        private ICommand saveAppointmentCommand;
        private ICommand removeAppointmentCommand;

        public ICommand SaveAppointmentCommand
        {
            get
            {
                if (saveAppointmentCommand == null)
                {
                    saveAppointmentCommand = new RelayCommand<Appointment>((app) => saveAppointmentExecute(app));
                }

                return saveAppointmentCommand;
            }
        }

        public ICommand RemoveAppointmentCommand
        {
            get
            {
                if (removeAppointmentCommand == null)
                {
                    removeAppointmentCommand = new RelayCommand<Appointment>((app) => RemoveAppointmentExecute(app));
                }

                return removeAppointmentCommand;
            }
        }

        private ICommand supplierDetailsCommand;
        public ICommand SupplierDetailsCommand
        {
            get
            {
                if (supplierDetailsCommand == null)
                {
                    supplierDetailsCommand = new RelayCommand<string>((s) => supplierDetailsExecute(s));
                }

                return supplierDetailsCommand;
            }
        }

        private void supplierDetailsExecute(string partnerName)
        {
            Partner selectedPartner = repository.GetAll<Partner>().Where(x => x.Name == partnerName).FirstOrDefault();
            bool workWith = repository.GetAll<Receipt>().Where(x => x.Partner == selectedPartner).Any();

            NavigationParameters p = new NavigationParameters
            {
                {"Supplier", selectedPartner},
                {"WorkWith", workWith }
            };

            dialogService.ShowDialogView<ISupplierDetailsViewModel>("Detalji dobavljača", "default", p, null);
        }

        private void saveAppointmentExecute(Appointment app)
        {
            AppointmentEntry entry = repository.GetAll<AppointmentEntry>()
                .Where(x => x.Id == app.UniqueId).FirstOrDefault();

            if (entry != null)
            {
                AppointmentEntry oldEntry = entry; // keep copy of old value just in case we need rollback.

                entry.Subject = app.Subject;
                entry.Body = app.Body;
                entry.StartDate = app.Start;
                entry.EndDate = app.End;
                entry.IsImportant = (app.Importance == Importance.High) ? true : false;
                entry.Category = (AppointmentCategories)Categories.IndexOf(app.Category);

                try
                {
                    repository.Update<AppointmentEntry>(entry);
                }
                catch (TransactionFailedException)
                {
                    // report concurrency problem
                    System.Windows.MessageBoxResult result = dialogService.ShowMessageBox("Nastao je problem pri ažuriranju obavjesti u bazi. Postoji mogućnost da je istu obavjest u međuvremenu ažurirao drugi korisnik.", "Greška",
                        System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);

                    // rollback.
                    app.Subject = oldEntry.Subject;
                    app.Body = oldEntry.Body;
                    app.Start = oldEntry.StartDate;
                    app.End = oldEntry.EndDate;
                    app.Importance = oldEntry.IsImportant ? Importance.High : Importance.Low;
                    if ((int)oldEntry.Category < Categories.Count)
                        app.Category = Categories[(int)oldEntry.Category];
                    
                    // can't rollback category info.
                }
            }
            else
            {
                entry = new AppointmentEntry(app.UniqueId)
                {
                    Subject = app.Subject,
                    Body = app.Body,
                    StartDate = app.Start,
                    EndDate = app.End,
                    IsImportant = (app.Importance == Importance.High) ? true : false,
                    Category = (AppointmentCategories) Categories.IndexOf(app.Category)
                };

                try
                {
                    repository.Add<AppointmentEntry>(entry);
                    SelectedAppointment = app;
                }
                catch (TransactionFailedException)
                {
                    // report concurrency problem
                    System.Windows.MessageBoxResult result = dialogService.ShowMessageBox("Nastao je problem pri dodavanju obavjesti u bazu. Postoji mogućnost da je istu obavjest u međuvremenu dodao neki drugi korisnik ili ista već postoji u bazi", "Greška",
                        System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);

                    Appointments.Remove(app);
                }
            }
        }

        private void RemoveAppointmentExecute(Appointment app)
        {
            AppointmentEntry entry = repository.GetAll<AppointmentEntry>().Where(x => x.Id == app.UniqueId).FirstOrDefault();

            if (entry == null)
                return; // there's nothing to remove from database.

            try
            {
                repository.Remove<AppointmentEntry>(entry);
            }
            catch (TransactionFailedException)
            {
                // report concurrency problem
                dialogService.ShowMessageBox("Nastao je problem pri brisanju obavjesti iz baze. Postoji mogućnost da je obavjest u međuvremenu obrisao neki drugi korisnik ili obavjest ne postoji u bazi.", "Greška",
                    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
        }

        private void loadResourceTypes()
        {
            //IsBusy = true;
            
            var resources = Task.Factory.StartNew(() =>
            {
                IList<Partner> partners = null;
                switch (SelectedPartnerFilter)
                {
                    case "S kojima poslujemo":
                        partners = repository.GetAll<Partner>().Where(p => p.Receipts.Any()).ToList();
                        break;
                    case "S kojima ne poslujemo":
                        partners = repository.GetAll<Partner>().Where(p => !p.Receipts.Any()).ToList();
                        break;
                    case "- Svi -":
                    default:
                        partners = repository.GetAll<Partner>().ToList();
                        break;
                }

                return partners
                .Select(x => new Resource
                {
                    ResourceName = x.Name,
                    DisplayName = x.Name + "#:#" + repository.GetAll<Receipt>(false).Where(r => r.Partner.Id == x.Id).Any(),
                });
            }).Result;

            ResourceType supplierResType = new ResourceType()
            {
                DisplayName = "Supplier",
                Name = "Supplier"
            };

            foreach (var res in resources)
            {
                supplierResType.Resources.Add(res);
            }

            ResourceTypes = new ObservableCollection<ResourceType>{ supplierResType };
            //IsBusy = false;
        }

        private void loadAppointments()
        {
            IsBusy = true;

            var appointments = Task.Factory.StartNew(() =>
            {
                return repository.GetAll<AppointmentEntry>()
                .Select(x =>
                    new Appointment
                    {
                        Subject = x.Subject,
                        Body = x.Body,
                        Start = x.StartDate,
                        End = x.EndDate,
                        Importance = (x.IsImportant) ? Importance.High : Importance.Low
                    }
                );
            }).Result;

            Appointments = new ObservableCollection<Appointment>(appointments);

            IsBusy = false;
        }
    }
}
