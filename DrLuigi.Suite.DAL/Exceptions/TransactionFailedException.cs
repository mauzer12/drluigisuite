﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.DAL
{
    public class TransactionFailedException : Exception
    {
        public TransactionFailedException()
        {
        }

        public TransactionFailedException(string message)
            :base(message)
        {
        }

        public TransactionFailedException(string message, Exception inner)
            :base(message, inner)
        {
        }
    }
}
