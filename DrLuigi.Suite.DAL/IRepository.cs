﻿using System.Linq;

namespace DrLuigi.Suite.DAL
{
    public interface IRepository
    {
        IUnitOfWork UoW { get; }
        TEntity Get<TEntity>(object id, bool stateless = false) where TEntity : class;
        IQueryable<TEntity> GetAll<TEntity>(bool stateless = false) where TEntity : class;
        void Add<TEntity>(TEntity entity, bool stateless = false) where TEntity : class;
        void Update<TEntity>(TEntity entity, bool stateless = false) where TEntity : class;
        void Remove<TEntity>(TEntity entity, bool stateless = false) where TEntity : class;
    }
}
