﻿using DrLuigi.Suite.Client.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DrLuigi.Services.Authentication
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISHA256AuthenticationService" in both code and config file together.
    [ServiceContract]
    public interface IAuthenticationService
    {
        [OperationContract]
        bool IsUsernameUnique(string username);

        [OperationContract]
        UserInfo SigninUser(string username, string password);

        [OperationContract]
        void SignoutUser(string username);
    }
}
