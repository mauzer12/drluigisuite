﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using DrLuigi.Suite.Client.Authentication;
using DrLuigi.Suite.DAL;
using DrLuigi.Suite.Domain;
using System.Security.Cryptography;
using DrLuigi.Suite.DAL.NHibernate;

namespace DrLuigi.Services.Authentication
{
    public class SHA256AuthenticationService : IAuthenticationService
    {
        private readonly IRepository repository;
        public SHA256AuthenticationService()
        {
            FactoryBuilder factoryBuilder = new FactoryBuilder("LocalConn", false);
            SessionManager sessionManager = new SessionManager(factoryBuilder);
            UnitOfWork uow = new UnitOfWork(sessionManager);
            this.repository = new Repository(uow);
        }

        public UserInfo SigninUser(string username, string password)
        {
            var user = repository.GetAll<User>().Where(x => x.Username == username).FirstOrDefault();

            if (user == null)
                throw new Exception("Invalid username or password provided."); // no such username.


            string userPasswd = user.Password;
            byte[] enteredPasswd = Encoding.ASCII.GetBytes(password);
            byte[] salt = Encoding.ASCII.GetBytes(user.Salt);

            if (getHash(enteredPasswd, salt) == userPasswd)
            {
                // update user and save changes.
                user.LastSignIn = DateTime.Now;
                user.SignedIn = true;
                repository.Update<User>(user);

                // TODO: Save session in protected mem.

                Console.WriteLine("User " + username + " successfully logedin");

                return (UserInfo)user;
            }
            else
                throw new Exception("Invalid username or password provided."); // no such username.
        }

        public void SignoutUser(string username)
        {
            var user = repository.GetAll<User>().Where(x => x.Username == username).FirstOrDefault();

            if (user == null)
                return;

            user.SignedIn = false;
            repository.Update<User>(user);
        }

        /// <summary>
        /// Chack is username already exists in user database.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public bool IsUsernameUnique(string username)
        {
            return !repository.GetAll<User>().Where(x => x.Username == username).Any();
        }

        /// <summary>
        /// Helper methord responsible for plaintext encription.
        /// </summary>
        /// <param name="plaintext"></param>
        /// <param name="salt"></param>
        /// <returns>Returns SHA256(SHA256(plaintext) + salt)</returns>
        private string getHash(byte[] plaintext, byte[] salt)
        {
            // hash plaintext
            HashAlgorithm hashAlgorithm = new SHA256Managed();
            byte[] innerHash = hashAlgorithm.ComputeHash(plaintext);


            // salt plaintext
            byte[] saltedInnerHash = new byte[innerHash.Length + salt.Length];
            Buffer.BlockCopy(innerHash, 0, saltedInnerHash, 0, innerHash.Length);
            Buffer.BlockCopy(salt, 0, saltedInnerHash, innerHash.Length, salt.Length);


            // hash saltedInnerHash
            byte[] outerHash = hashAlgorithm.ComputeHash(saltedInnerHash);


            return Encoding.ASCII.GetString(innerHash);
        }

        /// <summary>
        /// Helper method that simply create rand paswd salt.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private byte[] generateSalt(int length)
        {
            byte[] salt = new byte[length];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(salt);
            }

            return salt;
        }
    }
}
