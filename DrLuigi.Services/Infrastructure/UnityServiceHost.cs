﻿using Microsoft.Practices.Unity;
using System;
using System.ServiceModel;

namespace DrLuigi.Services
{
    public class UnityServiceHost : ServiceHost
    {
        private IUnityContainer container;

        public UnityServiceHost(IUnityContainer container, Type serviceType, params Uri[] baseAddresses) 
            : base(serviceType, baseAddresses) 
        {
            this.container = container;
        }

        protected override void OnOpening()
        {
            if (this.Description.Behaviors.Find<UnityServiceBehavior>() == null)
            {
                this.Description.Behaviors.Add(new UnityServiceBehavior(this.container));
            }

            base.OnOpening();
        }
    }
}
