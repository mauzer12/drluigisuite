﻿using Microsoft.Practices.Unity;
using System;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace DrLuigi.Services
{
    public class UnityServiceHostFactory : ServiceHostFactory
    {
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            IUnityContainer container = new UnityContainer();
            UnityServiceHost serviceHost = new UnityServiceHost(container, serviceType, baseAddresses);

            // Configure container.
            //container.RegisterType<ITestProvider, TestProvider>();

            return serviceHost;
        }
    }
}
