﻿using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Client.GeneralModule
{
    public interface ICompanyEditViewModel : IViewModel, IDialogAware
    {
        ObservableCollection<City> Cities { get; }
        City SelectedCity { get; }
        ObservableCollection<Country> Countries { get; }
        Country SelectedCountry { get; }
        Company Company { get; }
    }
}
