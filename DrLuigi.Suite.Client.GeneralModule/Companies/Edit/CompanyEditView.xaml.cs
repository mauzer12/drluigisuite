﻿using System.Windows.Controls;

namespace DrLuigi.Suite.Client.GeneralModule
{
    /// <summary>
    /// Interaction logic for CompanyEditView.xaml
    /// </summary>
    public partial class CompanyEditView : UserControl
    {
        public CompanyEditView()
        {
            InitializeComponent();

            this.Loaded += (s, e) => 
                CompanyNumberBox.Focus();
        }
    }
}
