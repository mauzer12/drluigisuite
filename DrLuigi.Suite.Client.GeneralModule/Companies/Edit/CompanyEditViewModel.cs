﻿using DrLuigi.Suite.DAL;
using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.GeneralModule
{
    public class CompanyEditViewModel : ViewModel, ICompanyEditViewModel
    {
        private readonly IRepository repository;
        private readonly IDialogService dialogService;

        protected CompanyEditViewModel(Company product)
        {
            Company = product;
        }

        [InjectionConstructor]
        public CompanyEditViewModel(IRepository repo, IDialogService dialogService)
            :this(new Company { CompanyNumber=" "})
        {
            this.repository = repo;
            this.dialogService = dialogService;

            loadCities();
            loadCountries();
        }

        public ObservableCollection<City> Cities
        {
            get { return GetValue(() => Cities); }
            private set { SetValue(value, () => Cities); }
        }

        public City SelectedCity
        {
            get { return GetValue(() => SelectedCity, Company.City); }
            set
            {
                SetValue(value, () => SelectedCity, () =>
                {
                    Company.City = value;

                    //if(value == null && Company)
                    SelectedCountry = value != null ? value.Country : null;

                    if (value != null)
                    {

                    }

                    /*if (value != null && value.Country != null)
                        SelectedCountry = SelectedCity.Country;*/
                });
            }
        }

        public ObservableCollection<Country> Countries
        {
            get { return GetValue(() => Countries); }
            private set { SetValue(value, () => Countries); }
        }

        public Country SelectedCountry
        {
            get
            {
                return GetValue(() => SelectedCountry, 
                    Company.City != null ? Company.City.Country : null);
            }
            set
            {
                SetValue(value, () => SelectedCountry, () =>
                {
                    if (value == null)
                    {
                        
                        // make sure that new country has selected city and if not set city to unknown.
                        /*if(!value.Cities.Where(c => c == SelectedCity).Any())
                            SelectedCity = null;*/
                    }
                });
            }
        }

        public Company Company { get; private set; }

        public bool IsLoading
        {
            get { return GetValue(() => IsLoading); }
            set { SetValue(value, () => IsLoading); }
        }

        public event EventHandler<DialogEventArgs> PendingClose;

        public void OnDialogInitialized(NavigationParameters p)
        {
            Company = p["Company"] as Company;
        }

        private void loadCities()
        {
            var cities = Task.Factory.StartNew(() =>
            {
                return repository.GetAll<City>().ToList();
            }).Result;

            this.Cities = new ObservableCollection<City>(cities);
        }

        private void loadCountries()
        {
            var countries = Task.Factory.StartNew(() =>
            {
                return repository.GetAll<Country>().ToList();
            }).Result;

            this.Countries = new ObservableCollection<Country>(countries);
        }
    }
}
