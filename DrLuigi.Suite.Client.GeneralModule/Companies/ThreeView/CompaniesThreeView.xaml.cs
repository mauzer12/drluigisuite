﻿using System.Windows.Controls;

namespace DrLuigi.Suite.Client.GeneralModule
{
    /// <summary>
    /// Interaction logic for CompanysThreeView.xaml
    /// </summary>
    public partial class CompaniesThreeView : UserControl
    {
        public CompaniesThreeView()
        {
            InitializeComponent();
        }

        public CompaniesThreeView(ICompaniesThreeViewModel vm)
            :this()
        {
            this.DataContext = vm;
        }
    }
}
