﻿using DrLuigi.Suite.DAL;
using DrLuigi.Suite.DAL.NHibernate;
using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Telerik.Windows.Data;

namespace DrLuigi.Suite.Client.GeneralModule
{
    public class CompaniesThreeViewModel : ViewModel, ICompaniesThreeViewModel
    {
        private readonly IRepository repository;
        private readonly IDialogService dialogService;
        //private readonly IClientChannelFactory<TestService.ITestServiceChannel> testProxyFactory;

        public CompaniesThreeViewModel(IRepository repo, IDialogService dialogService)
        {
            this.repository = repo;
            this.dialogService = dialogService;

            // test code ...

            /*TestService.ITestServiceChannel proxy = null;

            try
            {
                proxy = testProxyFactory.CreateChannel();
                System.Windows.MessageBox.Show(proxy.GetData(666));
                proxy.Close();
            }
            catch
            {
                if (proxy != null)
                    proxy.Abort();

                Console.WriteLine("Failed on server communication test.");
            }*/

            loadCompanies();
        }

        public override string Title
        {
            get
            {
                return "Matični podaci >> Tvrtke";
            }
        }

        public ObservableCollection<Company> CompaniesCollection
        {
            get { return GetValue(() => CompaniesCollection); }
            set { SetValue(value, () => CompaniesCollection); }
        }

        public QueryableCollectionView CompaniesView
        {
            get { return GetValue(() => CompaniesView); }
            set { SetValue(value, () => CompaniesView); }
        }

        public Company SelectedCompany
        {
            get { return GetValue(() => SelectedCompany); }
            set { SetValue(value, () => SelectedCompany); }
        }

        public bool IsBusy
        {
            get { return GetValue(() => IsBusy); }
            set { SetValue(value, () => IsBusy); }
        }

        private ICommand newCompanyCommand;
        public ICommand NewCompanyCommand
        {
            get
            {
                if (newCompanyCommand == null)
                {
                    newCompanyCommand = new RelayCommand(() => newCompanyExecute());
                }

                return newCompanyCommand;
            }
        }

        private ICommand editCompanyCommand;
        public ICommand EditCompanyCommand
        {
            get
            {
                if (editCompanyCommand == null)
                {
                    editCompanyCommand = new RelayCommand(
                        () => editCompanyExecute(),
                        () => SelectedCompany != null);
                }

                return editCompanyCommand;
            }
        }

        private ICommand removeCompanyCommand;
        public ICommand RemoveCompanyCommand
        {
            get
            {
                if (removeCompanyCommand == null)
                {
                    removeCompanyCommand = new RelayCommand(
                        () => removeCompanyExecute(),
                        () => SelectedCompany != null);
                }

                return removeCompanyCommand;
            }
        }

        private ICommand reloadCompaniesCommand;
        public ICommand ReloadCompaniesCommand
        {
            get
            {
                if (reloadCompaniesCommand == null)
                {
                    reloadCompaniesCommand = new RelayCommand(
                        () => CompaniesView.Refresh(),
                        () => !IsBusy);
                }

                return reloadCompaniesCommand;
            }
        }

        private void loadCompanies()
        {
            IsBusy = true;

            var companies = Task.Factory.StartNew(() =>
            {
                return repository.GetAll<Company>();
            }).Result;

            CompaniesView = new QueryableCollectionView(companies);

            IsBusy = false;
        }

        private void newCompanyExecute()
        {
            dialogService.ShowDialogView<ICompanyEditViewModel>(
                "Nova tvrtka", "default", null,
                onDialogClosing: (dialog, resultVM, e) =>
                {
                    e.Cancel = true;
                    if (dialog.DialogResult.HasValue && dialog.DialogResult.Value)
                    {
                        resultVM.IsLoading = true;
                        try
                        {
                            repository.Update<Company>(resultVM.Company);
                            SelectedCompany = resultVM.Company;

                            CompaniesView.Refresh();
                            e.Cancel = false;
                        }
                        catch (TransactionFailedException)
                        {
                            // report concurrency problem
                            System.Windows.MessageBoxResult result = dialogService.ShowMessageBox("Nastao je problem pri dodavanju tvrtke u bazu. Postoji mogućnost da je istu tvrtku u međuvremenu dodao neki drugi korisnik ili ista već postoji u bazi", "Greška",
                                System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);

                            if (result == System.Windows.MessageBoxResult.Cancel)
                                e.Cancel = false;
                        }
                        resultVM.IsLoading = false;
                    }
                    else if (dialog.DialogResult.HasValue && !dialog.DialogResult.Value)
                        e.Cancel = false;
                });
        }

        private void editCompanyExecute()
        {
            NavigationParameters p = new NavigationParameters
            {
                {"Company", SelectedCompany }
            };

            dialogService.ShowDialogView<ICompanyEditViewModel>(
                "Uredi tvrtku", "default", p,
                onDialogClosing: (dialog, resultVM, e) =>
                {
                    e.Cancel = true;
                    if (dialog.DialogResult.HasValue && dialog.DialogResult.Value)
                    {
                        resultVM.IsLoading = true;
                        try
                        {
                            repository.Update<Company>(resultVM.Company);
                            e.Cancel = false;
                        }
                        catch (TransactionFailedException)
                        {
                            // report concurrency problem
                            System.Windows.MessageBoxResult result = dialogService.ShowMessageBox("Nastao je problem pri ažuriranju tvrtke u bazi. Postoji mogućnost da je istu tvrtku u međuvremenu ažurirao drugi korisnik.", "Greška",
                                System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);

                            if (result == System.Windows.MessageBoxResult.Cancel)
                                e.Cancel = false;
                        }
                        resultVM.IsLoading = false;
                    }
                    else if (dialog.DialogResult.HasValue && !dialog.DialogResult.Value)
                        e.Cancel = false;
                });
        }

        private void removeCompanyExecute()
        {
            try
            {
                repository.Remove<Company>(SelectedCompany);
                CompaniesView.Refresh();
            }
            catch (TransactionFailedException)
            {
                // report concurrency problem
                dialogService.ShowMessageBox("Nastao je problem pri brisanju tvrtke iz baze. Postoji mogućnost da je tvrtka u međuvremenu obrisao neki drugi korisnik ili tvrtka ne postoji u bazi. Savjet: osvježite prikaz podataka.", "Greška",
                    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
        }
    }
}
