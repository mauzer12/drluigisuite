﻿using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.GeneralModule
{
    public interface ICompaniesThreeViewModel : IViewModel
    {
        ObservableCollection<Company> CompaniesCollection { get; set; }
        Company SelectedCompany { get; set; }
        bool IsBusy { get; set; }
        ICommand NewCompanyCommand { get; }
        ICommand EditCompanyCommand { get; }
        ICommand RemoveCompanyCommand { get; }
        ICommand ReloadCompaniesCommand { get; }
    }
}
