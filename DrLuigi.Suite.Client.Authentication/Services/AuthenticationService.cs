﻿using System;
using System.Linq;
using System.Text;
using DrLuigi.Suite.Domain;
using DrLuigi.Suite.DAL;
using System.Security.Cryptography;
using DrLuigi.Suite.Client.GlobalServices;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.Authentication
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IRepository repository;
        private readonly IEncriptionProvider encriptionProvider;
        private readonly IInternetConnectionService internetService;

        public AuthenticationService(IRepository repo, IEncriptionProvider encriptionProvider, 
                                     IInternetConnectionService internetConnService)
        {
            this.repository = repo;
            this.encriptionProvider = encriptionProvider;
            this.internetService = internetConnService;
        }

        public async Task<UserInfo> AuthenticateUserAsync(string username, string password)
        {
            try
            {
                var user = repository.GetAll<User>().Where(x => x.Username == username).FirstOrDefault();

                if (user == null)
                    throw new InvalidCredentialsException(); // no such username.


                string userPasswd = user.Password;
                byte[] enteredPasswd = Encoding.ASCII.GetBytes(password);
                byte[] salt = Encoding.ASCII.GetBytes(user.Salt);

                if (encriptionProvider.GetHash(enteredPasswd, salt) == userPasswd)
                {
                    // update user and save changes.
                    user.LastSignIn = DateTime.Now;
                    user.SignedIn = true;

                    repository.Update<User>(user);
                    return (UserInfo)user;
                }
                else
                    throw new InvalidCredentialsException(); // invalid username or password.
            }
            catch (Exception ex)
            {
                bool hasConn = await internetService.HasInternetConnectionAsync();

                if (!hasConn)
                    throw new InternetConnectionException();

                throw ex;
            }
        }

        public async Task ChangeUserPasswordAsync(string username, string currentPassword, string newPassword)
        {
            try
            {
                var user = repository.GetAll<User>().Where(x => x.Username == username).FirstOrDefault();

                if (user == null)
                    throw new InvalidCredentialsException(); // no such username

                string userPasswd = user.Password;
                byte[] enteredCurrentPasswd = Encoding.ASCII.GetBytes(currentPassword);
                byte[] enteredNewPassword = Encoding.ASCII.GetBytes(newPassword);
                byte[] salt = Encoding.ASCII.GetBytes(user.Salt);
                byte[] newSalt = generateSalt(32);

                if (encriptionProvider.GetHash(enteredCurrentPasswd, salt) == userPasswd)
                {
                    // generate new salt and finnaly change password.
                    user.Salt = Encoding.ASCII.GetString(newSalt);
                    user.Password = encriptionProvider.GetHash(enteredNewPassword, newSalt);

                    repository.Update<User>(user);
                }
                else
                    throw new InvalidCredentialsException(); // invalid username or password
            }
            catch (Exception ex)
            {
                bool hasConn = await internetService.HasInternetConnectionAsync();

                if (!hasConn)
                    throw new InternetConnectionException();

                throw ex;
            }
        }

        public void SignoutUser(string username)
        {
            try
            {
                var user = repository.GetAll<User>().Where(x => x.Username == username).FirstOrDefault();

                if (user == null)
                    return;

                user.SignedIn = false;
                repository.Update<User>(user);
            }
            catch { } // for now just ignore 
        }

        /// <summary>
        /// Chack is username already exists in user database.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public bool IsUsernameUnique(string username)
        {
            return !repository.GetAll<User>().Where(x => x.Username == username).Any();
        }

        /// <summary>
        /// Helper method that simply create rand paswd salt.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private byte[] generateSalt(int length)
        {
            byte[] salt = new byte[length];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(salt);
            }

            return salt;
        }
    }
}
