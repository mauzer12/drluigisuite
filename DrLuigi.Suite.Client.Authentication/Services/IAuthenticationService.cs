﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrLuigi.Suite.Domain;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.Authentication
{
    public interface IAuthenticationService
    {
        Task<UserInfo> AuthenticateUserAsync(string username, string password);

        Task ChangeUserPasswordAsync(string username, string currentPassword, string newPassword);

        void SignoutUser(string username);

        bool IsUsernameUnique(string username);
    }
}
