﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace DrLuigi.Suite.Client.Authentication
{
    public class SHA256EncriptionProvider : IEncriptionProvider
    {
        /// <summary>
        /// Helper methord responsible for plaintext encription.
        /// </summary>
        /// <param name="plaintext"></param>
        /// <param name="salt"></param>
        /// <returns>Returns SHA256(SHA256(plaintext) + salt)</returns>
        public string GetHash(byte[] plaintext, byte[] salt)
        {
            // hash plaintext
            HashAlgorithm hashAlgorithm = new SHA256Managed();
            byte[] innerHash = hashAlgorithm.ComputeHash(plaintext);


            // salt plaintext
            byte[] saltedInnerHash = new byte[innerHash.Length + salt.Length];
            Buffer.BlockCopy(innerHash, 0, saltedInnerHash, 0, innerHash.Length);
            Buffer.BlockCopy(salt, 0, saltedInnerHash, innerHash.Length, salt.Length);


            // hash saltedInnerHash
            byte[] outerHash = hashAlgorithm.ComputeHash(saltedInnerHash);


            return Encoding.ASCII.GetString(innerHash);
        }
    }
}
