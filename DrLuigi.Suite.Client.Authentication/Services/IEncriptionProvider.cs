﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Client.Authentication
{
    public interface IEncriptionProvider
    {
        string GetHash(byte[] plaintext, byte[] salt);
    }
}
