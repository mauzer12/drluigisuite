﻿namespace DrLuigi.Suite.Client.Authentication
{
    public enum UserState
    {
        Unknown,
        Inactive,
        Signedin,
        Signedout
    }
}
