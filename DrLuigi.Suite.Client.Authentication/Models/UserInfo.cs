﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrLuigi.Suite.Domain;

namespace DrLuigi.Suite.Client.Authentication
{
    /// <summary>
    /// Wrapper around User domain entity class that do not expose password in any form. Applicable for 
    /// display purposes only.
    /// </summary>
    public class UserInfo
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public byte Role { get; set; }
        public byte[] Image { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime? ActivationDate { get; set; }
        public DateTime? LastSignIn { get; set; }
        public bool? Signedin { get; set; }
        public UserState State { get; set; }

        public static implicit operator UserInfo(User acc)
        {
            return new UserInfo
            {
                Id = acc.Id,
                Username = acc.Username,
                Role = acc.Role,
                Image = acc.Image,
                Firstname = acc.Firstname,
                Lastname = acc.Lastname,
                Email = acc.Email,
                Phone = acc.Phone,
                ActivationDate = acc.ActivationDate,
                LastSignIn = acc.LastSignIn,
                Signedin = acc.SignedIn,
                State = (acc.ActivationDate != null) ? 
                (acc.SignedIn.HasValue && acc.SignedIn.Value) ? UserState.Signedin : UserState.Signedout : UserState.Inactive 
            };
        }
    }
}
