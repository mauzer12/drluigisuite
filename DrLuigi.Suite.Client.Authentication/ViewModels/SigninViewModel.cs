﻿using System;
using DrLuigi.Suite.Infrastructure;
using System.Windows.Input;
using DrLuigi.Suite.Client.GlobalServices.AppManagement;
using Microsoft.Practices.Prism.Events;
using System.Collections.Generic;
using DrLuigi.Suite.Client.GlobalServices;

namespace DrLuigi.Suite.Client.Authentication
{
    public class SigninViewModel : ViewModel, ISigninViewModel
    {
        private readonly IAuthenticationService authService;
        private readonly IApplicationService appService;
        private readonly IDialogService dialogService;
        private readonly IEventAggregator eventAggregator;

        private int triesRemaining = 15;

        public SigninViewModel(IAuthenticationService authService, 
                               IApplicationService appService, 
                               IDialogService dialogService,
                               IEventAggregator eventAggregator)
        {
            this.authService = authService;
            this.appService = appService;
            this.dialogService = dialogService;
            this.eventAggregator = eventAggregator;
        }

        public void OnDialogInitialized(NavigationParameters p) { }
        public event EventHandler<DialogEventArgs> PendingClose;

        public bool IsLoading
        {
            get { return GetValue(() => IsLoading); }
            set { SetValue(value, () => IsLoading); }
        }

        public string Username
        {
            get { return GetValue(() => Username); }
            set { SetValue(value, () => Username); }
        }

        public string ErrorMessage
        {
            get { return GetValue(() => ErrorMessage); }
            set { SetValue(value, () => ErrorMessage); }
        }

        // Mocked language collection - for now used only for UI organization purposes.
        private IList<string> languageCollection = new List<string>
        {
            "Hrvatski",
            "Engleski",
            "Njemački",
            "Ruski",
            "Španjolski",
            "Ukrainski",
            "Bugarski"
        };

        public IList<string> LanguageCollection
        {
            get { return languageCollection; }
        }

        public string SelectedLanguage
        {
            get { return GetValue(() => SelectedLanguage, "Hrvatski"); }
            set { SetValue(value, () => SelectedLanguage); }
        }

        private ICommand signinCommand;
        public ICommand SigninCommand
        {
            get 
            {
                if (signinCommand == null)
                {
                    signinCommand = new RelayCommand<System.Windows.Controls.PasswordBox>(
                        (p) => signinExecute(p),
                        (p) => !string.IsNullOrWhiteSpace(Username) && !string.IsNullOrWhiteSpace(p.Password));
                }

                return signinCommand;
            }
        }

        private ICommand exitCommand;
        public ICommand ExitCommand
        {
            get 
            {
                if (exitCommand == null)
                {
                    exitCommand = new RelayCommand(() => exitExecute());
                }

                return exitCommand;
            }
        }

        private void exitExecute()
        {
            appService.ExitApplication();
        }

        private async void signinExecute(System.Windows.Controls.PasswordBox pBox)
        {
            IsLoading = true;

            try
            {
                UserInfo user = await authService.AuthenticateUserAsync(Username, pBox.Password);

                // let the world now user has loged in.
                eventAggregator.GetEvent<UserSignedin>().Publish(user);

                // close dialog manually.
                if (PendingClose != null)
                    PendingClose(this, new DialogEventArgs(true));
            }
            catch (InternetConnectionException)
            {
                dialogService.ShowMessageBox("Niste spojeni na internet!\n\nProvjerite mrežnu opremu i u slučaju da se kvar ne otkloni obavjestite Vašeg internet operatera.",
                        "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
            catch (InvalidCredentialsException)
            {
                if (--triesRemaining <= 0)
                {
                    System.Windows.MessageBox.Show("Aplikacija će se zatvoriti iz sigurnosnih razloga. Kontaktirajte naš tim za podršku korisnicima.",
                        "Try count exceeded", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Exclamation);

                    exitExecute();
                }

                ErrorMessage = "Korisničko ime ili lozinka nisu ispravni!";
            }

            IsLoading = false;
        }
    }
}
