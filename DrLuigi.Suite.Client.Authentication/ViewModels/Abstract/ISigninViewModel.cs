﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrLuigi.Suite.Infrastructure;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.Authentication
{
    public interface ISigninViewModel : IViewModel, IDialogAware
    {
        IList<string> LanguageCollection { get; }
        string SelectedLanguage { get; set; }
        string Username { get; set; }
        ICommand SigninCommand { get; }
        ICommand ExitCommand { get; }
    }
}
