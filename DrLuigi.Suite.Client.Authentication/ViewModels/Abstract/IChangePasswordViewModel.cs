﻿using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.Authentication
{
    public interface IChangePasswordViewModel : IViewModel
    {
        string Username { get; }
        string ErrorMessage {get; set;}
        ICommand ChangePasswordCommand { get; }
    }
}
