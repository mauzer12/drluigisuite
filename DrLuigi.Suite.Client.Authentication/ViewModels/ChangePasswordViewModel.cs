﻿using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.Authentication
{
    public class ChangePasswordViewModel : ViewModel, IChangePasswordViewModel, IDialogAware
    {
        private readonly IAuthenticationService authService;
        public ChangePasswordViewModel(IAuthenticationService authService)
        {
            this.authService = authService;
        }

        public string Username { get; private set; }

        public string ErrorMessage
        {
            get { return GetValue(() => ErrorMessage); }
            set { SetValue(value, () => ErrorMessage); }
        }

        public bool IsLoading
        {
            get { return GetValue(() => IsLoading); }
            set { SetValue(value, () => IsLoading); }
        }

        public event EventHandler<DialogEventArgs> PendingClose;

        private ICommand changePasswordCommand;
        public ICommand ChangePasswordCommand
        {
            get
            {
                if (changePasswordCommand == null)
                {
                    changePasswordCommand = new RelayCommand<Dictionary<string, PasswordBox>>(
                        (d) => changePasswordExecute(d),
                        (d) => changePasswordCanExecute(d));
                }

                return changePasswordCommand;
            }
        }

        private bool changePasswordCanExecute(Dictionary<string, PasswordBox> d)
        {
            return !string.IsNullOrWhiteSpace(d["current"].Password) && !string.IsNullOrWhiteSpace(d["new"].Password) &&
                !string.IsNullOrWhiteSpace(d["re-new"].Password);
        }

        private void changePasswordExecute(Dictionary<string, PasswordBox> d)
        {
            if (d["new"].Password != d["re-new"].Password)
            {
                ErrorMessage = "Ponovno utipkana lozinka nije jednaka novoj lozinci!";
                return;
            }                

            try
            {
                authService.ChangeUserPasswordAsync(Username, d["current"].Password, d["re-new"].Password);

                // close dialog manually.
                if (PendingClose != null)
                    PendingClose(this, new DialogEventArgs(true));
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }
        }

        public void OnDialogInitialized(NavigationParameters p)
        {
            Username = p["Username"].ToString();
        }
    }
}
