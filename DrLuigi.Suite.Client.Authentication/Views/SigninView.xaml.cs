﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DrLuigi.Suite.Client.Authentication
{
    /// <summary>
    /// Interaction logic for SignInView.xaml
    /// </summary>
    public partial class SigninView : UserControl
    {
        public SigninView()
        {
            InitializeComponent();

            this.Loaded += (s, e) =>
                {
                    UsernameBox.Focus();
                    SigninBtn.CommandParameter = PasswdBox;
                };
        }
    }
}
