﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DrLuigi.Suite.Client.Authentication
{
    /// <summary>
    /// Interaction logic for ChangePasswordView.xaml
    /// </summary>
    public partial class ChangePasswordView : UserControl
    {
        public ChangePasswordView()
        {
            InitializeComponent();

            this.Loaded += (s, e) =>
            {
                CurrentPasswdBox.Focus();
                ApplyBtn.CommandParameter = new Dictionary<string, PasswordBox>
                {
                    { "current", CurrentPasswdBox },
                    { "new", NewPasswdBox },
                    { "re-new", NewRePasswdBox }
                };
            };
        }
    }
}
