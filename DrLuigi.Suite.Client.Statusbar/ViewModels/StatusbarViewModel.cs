﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrLuigi.Suite.Infrastructure;

namespace DrLuigi.Suite.Client.Statusbar
{
    public class StatusbarViewModel : ViewModel, IStatusbarViewModel
    {
        public StatusbarViewModel()
        {
        }

        // TODO: Replace hardcoded default values with real connection.
        public string Connection
        {
            get { return GetValue(() => Connection, @"(localDB)\V12 DrLuigiSuite.mdf"); }
            set { SetValue(value, () => Connection); }
        }

        // TODO: Replace hardcoded default values with real app status. 
        public string Status
        {
            get { return GetValue(() => Status, "Spreman"); }
            set { SetValue(value, () => Status); }
        }
    }
}
