﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrLuigi.Suite.Infrastructure;

namespace DrLuigi.Suite.Client.Statusbar
{
    public interface IStatusbarViewModel : IViewModel
    {
        string Connection {get; set;}
        string Status {get; set;}
    }
}
