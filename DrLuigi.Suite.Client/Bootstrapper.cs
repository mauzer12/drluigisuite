﻿using DrLuigi.Suite.Client.GlobalServices.AppManagement;
using DrLuigi.Suite.DAL;
using DrLuigi.Suite.DAL.NHibernate;
using DrLuigi.Suite.Infrastructure;
using HibernatingRhinos.Profiler.Appender.NHibernate;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;
using System;
using System.Windows;

namespace DrLuigi.Suite.Client
{
    /// <summary>
    /// Production app bootstrapper.
    /// </summary>
    public class Bootstrapper : UnityBootstrapper
    {
        public override void Run(bool runWithDefaultConfiguration)
        {
#if DEBUG
            NHibernateProfiler.Initialize();
#endif
            base.Run(runWithDefaultConfiguration);
        }

        protected override IModuleCatalog CreateModuleCatalog()
        {
            return Microsoft.Practices.Prism.Modularity.ModuleCatalog.CreateFromXaml(
                new Uri("ModuleCatalog.xaml", UriKind.Relative));
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();

/*#if DEBUG
            MessageBoxResult qResult = MessageBox.Show("Do you want to recreate database?", "Db", 
                MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);

            if (qResult == MessageBoxResult.Yes)
            {
                // Start db data converter 
                string fileToProcess = Environment.CurrentDirectory + @"\Tools\DrLuigi.Converter.exe";
                Process p = Process.Start(fileToProcess);
                p.WaitForExit();

                p.Exited += (s, e) =>
                {
                    if (p.ExitCode != 0) // TODO: find a way to intercept app exit.
                        MessageBox.Show("Warning", "Conversion exited with error code. Take look at converter inner exception.");
                };
            }
#endif*/

            // DAL registration section.
            this.Container.RegisterInstance<IFactoryBuilder>(new FactoryBuilder("LocalConn"));
            this.Container.RegisterType<ISessionManager, SessionManager>();
            this.Container.RegisterType<IUnitOfWork, UnitOfWork>();
            this.Container.RegisterType<IRepository, Repository>();

            // App lifecycle services registration section.
            this.Container.RegisterInstance<IApplicationService>(new ApplicationService());

            // Dialog service registration section.
            this.Container.RegisterType<IDialogService, DialogService>();
            this.Container.RegisterType<IDialogShell, BasicDialogShell>("default");
            this.Container.RegisterType<IDialogShell, NoCRMDialogShell>("noCRM");

            Container.RegisterType<Func<string, IDialogShell>>
                (
                    new InjectionFactory
                        (
                            (c) => new Func<string, IDialogShell>(name => c.Resolve<IDialogShell>(name))
                        )
                );

            Container.RegisterType<Func<Type, IViewModel>>
                (
                    new InjectionFactory
                        (
                            (c) => new Func<Type, IViewModel>((type) =>
                            {
                                return (IViewModel)c.Resolve(type);
                            })
                        )
                );
        }

        protected override DependencyObject CreateShell()
        {
            return this.Container.Resolve<Shell>();
        }

        protected override void InitializeShell()
        {
            var shell = new Shell();
            
            shell.Loaded += (sender, e) =>
            {
                Telerik.Windows.Controls.Navigation.RadWindowInteropHelper
                    .SetShowInTaskbar(sender as DependencyObject, true);

#if DEBUG
                // Simple way I come around to test is Telerik RadWindow actually application MainWindow.
                // Essentialy RadWindow is not inherited from System.Windows.Window type.
                System.Diagnostics.Debug.Assert(shell.Owner == null,
                    "FAILED: Missmatch occured in App Bootstrapper. Shell is not set as Application.Current.MainWindow");
#endif
            };

            shell.Show();
        }
    }
}
