﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace DrLuigi.Suite.Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly Bootstrapper bootstrapper;

        public App()
        {
            bootstrapper = new Bootstrapper();
            DispatcherUnhandledException += App_DispatcherUnhandledException;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            bootstrapper.Run();

            // TODO: Load culture specifications.
            System.Threading.Thread.CurrentThread.CurrentCulture =
                System.Globalization.CultureInfo.GetCultureInfo("hr-HR");
        }

        private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
#if DEBUG
            if (!System.Diagnostics.Debugger.IsAttached)
            {
                e.Handled = true;

                try
                {
                    string errorMsg = e.Exception.Message + "\n\n" + e.Exception.StackTrace;
                    errorMsg = errorMsg.Replace('"', '\'').Replace("\r\n", @"\n");

                    MessageBox.Show(errorMsg, "Unhandled exception", MessageBoxButton.OK, MessageBoxImage.Error);
                    Environment.Exit(1);
                }
                catch { }
            }
#else
            if (e.Exception == null)
                return;

            // TODO: Log exception to database or exceptionless service.

            Environment.Exit(1);
#endif
        }
    }
}
