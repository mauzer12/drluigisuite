﻿using DrLuigi.Suite.Client.GlobalServices;
using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.Help
{
    public class FeedbackViewModel : ViewModel, IFeedbackViewModel
    {
        private readonly IDialogService dialogService;
        private readonly IMailService mailService;

        public FeedbackViewModel(IDialogService dialogService, IMailService mailService)
        {
            this.dialogService = dialogService;
            this.mailService = mailService;
        }

        public bool IsLoading
        {
            get { return GetValue(() => IsLoading); }
            set { SetValue(value, () => IsLoading); }
        }

        public string LoadingMessage
        {
            get { return "Molimo pričekajte ... Slanje povratne informacije u tijeku."; }
        }

        public void OnDialogInitialized(NavigationParameters p)
        {
            Screenshot = p["Screenshot"] as byte[];
            IncludeScreenshot = (bool) p["IncludeScreenshot"];
        }

        public event EventHandler<DialogEventArgs> PendingClose;

        public string Message
        {
            get { return GetValue(() => Message); }
            set { SetValue(value, () => Message); }
        }

        public bool IncludeScreenshot
        {
            get { return GetValue(() => IncludeScreenshot); }
            set { SetValue(value, () => IncludeScreenshot); }
        }

        public byte[] Screenshot 
        {
            get { return GetValue(() => Screenshot); }
            private set { SetValue(value, () => Screenshot); }
        }

        private ICommand sendFeedbackCommand;
        public ICommand SendFeedbackCommand
        {
            get
            {
                if (sendFeedbackCommand == null)
                {
                    sendFeedbackCommand = new RelayCommand(
                        () => sendFeedbackExecute(),
                        () => !string.IsNullOrWhiteSpace(Message));
                }

                return sendFeedbackCommand;
            }
        }

        private async void sendFeedbackExecute()
        {
            IsLoading = true;

            try
            {
                if (IncludeScreenshot)
                {
                    await mailService.SendMailWithAttachmentAsync("gorankozar@yahoo.com", "gorankozar@ymail.com",
                    "DrLuigi Suite Feedback", Message, Screenshot, "screenshot", MediaTypeNames.Image.Jpeg);
                }
                else
                {
                    await mailService.SendMailAsync("gorankozar@yahoo.com", "gorankozar@ymail.com",
                    "DrLuigi Suite Feedback", Message);
                }

                // close dialog manually.
                if (PendingClose != null)
                    PendingClose(this, new DialogEventArgs(true));
            }
            catch (InternetConnectionException)
            {
                dialogService.ShowMessageBox("Niste spojeni na internet!\n\nProvjerite mrežnu opremu i u slučaju da se kvar ne otkloni obavjestite Vašeg internet operatera.",
                        "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
            catch (Exception)
            {
                dialogService.ShowMessageBox("Povratna informacija nemože biti poslana!\n\nObavjestite korisničku podršku o nastalom problemu.", "Fatal Error",
                    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);

                // TODO: log exception info. This is fatal error.
                
                if (PendingClose != null)
                    PendingClose(this, new DialogEventArgs(false));
            }

            IsLoading = false;
        }
    }
}
