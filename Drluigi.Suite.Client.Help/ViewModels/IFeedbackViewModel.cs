﻿using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.Help
{
    public interface IFeedbackViewModel : IViewModel, IDialogAware
    {
        string Message { get; set; }
        bool IncludeScreenshot { get; set; }
        byte[] Screenshot { get; }

        ICommand SendFeedbackCommand { get; }
    }
}
