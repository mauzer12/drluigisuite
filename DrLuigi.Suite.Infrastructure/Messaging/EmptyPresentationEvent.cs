﻿using Microsoft.Practices.Prism.Events;
using System;
using System.Collections.Generic;

namespace DrLuigi.Suite.Infrastructure
{
    /// <summary>
    /// Wrapper around MS PRISM generic CompositePresentationEvent<object>. There's no class in PRISM library that
    /// provides simple empty event without payload to pablish within event aggregator.
    /// </summary>
    public class EmptyPresentationEvent : EventBase
    {
        /// <summary>
        /// Event which facade is for
        /// </summary>
        private readonly CompositePresentationEvent<object> innerEvent;

        /// <summary>
        /// Dictionary which maps parameterless actions to wrapped 
        /// actions which take the ignored parameter 
        /// </summary>
        private readonly Dictionary<Action, Action<object>> subscriberActions;

        public EmptyPresentationEvent()
        {
            innerEvent = new CompositePresentationEvent<object>();
            subscriberActions = new Dictionary<Action, Action<object>>();
        }

        public void Publish()
        {
            innerEvent.Publish(null);
        }

        public void Subscribe(Action action)
        {
            Action<object> wrappedAction = (o) => action();
            subscriberActions.Add(action, wrappedAction);
            innerEvent.Subscribe(wrappedAction);
        }

        public void Unsubscribe(Action action)
        {
            if (!subscriberActions.ContainsKey(action))
                return;

            var actionToUnsubscribe = subscriberActions[action];
            innerEvent.Unsubscribe(actionToUnsubscribe);
            subscriberActions.Remove(action);
        }
    }
}
