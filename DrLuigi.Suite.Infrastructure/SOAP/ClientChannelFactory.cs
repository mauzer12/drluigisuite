﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Infrastructure
{
    /// <summary>
    /// Wrapper class ensures proper life-cycle handling for IClientChannel 
    /// objects by implementing proper Open, Close and Dispose techniques
    /// based on recommended practices.
    /// </summary>
    /// <typeparam name="TChannel">An interface which defines an ServiceContract</typeparam>
    public class ClientChannelFactory<TChannel> : IClientChannelFactory<TChannel>
        where TChannel : IClientChannel
    {
        private readonly ChannelFactory<TChannel> factory;

        public ClientChannelFactory(string endpointName)
        {
            this.factory =
                new ChannelFactory<TChannel>(endpointName);
        }

        public void Open()
        {
            this.factory.Open();
        }

        public void Close()
        {
            this.factory.Close();
        }

        public void Abort()
        {
            this.factory.Abort();
        }

        public TChannel CreateChannel()
        {
            return this.factory.CreateChannel();
        }

        #region IDisposable Members

        private bool isDisposed = false;
        public void Dispose()
        {
            if (isDisposed)
                throw new ObjectDisposedException("ClientChannelFactory dispose.");

            try
            {
                this.Close();
            }
            finally { isDisposed = true; }
        }

        #endregion
    }
}
