﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Infrastructure
{
    public interface IClientChannelFactory<TChannel> : IDisposable
        where TChannel : IClientChannel
    {
        void Open();
        void Close();
        void Abort();
        TChannel CreateChannel();
    }
}
