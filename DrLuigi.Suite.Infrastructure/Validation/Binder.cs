﻿using System;

namespace DrLuigi.Suite.Infrastructure
{
    /// <summary>
    /// Validation object, responsible to provide validation rules and error message.
    /// </summary>
    public class Binder
    {
        private readonly Func<bool> ruleDelegate;
        private string message;

        internal Binder(Func<bool> ruleDelegate, string message = "")
        {
            this.ruleDelegate = ruleDelegate;
            this.message = message;

            IsDirty = true;
        }

        public string Message
        {
            set { this.message = value; }
        }

        internal string Error { get; private set; }

        internal bool HasError { get; private set; }

        internal bool IsDirty { get; set; }

        internal void Update()
        {
            if (!IsDirty)
                return;

            Error = null;
            HasError = false;
            try
            {
                if (!ruleDelegate())
                {
                    Error = message;
                    HasError = true;
                }
            }
            catch (Exception e)
            {
                Error = e.Message;
                HasError = true;
            }
        }
    }
}
