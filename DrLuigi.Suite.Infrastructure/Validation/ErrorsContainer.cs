﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Infrastructure
{
    public class ErrorsContainer<T>
    {
        private static readonly T[] noErrors = new T[0];
        private readonly Action<string> raiseErrorsChanged;
        private readonly Dictionary<string, List<T>> validationResults;

        /// <summary>
        /// Gets a value indicating whether the object has validation errors. 
        /// </summary>
        public bool HasErrors
        {
            get
            {
                return this.validationResults.Count != 0;
            }
        }

        /// <summary>
        /// Gets value indicating number of properties influenced by error state of object.
        /// </summary>
        public int ErrorsCount
        {
            get
            {
                return this.validationResults.Count;
            }
        }
    }
}
