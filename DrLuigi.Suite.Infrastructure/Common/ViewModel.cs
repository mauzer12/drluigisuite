﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace DrLuigi.Suite.Infrastructure
{
    public abstract class ViewModel : ObservableObject, IDataErrorInfo, IDisposable 
    {
        private Dictionary<string, Binder> ruleMap = new Dictionary<string, Binder>();

        public virtual string Title
        {
            get { return ""; }
        }

        public string Error
        {
            get; private set;
        }

        public bool HasErrors
        {
            get;
        }

        public int ErrorsCount
        {
            get;
        }

        public void AddRule<T>(Expression<Func<T>> expression, Func<bool> ruleDelegate, string errorMessage)
        {
            var name = GetPropertyName(expression);
            if (ruleMap.Keys.Contains(name)) return; ///////////trebalo bi bacati exception uvijek međutim annotation može imati problem sa ovim jer dvije klase mogu imati isti naziv svojstva
            ruleMap.Add(name, new Binder(ruleDelegate, errorMessage));
        }

        public Dictionary<string, List<string>> GetAllErrors()
        {
            return null;
        }

        /*protected ErrorsContainer<string> ErrorsContainer
        {
            get
            {
                if (errorsContainer == null)
                {
                    errorsContainer =
                        new ErrorsContainer<string>(pn => this.OnErrorsChanged(pn));
                }

                return errorsContainer;
            }
        }*/

        protected override bool SetValue<T>(T value, Expression<Func<T>> prop, Action OnChanged = null)
        {
            bool canSetValue = base.SetValue(value, prop, OnChanged);

            if (canSetValue)
            {
                string propertyName = GetPropertyName(prop);

                if (ruleMap.Keys.Contains(propertyName))
                    ruleMap[propertyName].IsDirty = true;
                /*else
                {
                    // add new rule map on property.
                    var propertyInfo = this.GetType().GetProperty(propertyName);

                    // TODO: have data annotations?

                    if (propertyInfo == null)
                        throw new ArgumentException("property info");

                    List<ValidationResult> validationResults = null;
                    Binder b = new Binder(() => TryValidateProperty(propertyInfo, out validationResults));


                    AddRule(prop, b);
                }*/
            }

            return canSetValue;
        }

        protected bool TryValidateProperty(PropertyInfo propertyInfo, out List<ValidationResult> results)
        {
            results = new List<ValidationResult>();
            var context = new ValidationContext(this, null, null) { MemberName = propertyInfo.Name };
            var propertyValue = propertyInfo.GetValue(this, null);

            return Validator.TryValidateProperty(propertyValue, context, results);
        }

        public string this[string columnName]
        {
            get
            {
                if (ruleMap.ContainsKey(columnName))
                {
                    ruleMap[columnName].Update();

                    //var values = ruleMap.Values.ToList();
                    //ErrorCount = values.Where(b => b.HasError).Count();

                    return ruleMap[columnName].Error;
                }

                var propertyInfo = this.GetType().GetProperty(columnName);

                if (propertyInfo == null)
                    throw new ArgumentException("property info");

                List<ValidationResult> validationResults = null;
                TryValidateProperty(propertyInfo, out validationResults);
                


                if (validationResults.Any())
                    return validationResults[0].ErrorMessage;

                return null;
            }
        }

        private bool isDisposed = false;
        public virtual void Dispose()
        {
            if (isDisposed)
                throw new ObjectDisposedException("ViewModel base dispose.");

            try
            {
                // TODO: Control NHibernate session lifetime cycle per form here.
            }
            finally { isDisposed = true; }
        }
    }
}
