﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Infrastructure
{
    public interface IViewModel
    {
        string Title { get; }
        int ErrorsCount { get; }
    }
}
