﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;

namespace DrLuigi.Suite.Infrastructure
{
    public class ObservableObject : INotifyPropertyChanged
    {
        private Dictionary<string, object> properties =
            new Dictionary<string, object>();

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(name));
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        protected virtual T GetValue<T>(Expression<Func<T>> prop, T defaultValue = default(T))
        {
            string name = GetPropertyName(prop);
            if (properties.ContainsKey(name))
            {
                return (T)properties[name];
            }
            else
            {
                properties.Add(name, defaultValue);
                return defaultValue;
            }
        }

        protected virtual bool SetValue<T>(T value, Expression<Func<T>> prop, Action OnChanged = null)
        {
            string name = GetPropertyName(prop);

            // do not trigger PropertyChanged event if not necessary
            var oldValue = GetValue<T>(prop);
            if (!object.Equals(oldValue, value))
            {
                properties[name] = value;

                OnPropertyChanged(new PropertyChangedEventArgs(name));

                // callback
                if (OnChanged != null)
                    OnChanged();

                return true;
            }

            return false;
        }

        protected string GetPropertyName<T>(Expression<Func<T>> expression)
        {
            if (expression == null)
                throw new ArgumentNullException("expression");

            Expression body = expression.Body;
            MemberExpression memberExpression = body as MemberExpression;

            if (memberExpression == null)
            {
                memberExpression = (MemberExpression)((UnaryExpression)body).Operand;
            }

            return memberExpression.Member.Name;
        }
    }
}
