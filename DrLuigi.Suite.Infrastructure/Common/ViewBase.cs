﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace DrLuigi.Suite.Infrastructure
{
    public class ViewBase : UserControl, IViewBase
    {
        public ViewBase()
        {
            this.Loaded += ViewBase_Loaded;
            this.Unloaded += ViewBase_Unloaded;
        }

        private void ViewBase_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            // this adds a form level handler and will listen for each control that has the 
            // NotifyOnValidationError = True and a ValidationError occurs.
            Validation.AddErrorHandler(this, OnExceptionValidationError);
        }

        private void ViewBase_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {
            Validation.RemoveErrorHandler(this, OnExceptionValidationError);
        }

        private void OnExceptionValidationError(object sender, ValidationErrorEventArgs args)
        {
            if (args.Error.RuleInError is ExceptionValidationRule)
            {
                IViewModel vm = this.DataContext as IViewModel;

                if (vm != null)
                {
                    BindingExpression bindingExpression = args.Error.BindingInError as BindingExpression;
                    string dataItemName = bindingExpression.DataItem.ToString();
                    string propertyName = bindingExpression.ParentBinding.Path.Path;

                    StringBuilder sb = new StringBuilder();

                    foreach (ValidationError error in Validation.GetErrors(args.OriginalSource as DependencyObject))
                    {
                        if (error.RuleInError is ExceptionValidationRule)
                        {
                            sb.AppendFormat("{0} has error ", propertyName);

                            if (error.Exception == null || error.Exception.InnerException == null)
                            {
                                sb.AppendLine(error.ErrorContent.ToString());
                            }
                            else
                            {
                                sb.AppendLine(error.Exception.InnerException.Message);
                            }
                        }
                    }

                    /*if (args.Action == ValidationErrorEventAction.Added)
                        vm.AddUIValidationError(string propertyName, sb.ToString());
                    else if (args.Action == ValidationErrorEventAction.Removed)
                        vm.RemoveUIValidationError(string propertyName, sb.ToString());*/
                }
            }
        }
    }
}
