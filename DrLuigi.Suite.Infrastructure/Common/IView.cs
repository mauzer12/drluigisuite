﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DrLuigi.Suite.Infrastructure
{
    public interface IView
    {
        object DataContext { get; set; }
        event RoutedEventHandler Loaded;
    }
}
