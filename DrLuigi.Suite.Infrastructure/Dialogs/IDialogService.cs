﻿using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DrLuigi.Suite.Infrastructure
{
    public interface IDialogService
    {
        MessageBoxResult ShowMessageBox(string message, string title, MessageBoxButton button, MessageBoxImage image);
        void ShowDialogView<TDialogViewModel>(string title, string dialogType, NavigationParameters p = null, Action<IDialogShell, TDialogViewModel, CancelEventArgs> onDialogClosing = null, OwnerEffects effect = OwnerEffects.Blur)
            where TDialogViewModel : IViewModel;
        void ShowDialogView(string title, string dialogType, Action < IDialogShell, CancelEventArgs> onDialogClosing = null, OwnerEffects effect = OwnerEffects.Blur);
        void ShowView<TDialogViewModel>(IDialogShell dialog, TDialogViewModel viewModel);
        string BrowseFile(string filter, string defaultExt = null, bool multiselect = true, string initialDir = null);
    }
}
