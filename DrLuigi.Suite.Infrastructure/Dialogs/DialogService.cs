﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;

namespace DrLuigi.Suite.Infrastructure
{
    public class DialogService : IDialogService
    {
        private readonly Func<string, IDialogShell> dialogFactory;
        private readonly Func<Type, IViewModel> vmFactory;

        public DialogService(Func<string, IDialogShell> dialogFactory, Func<Type, IViewModel> vmFactory)
        {
            this.dialogFactory = dialogFactory;
            this.vmFactory = vmFactory;
        }

        public MessageBoxResult ShowMessageBox(string message, string title, MessageBoxButton button,
            MessageBoxImage image)
        {
            // Show win32 message box.
            return MessageBox.Show(message, title, button, image);
        }

        public void ShowDialogView<TDialogViewModel>(string title, string dialogType, NavigationParameters p = null,
            Action < IDialogShell, TDialogViewModel, CancelEventArgs> onDialogClosing = null, OwnerEffects effect = OwnerEffects.Blur)
            where TDialogViewModel : IViewModel
        {
            // resolve dialog shell and set dialog title.
            IDialogShell dialog = dialogFactory(dialogType);
            dialog.Title = title ?? "Dialog";

            // resolve viewModel and it's dependencies dynamicaly. 
            TDialogViewModel vm = (TDialogViewModel)vmFactory(typeof(TDialogViewModel));

            // in case vm has control over dialog behavior resolve dialog vm dependencies and hook for dialog result.
            if (vm is IDialogAware)
            {
                if (p != null)
                    (vm as IDialogAware).OnDialogInitialized(p);
                
                (vm as IDialogAware).PendingClose += (s, e) =>
                {
                    dialog.DialogResult = e.DialogResult;
                    dialog.Close();
                };
            }

            dialog.Content = vm;

            // set dialog window owner and manage effects.
            (dialog as Window).Owner = getWindowOwner();

            (dialog as Window).Loaded += (s, e) =>
            {
                if ((dialog as Window).Owner != null)
                {
                    applyEffect((dialog as Window).Owner, effect);
                    (s as Window).Closed += (sender, args) =>
                        removeEffect(Application.Current.MainWindow, effect);
                }
                else
                {
                    applyEffect(Application.Current.MainWindow.Content as FrameworkElement, effect);
                    (s as Window).Closed += (sender, args) =>
                        removeEffect(Application.Current.MainWindow.Content as FrameworkElement, effect);
                }
            };

            // setup callback routine and finally diplay dialog.
            if (onDialogClosing != null)
                dialog.Closing += (sender, e) => onDialogClosing(dialog, vm, e);

            dialog.ShowDialog();
        }

        public void ShowDialogView(string title, string dialogType, Action<IDialogShell, CancelEventArgs> onDialogClosing = null,
            OwnerEffects effect = OwnerEffects.Blur)
        {
            IDialogShell dialog = dialogFactory(dialogType);
            dialog.Title = title ?? "Dialog";

            (dialog as Window).Owner = getWindowOwner();

            (dialog as Window).Loaded += (s, e) =>
            {
                if ((dialog as Window).Owner != null)
                {
                    applyEffect((dialog as Window).Owner, effect);
                    (s as Window).Closed += (sender, args) =>
                        removeEffect(Application.Current.MainWindow, effect);
                }
                else
                {
                    applyEffect(Application.Current.MainWindow.Content as FrameworkElement, effect);
                    (s as Window).Closed += (sender, args) =>
                        removeEffect(Application.Current.MainWindow.Content as FrameworkElement, effect);
                }
            };

            if (onDialogClosing != null)
                dialog.Closing += (sender, e) => onDialogClosing(dialog, e);

            dialog.ShowDialog();
        }

        public void ShowView<TDialogViewModel>(IDialogShell dialog, TDialogViewModel viewModel)
        {
            dialog.DataContext = viewModel;
            dialog.Show();
        }

        public string BrowseFile(string filter, string defaultExt = null, bool multiselect = true, string initialDir = null)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // FileDialog setup.
            dlg.DefaultExt = defaultExt;
            dlg.Filter = filter;
            dlg.Multiselect = multiselect;
            dlg.AddExtension = true;
            dlg.CheckFileExists = true;
            dlg.CheckPathExists = true;

            if (!string.IsNullOrWhiteSpace(initialDir))
                dlg.InitialDirectory = initialDir;

            string filePath = null;

            if (dlg.ShowDialog() == true)
            {
                filePath = dlg.FileName;
            }

            return filePath;
        }

        private Window getWindowOwner()
        {
            foreach (Window w in Application.Current.Windows)
            {
                if (w.IsActive)
                    return w;
            }

            return null;
        }

        private void applyEffect(FrameworkElement target, OwnerEffects effect)
        {
            switch (effect)
            {
                case OwnerEffects.Blur:
                    if (target.Visibility == Visibility.Visible)
                    {
                        target.Effect = new System.Windows.Media.Effects.BlurEffect
                        {
                            Radius = 8
                        };
                    }
                    break;

                case OwnerEffects.Hide:
                    target.Visibility = Visibility.Hidden;
                    break;

                case OwnerEffects.None:
                default:
                    break;
            }
        }

        private void removeEffect(FrameworkElement target, OwnerEffects effect)
        {
            switch (effect)
            {
                case OwnerEffects.Blur:
                    target.Effect = null;
                    break;

                case OwnerEffects.Hide:
                    target.Visibility = Visibility.Visible;
                    break;

                case OwnerEffects.None:
                default:
                    break;
            }
        }
    }
}
