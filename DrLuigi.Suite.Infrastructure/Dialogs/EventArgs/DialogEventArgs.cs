﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Infrastructure
{
    public class DialogEventArgs : EventArgs
    {
        public DialogEventArgs(bool? dResult)
        {
            DialogResult = dResult;
        }

        public bool? DialogResult { get; private set; }
    }
}
