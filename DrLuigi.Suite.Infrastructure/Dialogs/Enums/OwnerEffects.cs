﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Infrastructure
{
    public enum OwnerEffects
    {
        None = 0,
        Blur,
        Hide
    }
}
