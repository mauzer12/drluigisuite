﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace DrLuigi.Suite.Infrastructure
{
    public class BasicDialogShell : Window, IDialogShell
    {
        private Image copyright;
        private ContentControl leftCmdPlaceholder;
        private ContentControl rightCmdPlaceholder;
        private FrameworkElement busyIndicator;

        public BasicDialogShell()
        {
            DefaultStyleKey = typeof(BasicDialogShell);
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }

        /*public event CancelEventHandler Closing;

        protected override bool OnClosing()
        {
            if (Closing != null)
                Closing(this, new CancelEventArgs());

            return base.OnClosing();
        }*/

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            leftCmdPlaceholder = GetTemplateChild("PART_LeftCmd") as ContentControl;
            if (leftCmdPlaceholder == null)
                throw new Exception("PART_LeftCmd missing in dialog shell template");

            rightCmdPlaceholder = GetTemplateChild("PART_RightCmd") as ContentControl;
            if (leftCmdPlaceholder == null)
                throw new Exception("PART_RightCmd missing in dialog shell template");

            busyIndicator = GetTemplateChild("PART_BusyIndicator") as FrameworkElement;

            copyright = GetTemplateChild("PART_Copyright") as Image;

            this.Loaded += BasicDialogShell_Loaded;
        }

        private void BasicDialogShell_Loaded(object sender, RoutedEventArgs e)
        {
            // In case view is resolved magicaly from VM
            if (this.Content.GetType().GetInterfaces().Contains(typeof(IViewModel)))
            {
                FrameworkElement el = VisualThreeHelper.FindVisualChildren<FrameworkElement>(this)
                                                       .SkipWhile(x => x.Name != "DialogContent")
                                                       .ElementAtOrDefault(1);

                if (el != null)
                {
                    leftCmdPlaceholder.Content = GetLeftCommands(el);
                    rightCmdPlaceholder.Content = GetRightCommands(el);

                    // TODO: place this code into XAML.
                    if (leftCmdPlaceholder.Content != null)
                        copyright.Visibility = Visibility.Hidden;

                    leftCmdPlaceholder.DataContext = this.Content;
                    rightCmdPlaceholder.DataContext = this.Content;
                }
                else
                    throw new Exception("DialogContent name don't exists in given context.");   // in case somebody mess xaml code.
            }
            else
            {
                Binding leftCmdBinding = new Binding
                {
                    Source = this.Content,
                    Path = new PropertyPath(LeftCommandsProperty)
                };
                Binding rightCmdBinding = new Binding
                {
                    Source = this.Content,
                    Path = new PropertyPath(RightCommandsProperty)
                };

                leftCmdPlaceholder.SetBinding(ContentControl.ContentProperty, leftCmdBinding);
                rightCmdPlaceholder.SetBinding(ContentControl.ContentProperty, rightCmdBinding);
            }
        }

        public static readonly DependencyProperty IsBusyProperty =
            DependencyProperty.Register("IsBusy", typeof(bool), typeof(BasicDialogShell),
                new PropertyMetadata(false, OnIsBusyChanged));

        protected static void OnIsBusyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BasicDialogShell dialogShell = obj as BasicDialogShell;

            if(GetEnableBusyIndicator(dialogShell))
            {
                dialogShell.busyIndicator.Visibility = (bool)e.NewValue ? Visibility.Visible : Visibility.Hidden;

                if (dialogShell.leftCmdPlaceholder.Content != null)
                {
                    foreach (var btn in VisualThreeHelper.FindVisualChildren<Button>(dialogShell.leftCmdPlaceholder))
                    {
                        btn.IsEnabled = (bool)e.NewValue;
                    }
                }

                if (dialogShell.rightCmdPlaceholder.Content != null)
                {
                    foreach (var btn in VisualThreeHelper.FindVisualChildren<Button>(dialogShell.rightCmdPlaceholder))
                    {
                        btn.IsEnabled = (bool)e.NewValue;
                    }
                }
            }
        }

        public bool IsBusy
        {
            get { return (bool)GetValue(IsBusyProperty); }
            set { SetValue(IsBusyProperty, value); }
        }

        public static readonly DependencyProperty EnableBusyIndicatorProperty =
            DependencyProperty.RegisterAttached("EnableBusyIndicator", typeof(bool), typeof(BasicDialogShell),
                new PropertyMetadata(true));

        public static bool GetEnableBusyIndicator(DependencyObject obj)
        {
            return (bool)obj.GetValue(EnableBusyIndicatorProperty);
        }

        public static void SetEnableBusyIndicator(DependencyObject obj, object value)
        {
            obj.SetValue(EnableBusyIndicatorProperty, value);
        }

        public static readonly DependencyProperty LeftCommandsProperty =
            DependencyProperty.RegisterAttached("LeftCommands", typeof(object), typeof(BasicDialogShell));

        public static object GetLeftCommands(DependencyObject obj)
        {
            return (object)obj.GetValue(LeftCommandsProperty);
        }
        public static void SetLeftCommands(DependencyObject obj, object value)
        {
            obj.SetValue(LeftCommandsProperty, value);
        }

        public static readonly DependencyProperty RightCommandsProperty =
            DependencyProperty.RegisterAttached("RightCommands", typeof(object), typeof(BasicDialogShell));

        public static object GetRightCommands(DependencyObject obj)
        {
            return (object)obj.GetValue(RightCommandsProperty);
        }
        public static void SetRightCommands(DependencyObject obj, object value)
        {
            obj.SetValue(RightCommandsProperty, value);
        }
    }
}
