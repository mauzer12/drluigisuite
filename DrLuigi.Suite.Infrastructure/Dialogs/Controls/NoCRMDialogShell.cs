﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace DrLuigi.Suite.Infrastructure
{
    public class NoCRMDialogShell : BasicDialogShell
    {
        public NoCRMDialogShell()
        {
            DefaultStyleKey = typeof(NoCRMDialogShell);
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }
    }
}
