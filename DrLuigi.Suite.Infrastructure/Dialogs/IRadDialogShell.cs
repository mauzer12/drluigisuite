﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace DrLuigi.Suite.Infrastructure
{
    /// <summary>
    /// Contract for dialogs based on Telerik RadWindow control.
    /// </summary>
    public interface IRadDialogShell
    {
        object Header { get; set; }
        object Content { get; set; }
        bool IsLoading { get; set; }
        ContentControl Owner { get; set; }
        bool? DialogResult { get; set; }
        event CancelEventHandler Closing;
        void Show();
        void ShowDialog();
        void Close();
    }
}
