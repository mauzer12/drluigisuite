﻿using System.Windows;
using System.Windows.Controls;

namespace DrLuigi.Suite.Infrastructure
{
    public static class ButtonEx
    {
        public static readonly DependencyProperty IsOKProperty = 
            DependencyProperty.RegisterAttached("IsOK", typeof(bool), typeof(ButtonEx), 
            new FrameworkPropertyMetadata(false, new PropertyChangedCallback(OnIsOKChanged)));

        public static bool GetIsOK(DependencyObject d)
        {
            return (bool)d.GetValue(IsOKProperty);
        }

        public static void SetIsOK(DependencyObject d, bool value)
        {
            d.SetCurrentValue(IsOKProperty, value);
        }

        private static void OnIsOKChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Button target = (Button)d;
                
            if (target != null && (bool)e.NewValue)
            {
                target.Click += target_Click;
            }
        }

        private static void target_Click(object sender, RoutedEventArgs e)
        {
            Button target = (Button)sender;

            // IsOK property has meaning only on modal window just like IsCancel property.
            if (System.Windows.Interop.ComponentDispatcher.IsThreadModal)
            {
                // Convention: IsOK has priority.
                if (target.IsCancel)
                    target.IsCancel = false;

                Window parentWindow = Window.GetWindow(target);
                parentWindow.DialogResult = true;
                parentWindow.Close();
            }
            else
            {
                target.Click -= target_Click;
            }
        }
    }
}
