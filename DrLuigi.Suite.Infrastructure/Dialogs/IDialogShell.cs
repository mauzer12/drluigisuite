﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Infrastructure
{
    /// <summary>
    /// Contract for dialogs based on win32 Window control.
    /// </summary>
    public interface IDialogShell : IView
    {
        string Title { get; set; }
        object Content { get; set; }
        bool IsBusy { get; set; }
        System.Windows.Window Owner { get; set; }
        bool? DialogResult { get; set; }
        event CancelEventHandler Closing;
        void Show();
        bool? ShowDialog();
        void Close();
    }
}
