﻿using System;
namespace DrLuigi.Suite.Infrastructure
{
    public interface IDialogAware
    {
        void OnDialogInitialized(NavigationParameters p);
        event EventHandler<DialogEventArgs> PendingClose;
        bool IsLoading { get; set; }
    }
}
