﻿using DrLuigi.Suite.Infrastructure;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Windows.Controls.Map;

namespace DrLuigi.Suite.Client.Common.Shared
{
    public class LocationViewModel : ViewModel, ILocationViewModel
    {
        public LocationViewModel(Location location)
        {
            Location = location;
        }

        [InjectionConstructor]
        public LocationViewModel()
        {
        }

        public Location Location
        {
            get { return GetValue(() => Location); }
            set { SetValue(value, () => Location); }
        }

        public bool IsLoading
        {
            get; set;
        }

        public event EventHandler<DialogEventArgs> PendingClose;

        public void OnDialogInitialized(NavigationParameters p)
        {
            Location = (Location) p["Location"];
        }
    }
}
