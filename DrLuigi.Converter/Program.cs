﻿using DrLuigi.Suite.DAL;
using DrLuigi.Suite.DAL.NHibernate;
using DrLuigi.Suite.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DrLuigi.Suite.Client.Authentication;
using System.Threading;
using System.Text.RegularExpressions;
using DrLuigi.Suite.Client.GlobalServices;
using DrLuigi.Suite.Client.AdminModule;

namespace DrLuigi.Converter
{
    class Program
    {
        private static IInternetConnectionService internetService;
        private static ISessionManager sessionManager;
        private static IRepository repository;

        private static bool reportError = true;

        private static Dictionary<string, string> connStringsSource = new Dictionary<string, string>
        {
            /*{"2008", @"Data Source=194.152.206.64\SQLExpress;Initial Catalog=proizvodnja2008;Integrated Security=False;User ID=sa;Password=comrad;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"},
            {"2010", @"Data Source=194.152.206.64\SQLExpress;Initial Catalog=proizvodnja2010;Integrated Security=False;User ID=sa;Password=comrad;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"},
            {"2011", @"Data Source=194.152.206.64\SQLExpress;Initial Catalog=proizvodnja2011;Integrated Security=False;User ID=sa;Password=comrad;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"},
            {"2012", @"Data Source=194.152.206.64\SQLExpress;Initial Catalog=proizvodnja2012;Integrated Security=False;User ID=sa;Password=comrad;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"},
            {"2013", @"Data Source=194.152.206.64\SQLExpress;Initial Catalog=proizvodnja2013;Integrated Security=False;User ID=sa;Password=comrad;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"},
            {"2014", @"Data Source=194.152.206.64\SQLExpress;Initial Catalog=proizvodnja2014;Integrated Security=False;User ID=sa;Password=comrad;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"},
            {"2015", @"Data Source=194.152.206.64\SQLExpress;Initial Catalog=proizvodnja2015;Integrated Security=False;User ID=sa;Password=comrad;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"},*/
            {"Local", @"Server=(localdb)\ProjectsV12;Initial Catalog=proizvodnja2015;Integrated Security=True;" }
        };

        static void Main(string[] args)
        {
            Console.WriteLine("\t\t\t--- DrLuigi Db Converter v1.0 ---\n");
            Console.WriteLine("Copyright:   DrLuigi d.o.o");
            Console.WriteLine("Author:      Goran Kozar");
            Console.WriteLine("Created:     20.8.2015");
            Console.WriteLine("--------------------------");


            Console.WriteLine("\n\nDb schema setup in progress ...");
            internetService = new InternetConnectionService();
            IFactoryBuilder factoryBuilder = new FactoryBuilder("LocalConn", true);
            sessionManager = new SessionManager(factoryBuilder);
            factoryBuilder.DbSchemaCreated += (s, e) =>
            {
                Console.WriteLine("----> Db schema created successfuly.");
                Console.WriteLine("------------------------------------");
            };

            UnitOfWork uow = new UnitOfWork(sessionManager);
            repository = new Repository(uow);

            Console.WriteLine(DateTime.Now + "\n\nConversion start!\n\n");

            populateDatabase();
            createAdminAccount();

            Console.WriteLine("\n\nEnded in {0} ms", 2000);
            Console.WriteLine("========== {0} failed, {1} skipped ==========", 0, 0);

            Console.ReadKey();
        }

        private static void populateDatabase()
        {
            Console.WriteLine("Converting company data ...");
            foreach (KeyValuePair<string, string> entry in connStringsSource)
            {
                Console.WriteLine("Year: {0}", entry.Key);
                ConvertCompanyData(entry.Value);
                resolveEmployees(entry.Value);
                if(entry.Key == "2008")
                    resolveReceiptToPartner2008(entry.Value);
                else
                    resolveReceiptToPartner(entry.Value);
            }
            Console.WriteLine("----> Company conversion succeeded");

            Console.WriteLine("Converting partner data ...");
            foreach (KeyValuePair<string, string> entry in connStringsSource)
            {
                Console.WriteLine("Year: {0}", entry.Key);
                resolvePartners(entry.Value);
            }
            Console.WriteLine("----> Partner conversion succeeded");

            Console.WriteLine("Converting material data ...");
            foreach (KeyValuePair<string, string> entry in connStringsSource)
            {
                Console.WriteLine("Year: {0}", entry.Key);
                ConvertMaterialData(entry.Value);
            }
            Console.WriteLine("----> Material conversion succeeded");

            Console.WriteLine("Converting receipt item data ...");
            foreach (KeyValuePair<string, string> entry in connStringsSource)
            {
                Console.WriteLine("Year: {0}", entry.Key);
                resolveReceiptItems(entry.Value);
            }
            Console.WriteLine("----> Receipt item conversion succeeded");

            Console.WriteLine("Converting product data ...");
            /*foreach (KeyValuePair<string, string> entry in connStringsSource)
            {
                Console.WriteLine("Year: {0}", entry.Key);
                ConvertProductData(entry.Value);
            }*/
            Console.WriteLine("----> Product conversion succeeded");
        }

        private static void createAdminAccount()
        {
            IEncriptionProvider encriptionProvider = new SHA256EncriptionProvider();
            IUserRegistrationService userRegService = new UserRegistrationService(repository, encriptionProvider, internetService);
            Console.WriteLine("\n\n------------------------------------");
            Console.WriteLine("Creating admin account ...");

            UserInfo admin = new UserInfo
            {
                Role = 0,
                Username = "gkozar",
                Firstname = "Goran",
                Lastname = "Kozar",
                Email = "gorankozar@ymail.com"
            };

            string passwd = "admin";

            try
            {
                userRegService.RegisterUserAsync(admin, passwd);

                Console.WriteLine("----> Admin added successfuly.");
                Console.WriteLine("\nUsername: " + admin.Username + "\tPassword: " + passwd);
            }
            catch (InternetConnectionException)
            {
                Console.WriteLine("There's no internet connection");
                Console.ReadKey();
                Environment.Exit(1);
            }
            catch (Exception ex)
            {
                Console.WriteLine("----> Admin NOT added successfuly.");
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                Environment.Exit(1);
            }


            UserInfo testUser = new UserInfo
            {
                Role = 0,
                Username = "test",
                Firstname = "Goran",
                Lastname = "Kozar",
                Email = "gorankozar@ymail.com"
            };

            string testPasswd = "test";

            try
            {
                userRegService.RegisterUserAsync(testUser, testPasswd);

                Console.WriteLine("----> Test user added successfuly.");
                Console.WriteLine("\nUsername: " + testUser.Username + "\tPassword: " + testPasswd);
            }
            catch (InternetConnectionException)
            {
                Console.WriteLine("There's no internet connection");
                Console.ReadKey();
                Environment.Exit(1);
            }
            catch (Exception ex)
            {
                Console.WriteLine("----> Admin NOT added successfuly.");
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                Environment.Exit(1);
            }
        }

        private static void ConvertCompanyData(string connString)
        {
            SqlConnection sqlConn = new SqlConnection(connString);

            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    sqlConn.Open();

                SqlCommand sqlCmd = new SqlCommand(@"SELECT * FROM dbo.tvrtka", sqlConn);
                SqlDataReader reader = sqlCmd.ExecuteReader(CommandBehavior.Default);

                if (reader != null && reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Company c = repository.GetAll<Company>()
                            .Where(x => x.Name.Trim().ToLower() == reader.GetString(2).Trim().ToLower() && 
                                    reader.GetString(2) != "PRESTANAK RADA")
                            .FirstOrDefault();

                        if (c == null)
                        {
                            c = new Company
                            {
                                CompanyNumber = reader.GetString(1),
                                Name = reader.GetString(2),
                                Phone = reader.GetString(3),
                                FAX = reader.GetString(4),
                                AddressLine = reader.GetString(7),
                            };

                            if (!string.IsNullOrWhiteSpace(reader.GetString(6)) ||
                                !string.IsNullOrWhiteSpace(reader.GetString(5)))
                            {
                                c.City = repository.GetAll<City>()
                                    .Where(x => x.Name.ToLower() == reader.GetString(6).Trim().ToLower())
                                    .FirstOrDefault() ?? new City
                                    {
                                        PostNumber = reader.GetString(5) ?? " ",
                                        Name = reader.GetString(6) ?? " "
                                    };
                            }
                        }
                        else
                            continue;

                        try
                        {
                            repository.Update<Company>(c);
                        }
                        catch (TransactionFailedException)
                        {
                            Console.WriteLine("\n\nERROR: Transaction failed.");
                            Console.ReadKey();
                            Environment.Exit(1);
                        }
                    }
                }

                sqlConn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void resolveEmployees(string connString)
        {
            SqlConnection sqlConn = new SqlConnection(connString);

            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    sqlConn.Open();

                SqlCommand sqlCmd = new SqlCommand(@"SELECT t.naziv, r.ime, r.prezime FROM dbo.tvrtka AS t, dbo.radnik AS r WHERE t.sifTvrtka = r.sifTvrtka;", sqlConn);
                SqlDataReader reader = sqlCmd.ExecuteReader(CommandBehavior.Default);

                if (reader != null && reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Employee e = repository.GetAll<Employee>()
                            .Where(x => x.Firstname.Trim().ToLower() == reader.GetString(1).Trim().ToLower() &&
                                    x.Lastname.Trim().ToLower() == reader.GetString(2).Trim().ToLower())
                            .FirstOrDefault();

                        if (e == null)
                        {
                            e = new Employee
                            {
                                Firstname = reader.GetString(1).Trim(),
                                Lastname = reader.GetString(2).Trim()
                            };
                        }

                        if (e.Company == null && !string.IsNullOrWhiteSpace(reader.GetString(0)) && 
                            reader.GetString(0) != "PRESTANAK RADA")
                        {
                            e.Company = repository.GetAll<Company>()
                                .Where(x => x.Name.Trim().ToLower() == reader.GetString(0).ToLower().Trim())
                                .FirstOrDefault();

                            if (e.Company == null)
                            {
                                Console.WriteLine("UNEXPECTED: encountered null company while resolving employee.");
                                continue;
                            }
                        }

                        try
                        {
                            repository.Update<Employee>(e);
                        }
                        catch (TransactionFailedException)
                        {
                            Console.WriteLine("\n\nERROR: Transaction failed.");
                            Console.ReadKey();
                            Environment.Exit(1);
                        }
                    }
                }

                sqlConn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void resolvePartners(string connString)
        {
            SqlConnection sqlConn = new SqlConnection(connString);

            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    sqlConn.Open();

                SqlCommand sqlCmd = new SqlCommand(@"SELECT par.*, d.nazivDrzava, d.sifGrupaDrzava FROM dbo.partner AS par, dbo.drzava AS d WHERE par.sifDrzava = d.sifDrzava;", sqlConn);/////////////////////////////
                SqlDataReader reader = sqlCmd.ExecuteReader(CommandBehavior.Default);

                if (reader != null && reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Partner p = repository.GetAll<Partner>()
                            .Where(x => x.Name.Trim().ToLower() == reader.GetString(1).Trim().ToLower())
                            .FirstOrDefault();

                        if (p == null)
                        {
                            Console.WriteLine(reader.GetString(0));///////////
                            p = new Partner
                            {
                                Name = reader.GetString(1),
                                AddressLine = reader.IsDBNull(4) || string.IsNullOrWhiteSpace(reader.GetString(4)) ? null : reader.GetString(4),
                                Phone = reader.IsDBNull(5) || string.IsNullOrWhiteSpace(reader.GetString(5)) ? null : reader.GetString(5),
                                FAX = reader.IsDBNull(6) || string.IsNullOrWhiteSpace(reader.GetString(6)) ? null : reader.GetString(6),
                            };

                            string[] taxOIB = reader.IsDBNull(8) ? new string[] { } : reader.GetString(8)
                                .Split(new string[] { "OIB" }, StringSplitOptions.None);

                            if (taxOIB.Length > 2)
                                Console.WriteLine("UNEXPECTED: there's something I did not considered while parsing 'dbo.partner.porezniBr' fileld");

                            if (taxOIB.Length >= 1 && !string.IsNullOrWhiteSpace(taxOIB[0]))
                                p.TaxNumber = taxOIB[0].Trim();

                            if (taxOIB.Length >= 2 && !string.IsNullOrWhiteSpace(taxOIB[1]))
                                p.OIB = taxOIB[1].Trim();

                            string[] emailWeb = reader.IsDBNull(7) ? new string[] { } :
                                reader.GetString(7).Split(new char[] { ',' })
                                .Where(x => !string.IsNullOrWhiteSpace(x)).ToArray(); // in case ',' char is at end after spliting there can be empty string.

                            foreach (string entry in emailWeb)
                            {
                                if (isValidEmail(entry.Trim()))
                                {
                                    if (!string.IsNullOrWhiteSpace(p.Email))
                                        p.Email += ", ";

                                    p.Email += entry;
                                }
                                else if (entry.Contains("www"))
                                {
                                    bool isValid = false;

                                    // prepare entry, it' likly to be an web address
                                    string[] validScheme = { "http//:", "https//:" };
                                    foreach (string scheme in validScheme)
                                    {
                                        string e = entry.Trim().Replace("http:/", scheme); // correction
                                        if (!e.StartsWith(validScheme[0]) && !e.StartsWith(validScheme[1])) // use linq here
                                            e = e.Insert(0, scheme);

                                        if (isValidUrl(e))
                                        {
                                            isValid = true;

                                            if (!string.IsNullOrWhiteSpace(p.Web))
                                                p.Web += ", ";

                                            p.Web += e;
                                            break;
                                        }
                                    }

                                    if (!isValid)
                                        Console.WriteLine("WARNING: there's an entry thats likly to be an web address but it failed on url validation {0}", entry);
                                }
                                else
                                {
                                    Console.WriteLine("WARNING: entry {0} is not valid email nor valid uri! Its saved in Prod.Partner.Email.", entry);

                                    if (!string.IsNullOrWhiteSpace(p.Email))
                                        p.Email += ", ";

                                    p.Email += entry;
                                }
                            }

                            City city = null;
                            // resolve address city
                            if (!reader.IsDBNull(2) && !reader.IsDBNull(3) &&
                                !string.IsNullOrWhiteSpace(reader.GetString(2)) && !string.IsNullOrWhiteSpace(reader.GetString(3)))
                            {
                                city = repository.GetAll<City>()
                                .Where(x => x.PostNumber == reader.GetString(3))
                                .FirstOrDefault();

                                // not yet in db and not city with empty name and post number fields.
                                if (city == null && (!string.IsNullOrWhiteSpace(reader.GetString(3)) ||
                                    !string.IsNullOrWhiteSpace(reader.GetString(2))))
                                {
                                    city = new City
                                    {
                                        Name = reader.GetString(2) ?? " ", // TODO: capitalize
                                        PostNumber = reader.GetString(3) ?? " "
                                    };
                                }
                                else
                                {
                                    // city already exists, set fields that have non meaningful value.
                                    if (!reader.IsDBNull(3) && string.IsNullOrWhiteSpace(city.PostNumber) && !string.IsNullOrWhiteSpace(reader.GetString(3)))
                                    {
                                        city.PostNumber = reader.GetString(3).Trim();
                                    }

                                    if (!reader.IsDBNull(2) && string.IsNullOrWhiteSpace(city.Name) && !string.IsNullOrWhiteSpace(reader.GetString(2)))
                                    {
                                        city.Name = reader.GetString(2).Trim();
                                    }

                                    // correct postNumber
                                    if (city.Name == "TORRE SAN PATRIZIO")
                                        city.PostNumber = "63814";
                                }
                            }

                            if (city != null)
                            {
                                Country country = repository.GetAll<Country>()
                                    .Where(x => x.Code.ToUpper() == reader.GetString(9).ToUpper().Trim() ||
                                        x.Name.ToUpper().Trim() == reader.GetString(10).ToUpper().Trim())
                                    .FirstOrDefault();

                                //resolve address country.
                                if (country == null && !string.IsNullOrWhiteSpace(reader.GetString(9)))
                                {
                                    country = new Country
                                    {
                                        Code = reader.GetString(9).Trim() ?? " ",
                                        Name = capitalizeString(reader.GetString(10).Trim()) ?? " ",
                                        IsPartOfEU = !reader.IsDBNull(11) && reader.GetString(11).ToUpper().Trim() == "EU"
                                    };
                                }
                                else
                                {
                                    //country already exists, set fields that have non meaningful value.
                                    if (string.IsNullOrWhiteSpace(country.Code) && !string.IsNullOrWhiteSpace(reader.GetString(9)))
                                    {
                                        country.Code = reader.GetString(9).ToUpper().Trim();
                                    }

                                    if (string.IsNullOrWhiteSpace(country.Name) && !string.IsNullOrWhiteSpace(reader.GetString(10)))
                                    {
                                        country.Name = reader.GetString(10).Trim(); //TODO: capitalize
                                    }

                                    if (country.IsPartOfEU == null)
                                    {
                                        country.IsPartOfEU = reader.GetString(11) != null &&
                                            reader.GetString(11).ToUpper().Trim() == "EU";
                                    }
                                }

                                city.Country = country;
                            }
                            p.City = city;
                        }

                        try
                        {
                            repository.Update<Partner>(p);
                        }
                        catch (TransactionFailedException)
                        {
                            Console.WriteLine("\n\nERROR: Transaction failed.");
                            Console.ReadKey();
                            Environment.Exit(1);
                        }
                    }
                }
                sqlConn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void resolveReceiptToPartner(string connString)
        {
            SqlConnection sqlConn = new SqlConnection(connString);

            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    sqlConn.Open();

                SqlCommand sqlCmd = new SqlCommand(@"SELECT t.naziv, pri.brojPrimke, pri.datumUnosa, par.*, d.nazivDrzava, d.sifGrupaDrzava FROM dbo.tvrtka AS t, dbo.primka AS pri, dbo.partner AS par, dbo.drzava AS d WHERE t.sifTvrtka = pri.sifTvrtka AND pri.sifPartner = par.sifPartner AND par.sifDrzava = d.sifDrzava;", sqlConn);
                SqlDataReader reader = sqlCmd.ExecuteReader(CommandBehavior.Default);

                if (reader != null && reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Receipt r = new Receipt
                        {
                            ReceiptNumber = reader.GetString(1),
                            CreationDate = reader.GetDateTime(2),
                        };

                        // get company side of receipt mapping.
                        Company c = repository.GetAll<Company>()
                            .Where(x => x.Name.Trim().ToLower() == reader.GetString(0).Trim().ToLower())
                            .FirstOrDefault();

                        if (c == null)
                        {
                            // in case this message occure, I want to see for which company and I can change this block to add this company in db.
                            Console.WriteLine("UNEXPECTED: There's recept placed on company that no longer exists.");
                            continue;
                        }
                        else
                            r.Company = c;

                        // get partner side of receipt mapping.
                        Partner p = repository.GetAll<Partner>()
                            .Where(x => x.Name.Trim().ToLower() == reader.GetString(4).Trim().ToLower())
                            .FirstOrDefault();

                        if (p == null)
                        {
                            p = new Partner
                            {
                                Name = reader.GetString(4),
                                AddressLine = reader.IsDBNull(7) || string.IsNullOrWhiteSpace(reader.GetString(7)) ? null : reader.GetString(7),
                                Phone = reader.IsDBNull(8) || string.IsNullOrWhiteSpace(reader.GetString(8)) ? null : reader.GetString(8),
                                FAX = reader.IsDBNull(9) || string.IsNullOrWhiteSpace(reader.GetString(9)) ? null : reader.GetString(9),
                            };

                            string[] taxOIB = reader.IsDBNull(11) ? new string[] { } : reader.GetString(11)
                                .Split(new string[] { "OIB" }, StringSplitOptions.None);

                            if (taxOIB.Length > 2)
                                Console.WriteLine("UNEXPECTED: there's something I did not considered while parsing 'dbo.partner.porezniBr' fileld");

                            if (taxOIB.Length >= 1 && !string.IsNullOrWhiteSpace(taxOIB[0]))
                                p.TaxNumber = taxOIB[0].Trim();

                            if (taxOIB.Length >= 2 && !string.IsNullOrWhiteSpace(taxOIB[1]))
                                p.OIB = taxOIB[1].Trim();

                            string[] emailWeb = reader.IsDBNull(10) ? new string[] { } :
                                reader.GetString(10).Split(new char[] { ',' })
                                .Where(x => !string.IsNullOrWhiteSpace(x)).ToArray(); // in case ',' char is at end after spliting there can be empty string.

                            foreach (string entry in emailWeb)
                            {
                                if (isValidEmail(entry.Trim()))
                                {
                                    if (!string.IsNullOrWhiteSpace(p.Email))
                                        p.Email += ", ";

                                    p.Email += entry;
                                }
                                else if (entry.Contains("www"))
                                {
                                    bool isValid = false;

                                    // prepare entry, it' likly to be an web address
                                    string[] validScheme = { "http//:", "https//:" };
                                    foreach (string scheme in validScheme)
                                    {
                                        string e = entry.Trim().Replace("http:/", scheme); // correction
                                        if (!e.StartsWith(validScheme[0]) && !e.StartsWith(validScheme[1])) // use linq here
                                            e = e.Insert(0, scheme);

                                        if (isValidUrl(e))
                                        {
                                            isValid = true;

                                            if (!string.IsNullOrWhiteSpace(p.Web))
                                                p.Web += ", ";

                                            p.Web += e;
                                            break;
                                        }
                                    }

                                    if (!isValid)
                                        Console.WriteLine("WARNING: there's an entry thats likly to be an web address but it failed on url validation {0}", entry);
                                }
                                else
                                {
                                    Console.WriteLine("WARNING: entry {0} is not valid email nor valid uri! Its saved in Prod.Partner.Email.", entry);

                                    if (!string.IsNullOrWhiteSpace(p.Email))
                                        p.Email += ", ";

                                    p.Email += entry;
                                }
                            }

                            City city = null;
                            // resolve address city
                            if (!reader.IsDBNull(6) && !reader.IsDBNull(5) && 
                                !string.IsNullOrWhiteSpace(reader.GetString(6)) && !string.IsNullOrWhiteSpace(reader.GetString(5)))
                            {
                                city = repository.GetAll<City>()
                                .Where(x => x.PostNumber == reader.GetString(6))
                                .FirstOrDefault();

                                // not yet in db and not city with empty name and post number fields.
                                if (city == null && (!string.IsNullOrWhiteSpace(reader.GetString(6)) ||
                                    !string.IsNullOrWhiteSpace(reader.GetString(5))))
                                {
                                    city = new City
                                    {
                                        Name = reader.GetString(5) ?? " ", // TODO: capitalize
                                        PostNumber = reader.GetString(6) ?? " "
                                    };
                                }
                                else
                                {
                                    // city already exists, set fields that have non meaningful value.
                                    if (!reader.IsDBNull(6) && string.IsNullOrWhiteSpace(city.PostNumber) && !string.IsNullOrWhiteSpace(reader.GetString(6)))
                                    {
                                        city.PostNumber = reader.GetString(6).Trim();
                                    }

                                    if (!reader.IsDBNull(6) && string.IsNullOrWhiteSpace(city.Name) && !string.IsNullOrWhiteSpace(reader.GetString(5)))
                                    {
                                        city.Name = reader.GetString(5).Trim();
                                    }

                                    // correct postNumber
                                    if (city.Name == "TORRE SAN PATRIZIO")
                                        city.PostNumber = "63814";
                                }
                            }

                            if (city != null)
                            { 
                                Country country = repository.GetAll<Country>()
                                    .Where(x => x.Code.ToUpper() == reader.GetString(12).ToUpper().Trim() ||
                                        x.Name.ToUpper().Trim() == reader.GetString(13).ToUpper().Trim())
                                    .FirstOrDefault();

                                //resolve address country.
                                if (country == null && !string.IsNullOrWhiteSpace(reader.GetString(12)))
                                {
                                    country = new Country
                                    {
                                        Code = reader.GetString(12).Trim() ?? " ",
                                        Name = capitalizeString(reader.GetString(13).Trim()) ?? " ",
                                        IsPartOfEU = !reader.IsDBNull(14) && reader.GetString(14).ToUpper().Trim() == "EU"
                                    };
                                }
                                else
                                {
                                    //country already exists, set fields that have non meaningful value.
                                    if (string.IsNullOrWhiteSpace(country.Code) && !string.IsNullOrWhiteSpace(reader.GetString(12)))
                                    {
                                        country.Code = reader.GetString(12).ToUpper().Trim();
                                    }

                                    if (string.IsNullOrWhiteSpace(country.Name) && !string.IsNullOrWhiteSpace(reader.GetString(13)))
                                    {
                                        country.Name = reader.GetString(13).Trim(); //TODO: capitalize
                                    }

                                    if (country.IsPartOfEU == null)
                                    {
                                        country.IsPartOfEU = reader.GetString(14) != null &&
                                            reader.GetString(14).ToUpper().Trim() == "EU";
                                    }
                                }

                                city.Country = country;
                            }
                            p.City = city;
                        }
                        else
                        {
                            //partner already exists -> track history if you want.
                            if (p.Name.ToUpper() != reader.GetString(4).Trim().ToUpper())
                                p.Name = reader.GetString(4);

                            if (p.AddressLine == null || !reader.IsDBNull(7) && p.AddressLine.ToUpper() != reader.GetString(7).ToUpper().Trim())
                                p.AddressLine = reader.GetString(7);

                            if (p.Phone == null || !reader.IsDBNull(8) && p.Phone.Trim() != reader.GetString(8).Trim())
                                p.Phone = reader.GetString(8);

                            if (p.FAX == null || !reader.IsDBNull(9) && p.FAX.Trim() != reader.GetString(9).Trim())
                                p.FAX = reader.GetString(9);


                            string[] taxOIB = reader.IsDBNull(11) ? new string[] { } : reader.GetString(11)
                                .Split(new string[] { "OIB" }, StringSplitOptions.None);

                            if (taxOIB.Length > 2)
                                Console.WriteLine("UNEXPECTED: there's something I did not considered while parsing 'dbo.partner.porezniBr' fileld");

                            if (taxOIB.Length >= 1 && !string.IsNullOrWhiteSpace(taxOIB[0]) && p.TaxNumber != taxOIB[0].Trim())
                                p.TaxNumber = taxOIB[0].Trim();

                            if (taxOIB.Length >= 2 && !string.IsNullOrWhiteSpace(taxOIB[1]) && p.OIB != taxOIB[1].Trim())
                                p.OIB = taxOIB[1].Trim();

                            /*string[] emailWeb = reader.IsDBNull(10) ? new string[] { } :
                                reader.GetString(10).Split(new char[] { ',' })
                                .Where(x => !string.IsNullOrWhiteSpace(x)).ToArray(); // in case ',' char is at end after spliting there can be empty string.

                            foreach (string entry in emailWeb)
                            {
                                if (!p.Email.Contains(entry.Trim()) && isValidEmail(entry.Trim()))
                                {
                                    if (!string.IsNullOrWhiteSpace(p.Email))
                                        p.Email += ", ";

                                    p.Email += entry;
                                }
                                else if (entry.Contains("www"))
                                {
                                    bool isValid = false;

                                    // prepare entry, it' likly to be an web address
                                    string[] validScheme = { "http//:", "https//:" };
                                    foreach (string scheme in validScheme)
                                    {
                                        string e = entry.Trim().Replace("http:/", scheme); // correction
                                        if (!e.StartsWith(validScheme[0]) && !e.StartsWith(validScheme[1])) // use linq here
                                            e = e.Insert(0, scheme);
                                        if(!p.Web.Contains(e))
                                        {
                                            if (isValidUrl(e))
                                            {
                                                isValid = true;

                                                if (!string.IsNullOrWhiteSpace(p.Web))
                                                    p.Web += ", ";

                                                p.Web += e;
                                                break;
                                            }
                                        }
                                    }

                                    if (!isValid)
                                        Console.WriteLine("WARNING: there's an entry thats likly to be an web address but it failed on url validation {0}", entry);
                                }
                                else
                                {
                                    Console.WriteLine("WARNING: entry {0} is not valid email nor valid uri! Its saved in Prod.Partner.Email.", entry);

                                    if (p.Email.Contains(entry.Trim()) && !string.IsNullOrWhiteSpace(p.Email))
                                        p.Email += ", ";

                                    p.Email += entry;
                                }
                            }*/

                            //--------------
                            // resolve address city
                            City city = null;
                            if(!reader.IsDBNull(6) && !reader.IsDBNull(5))
                            { 
                                city = repository.GetAll<City>()
                                    .Where(x => (x.PostNumber == reader.GetString(6)))
                                    .FirstOrDefault();

                                // not yet in db and not city with empty name and post number fields.
                                if (city == null && (!string.IsNullOrWhiteSpace(reader.GetString(6)) ||
                                    !string.IsNullOrWhiteSpace(reader.GetString(5))))
                                {
                                    city = new City
                                    {
                                        Name = reader.GetString(5) ?? " ", // TODO: capitalize
                                        PostNumber = reader.GetString(6) ?? " "
                                    };

                                    // correct postNumber
                                    if (city.Name == "TORRE SAN PATRIZIO")
                                        city.PostNumber = "63814";
                                }
                                else
                                {
                                    // city already exists, set fields that have non meaningful value.
                                    if (!reader.IsDBNull(6) && !string.IsNullOrWhiteSpace(reader.GetString(6)) && city.PostNumber != reader.GetString(6).Trim())
                                    {
                                        city.PostNumber = reader.GetString(6).Trim();
                                    }

                                    if (!reader.IsDBNull(5) &&!string.IsNullOrWhiteSpace(reader.GetString(5)) && city.Name != reader.GetString(5).Trim())
                                    {
                                        city.Name = reader.GetString(5).Trim();
                                    }
                                }
                            }

                            if(city != null)
                            { 
                                Country country = repository.GetAll<Country>()
                                    .Where(x => x.Code.ToUpper() == reader.GetString(12).ToUpper().Trim() ||
                                        x.Name.ToUpper().Trim() == reader.GetString(13).ToUpper().Trim())
                                    .FirstOrDefault();

                                //resolve address country.
                                if (country == null && !string.IsNullOrWhiteSpace(reader.GetString(12)))
                                {
                                    country = new Country
                                    {
                                        Code = reader.GetString(12).Trim() ?? " ",
                                        Name = capitalizeString(reader.GetString(13).Trim()) ?? " ",
                                        IsPartOfEU = !reader.IsDBNull(14) && reader.GetString(14).ToUpper().Trim() == "EU"
                                    };
                                }
                                else
                                {
                                    //country already exists, set fields that have non meaningful value.
                                    if (!string.IsNullOrWhiteSpace(reader.GetString(12)) && country.Code != reader.GetString(12).Trim())
                                    {
                                        country.Code = reader.GetString(12).ToUpper().Trim();
                                    }

                                    if (!string.IsNullOrWhiteSpace(reader.GetString(13)) && country.Name != reader.GetString(13).Trim())
                                    {
                                        country.Name = reader.GetString(13).Trim(); //TODO: capitalize
                                    }

                                    if (country.IsPartOfEU == null)
                                    {
                                        country.IsPartOfEU = reader.GetString(14) != null &&
                                            reader.GetString(14).ToUpper().Trim() == "EU";
                                    }
                                }

                                city.Country = country;
                            }

                            p.City = city;
                        }

                        r.Partner = p;

                        try
                        {
                            repository.Update<Receipt>(r);
                        }
                        catch (TransactionFailedException)
                        {
                            Console.WriteLine("\n\nERROR: Transaction failed.");
                            Console.ReadKey();
                            Environment.Exit(1);
                        }
                    }
                }
                sqlConn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void resolveReceiptToPartner2008(string connString)
        {
            SqlConnection sqlConn = new SqlConnection(connString);

            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    sqlConn.Open();

                SqlCommand sqlCmd = new SqlCommand(@"SELECT t.naziv, pri.brojPrimke, pri.datumUnosa, par.* FROM dbo.tvrtka AS t, dbo.primka AS pri, dbo.partner AS par WHERE t.sifTvrtka = pri.sifTvrtka AND pri.sifPartner = par.sifPartner;", sqlConn);
                SqlDataReader reader = sqlCmd.ExecuteReader(CommandBehavior.Default);

                if (reader != null && reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Receipt r = new Receipt
                        {
                            ReceiptNumber = reader.GetString(1),
                            CreationDate = reader.GetDateTime(2),
                        };

                        // get company side of receipt mapping.
                        Company c = repository.GetAll<Company>()
                            .Where(x => x.Name.Trim().ToLower() == reader.GetString(0).Trim().ToLower())
                            .FirstOrDefault();

                        if (c == null)
                        {
                            // in case this message occure, I want to see for which company and I can change this block to add this company in db.
                            Console.WriteLine("UNEXPECTED: There's recept placed on company that no longer exists.");
                            continue;
                        }
                        else
                            r.Company = c;

                        // get partner side of receipt mapping.
                        Partner p = repository.GetAll<Partner>()
                            .Where(x => x.Name.Trim().ToLower() == reader.GetString(4).Trim().ToLower())
                            .FirstOrDefault();

                        if (p == null)
                        {
                            p = new Partner
                            {
                                Name = reader.GetString(4),
                                AddressLine = reader.IsDBNull(7) || string.IsNullOrWhiteSpace(reader.GetString(7)) ? null : reader.GetString(7),
                                Phone = reader.IsDBNull(8) || string.IsNullOrWhiteSpace(reader.GetString(8)) ? null : reader.GetString(8),
                                FAX = reader.IsDBNull(9) || string.IsNullOrWhiteSpace(reader.GetString(9)) ? null : reader.GetString(9),
                            };

                            string[] taxOIB = reader.IsDBNull(11) ? new string[] { } : reader.GetString(11)
                                .Split(new string[] { "OIB" }, StringSplitOptions.None);

                            if (taxOIB.Length > 2)
                                Console.WriteLine("UNEXPECTED: there's something I did not considered while parsing 'dbo.partner.porezniBr' fileld");

                            if (taxOIB.Length >= 1 && !string.IsNullOrWhiteSpace(taxOIB[0]))
                                p.TaxNumber = taxOIB[0].Trim();

                            if (taxOIB.Length >= 2 && !string.IsNullOrWhiteSpace(taxOIB[1]))
                                p.OIB = taxOIB[1].Trim();

                            string[] emailWeb = reader.IsDBNull(10) ? new string[] { } :
                                reader.GetString(10).Split(new char[] { ',' })
                                .Where(x => !string.IsNullOrWhiteSpace(x)).ToArray(); // in case ',' char is at end after spliting there can be empty string.

                            foreach (string entry in emailWeb)
                            {
                                if (isValidEmail(entry.Trim()))
                                {
                                    if (!string.IsNullOrWhiteSpace(p.Email))
                                        p.Email += ", ";

                                    p.Email += entry;
                                }
                                else if (entry.Contains("www"))
                                {
                                    bool isValid = false;

                                    // prepare entry, it' likly to be an web address
                                    string[] validScheme = { "http//:", "https//:" };
                                    foreach (string scheme in validScheme)
                                    {
                                        string e = entry.Trim().Replace("http:/", scheme); // correction
                                        if (!e.StartsWith(validScheme[0]) && !e.StartsWith(validScheme[1])) // use linq here
                                            e = e.Insert(0, scheme);

                                        if (isValidUrl(e))
                                        {
                                            isValid = true;

                                            if (!string.IsNullOrWhiteSpace(p.Web))
                                                p.Web += ", ";

                                            p.Web += e;
                                            break;
                                        }
                                    }

                                    if (!isValid)
                                        Console.WriteLine("WARNING: there's an entry thats likly to be an web address but it failed on url validation {0}", entry);
                                }
                                else
                                {
                                    Console.WriteLine("WARNING: entry {0} is not valid email nor valid uri! Its saved in Prod.Partner.Email.", entry);

                                    if (!string.IsNullOrWhiteSpace(p.Email))
                                        p.Email += ", ";

                                    p.Email += entry;
                                }
                            }

                            // resolve address city
                            City city = repository.GetAll<City>()
                                .Where(x => x.PostNumber == reader.GetString(6) || x.Name.Trim().ToLower() == reader.GetString(5).Trim().ToLower())
                                .FirstOrDefault();

                            // not yet in db and not city with empty name and post number fields.
                            if (city == null && (!string.IsNullOrWhiteSpace(reader.GetString(6)) ||
                                !string.IsNullOrWhiteSpace(reader.GetString(5))))
                            {
                                city = new City
                                {
                                    Name = reader.GetString(5) ?? " ", // TODO: capitalize
                                    PostNumber = reader.GetString(6) ?? " "
                                };
                            }
                            else
                            {
                                // city already exists, set fields that have non meaningful value.
                                if (string.IsNullOrWhiteSpace(city.PostNumber) && !string.IsNullOrWhiteSpace(reader.GetString(6)))
                                {
                                    city.PostNumber = reader.GetString(6).Trim();
                                }

                                if (string.IsNullOrWhiteSpace(city.Name) && !string.IsNullOrWhiteSpace(reader.GetString(5)))
                                {
                                    city.Name = reader.GetString(5).Trim();
                                }
                            }

                            p.City = city;
                        }
                        else
                        {
                            //partner already exists -> track history if you want.
                            if (p.Name.ToUpper() != reader.GetString(4).Trim().ToUpper())
                                p.Name = reader.GetString(4);

                            if (!reader.IsDBNull(7) && p.AddressLine.ToUpper() != reader.GetString(7).ToUpper().Trim())
                                p.AddressLine = reader.GetString(7);

                            if (!reader.IsDBNull(8) && p.Phone.ToUpper() != reader.GetString(8).ToUpper().Trim())
                                p.Phone = reader.GetString(8);

                            if (!reader.IsDBNull(9) && p.FAX != reader.GetString(9).Trim())
                                p.FAX = reader.GetString(9);


                            string[] taxOIB = reader.IsDBNull(11) ? new string[] { } : reader.GetString(11)
                                .Split(new string[] { "OIB" }, StringSplitOptions.None);

                            if (taxOIB.Length > 2)
                                Console.WriteLine("UNEXPECTED: there's something I did not considered while parsing 'dbo.partner.porezniBr' fileld");

                            if (taxOIB.Length >= 1 && !string.IsNullOrWhiteSpace(taxOIB[0]) && p.TaxNumber != taxOIB[0].Trim())
                                p.TaxNumber = taxOIB[0].Trim();

                            if (taxOIB.Length >= 2 && !string.IsNullOrWhiteSpace(taxOIB[1]) && p.OIB != taxOIB[1].Trim())
                                p.OIB = taxOIB[1].Trim();

                            /*string[] emailWeb = reader.IsDBNull(10) ? new string[] { } :
                                reader.GetString(10).Split(new char[] { ',' })
                                .Where(x => !string.IsNullOrWhiteSpace(x)).ToArray(); // in case ',' char is at end after spliting there can be empty string.

                            foreach (string entry in emailWeb)
                            {
                                if (!p.Email.Contains(entry.Trim()) && isValidEmail(entry.Trim()))
                                {
                                    if (!string.IsNullOrWhiteSpace(p.Email))
                                        p.Email += ", ";

                                    p.Email += entry;
                                }
                                else if (entry.Contains("www"))
                                {
                                    bool isValid = false;

                                    // prepare entry, it' likly to be an web address
                                    string[] validScheme = { "http//:", "https//:" };
                                    foreach (string scheme in validScheme)
                                    {
                                        string e = entry.Trim().Replace("http:/", scheme); // correction
                                        if (!e.StartsWith(validScheme[0]) && !e.StartsWith(validScheme[1])) // use linq here
                                            e = e.Insert(0, scheme);
                                        if(!p.Web.Contains(e))
                                        {
                                            if (isValidUrl(e))
                                            {
                                                isValid = true;

                                                if (!string.IsNullOrWhiteSpace(p.Web))
                                                    p.Web += ", ";

                                                p.Web += e;
                                                break;
                                            }
                                        }
                                    }

                                    if (!isValid)
                                        Console.WriteLine("WARNING: there's an entry thats likly to be an web address but it failed on url validation {0}", entry);
                                }
                                else
                                {
                                    Console.WriteLine("WARNING: entry {0} is not valid email nor valid uri! Its saved in Prod.Partner.Email.", entry);

                                    if (p.Email.Contains(entry.Trim()) && !string.IsNullOrWhiteSpace(p.Email))
                                        p.Email += ", ";

                                    p.Email += entry;
                                }
                            }*/

                            //--------------
                            // resolve address city
                            City city = repository.GetAll<City>()
                                .Where(x => x.PostNumber == reader.GetString(6) || x.Name.Trim().ToLower() == reader.GetString(5).Trim().ToLower())
                                .FirstOrDefault();

                            // not yet in db and not city with empty name and post number fields.
                            if (city == null && (!string.IsNullOrWhiteSpace(reader.GetString(6)) ||
                                !string.IsNullOrWhiteSpace(reader.GetString(5))))
                            {
                                city = new City
                                {
                                    Name = reader.GetString(5) ?? " ", // TODO: capitalize
                                    PostNumber = reader.GetString(6) ?? " "
                                };
                            }
                            else
                            {
                                // city already exists, set fields that have non meaningful value.
                                if (!string.IsNullOrWhiteSpace(reader.GetString(6)) && city.PostNumber != reader.GetString(6).Trim())
                                {
                                    city.PostNumber = reader.GetString(6).Trim();
                                }

                                if (!string.IsNullOrWhiteSpace(reader.GetString(5)) && city.Name != reader.GetString(5).Trim())
                                {
                                    city.Name = reader.GetString(5).Trim();
                                }
                            }

                            p.City = city;
                        }

                        r.Partner = p;

                        try
                        {
                            repository.Update<Receipt>(r);
                        }
                        catch (TransactionFailedException)
                        {
                            Console.WriteLine("\n\nERROR: Transaction failed.");
                            Console.ReadKey();
                            Environment.Exit(1);
                        }
                    }
                }
                sqlConn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void resolveReceiptItems(string connString)
        {
            SqlConnection sqlConn = new SqlConnection(connString);

            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    sqlConn.Open();

                SqlCommand sqlCmd = new SqlCommand(@"SELECT p.brojPrimke, m.punaSifra, ps.jedCijenaKn, ps.jedCijenaE, ps.kolicina, ps.rabat FROM dbo.primkaStavke AS ps, dbo.primka AS p, dbo.materijal AS m WHERE ps.brojPrimke = p.brojPrimke AND ps.idMaterijal = m.idMaterijal;", sqlConn);
                SqlDataReader reader = sqlCmd.ExecuteReader(CommandBehavior.Default);

                if (reader != null && reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ReceiptItem ri = new ReceiptItem
                        {
                            UnitPriceKN = reader.GetDecimal(2),
                            UnitPriceEU = reader.GetDecimal(3),
                            Amount = reader.GetDecimal(4),
                            Rebate = reader.GetDecimal(5)
                        };


                        Receipt r = repository.GetAll<Receipt>()
                            .Where(x => x.ReceiptNumber == reader.GetString(0))
                            .FirstOrDefault();

                        if(r == null)
                        {
                            Console.WriteLine("UNEXPECTED: There's recept item placed on receipt that no longer exists.");
                            continue;
                        }

                        Material m = repository.GetAll<Material>()
                            .Where(x => x.Code == reader.GetString(1).Trim())
                            .FirstOrDefault();

                        if(m == null)
                        {
                            Console.WriteLine("UNEXPECTED: {0}", reader.GetString(1));
                            continue;
                        }
                            

                        ri.Receipt = r;
                        ri.Material = m;

                        try
                        {
                            repository.Update<ReceiptItem>(ri);
                        }
                        catch (TransactionFailedException)
                        {
                            Console.WriteLine("\n\nERROR: Transaction failed.");
                            Console.ReadKey();
                            Environment.Exit(1);
                        }
                    }
                }
                sqlConn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static bool isValidEmail(string emailAddress)
        {
            string validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
            + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
            + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            Regex validEmailRegex = new Regex(validEmailPattern, RegexOptions.IgnoreCase);

            return validEmailRegex.IsMatch(emailAddress);
        }

        private static bool isValidUrl(string urlName)
        {
            return Uri.IsWellFormedUriString(urlName, UriKind.RelativeOrAbsolute);
        }

        private static string capitalizeString(string s)
        {
            return char.ToUpper(s[0]) + s.Substring(1);
        }

        private static void ConvertMaterialData(string connString)
        {
            
            SqlConnection sqlConn = new SqlConnection(connString);
            
            try
            {
                Console.WriteLine("* Creating material and resolving material stock info ...");
                setMaterialStockInfo(sqlConn);
                Console.WriteLine("* Resolving material group and category ...");
                resolveMaterialGroupAndCategory(sqlConn);
                Console.WriteLine("* Resolving material color ...");
                resolveMaterialColor(sqlConn);
                Console.WriteLine("* Resolving material composition ...");
                setMaterialComposition();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void setMaterialStockInfo(SqlConnection sqlConn)
        {
            if (sqlConn.State != ConnectionState.Open)
                sqlConn.Open();

            SqlCommand sqlCmd = new SqlCommand(@"SELECT m.*, COALESCE(z.kolicina, 0) AS zaliha FROM dbo.materijal AS m LEFT OUTER JOIN dbo.zaliha AS z ON m.idMaterijal = z.idMaterijal", sqlConn);
            SqlDataReader reader = sqlCmd.ExecuteReader(CommandBehavior.Default);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Material m = repository.GetAll<Material>().Where(x => x.Code == reader.GetString(18))
                                           .FirstOrDefault();

                    if (m == null)
                    {
                        m = new Material
                        {
                            Name = reader.GetString(17),
                            Code = reader.GetString(18),
                            //UnitPriceKN = reader.GetDecimal(19),
                            //UnitPriceEU = reader.GetDecimal(20),
                            StockQuantity = reader.GetDecimal(21)
                        };

                        // resolve measurment unit.
                        string mUnitName = reader.GetString(16).ToLower().Trim();

                        if (string.IsNullOrEmpty(mUnitName))
                            throw new Exception("WARNING: measurment unit name is not provided.");

                        // measurment unit correction
                        if (mUnitName == "m 2")
                            mUnitName = "m2";

                        if (mUnitName == "kut")
                            mUnitName = "pak";

                        if (mUnitName == "lit")
                            mUnitName = "l";

                        m.MeasurmentUnit = repository.GetAll<MeasurmentUnit>().Where(x => x.Name == mUnitName).FirstOrDefault() ??
                            new MeasurmentUnit { Name = mUnitName, Step = 1 };    
                    }
                    else
                    {
                        // adjust stock quantity to last known value.
                        m.StockQuantity = reader.GetDecimal(21);
                    }

                    try
                    {
                        repository.Update<Material>(m);
                    }
                    catch (TransactionFailedException)
                    {
                        Console.WriteLine("\n\nERROR: Transaction failed.");
                        Console.ReadKey();
                        Environment.Exit(1);
                    }
                }
            }

            sqlConn.Close(); // close conn on table for reading
        }

        private static void resolveMaterialGroupAndCategory(SqlConnection sqlConn)
        {
            if (sqlConn.State != ConnectionState.Open)
                sqlConn.Open();

            SqlCommand sqlCmd = new SqlCommand(@"SELECT m.punaSifra, m.sifMaterijalGlavna, mv.naziv AS nazivGrupe, gm.sifMaterijalPrva, gm.sifMaterijalZadnja, gm.naziv AS nazivKategorije FROM dbo.materijal AS m, dbo.materijalVrsta AS mv, dbo.grupaMaterijal gm WHERE m.sifMaterijalGlavna = mv.sifMaterijal AND CONVERT(INT, mv.sifMaterijal) >= CONVERT(INT, gm.sifMaterijalPrva) AND CONVERT(INT, mv.sifMaterijal) <= CONVERT(INT, gm.sifMaterijalZadnja);", sqlConn);
            SqlDataReader reader = sqlCmd.ExecuteReader(CommandBehavior.Default);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Material m = repository.GetAll<Material>().Where(x => x.Code == reader.GetString(0))
                        .FirstOrDefault();

                    if (m == null)
                    {
                        Console.WriteLine("UNEXPECTED: encountered null material while resolving material group and category.");
                        continue;
                    }

                    int groupCode = -1;
                    if (int.TryParse(reader.GetString(1), out groupCode))
                    {
                        m.Group = repository.GetAll<MaterialGroup>().Where(x => x.Code == groupCode).FirstOrDefault() ??
                            new MaterialGroup
                            {
                                Code = groupCode,
                                Name = reader.GetString(2),
                                Category = repository.GetAll<MaterialCategory>().Where(x => x.Name == reader.GetString(5))
                                                     .FirstOrDefault()
                            };

                        if (m.Group.Category == null)
                        {
                            m.Group.Category = new MaterialCategory();
                            m.Group.Category.Name = reader.GetString(5);

                            int codeMinValue, codeMaxValue = -1;
                            if (int.TryParse(reader.GetString(3), out codeMinValue) &&
                                int.TryParse(reader.GetString(4), out codeMaxValue))
                            {
                                m.Group.Category.MinValue = codeMinValue;
                                m.Group.Category.MaxValue = codeMaxValue;
                            }
                            else
                            {
                                Console.WriteLine("UNEXPECTED: Can't convert min or max value of material category to int32 type");
                            }
                        }

                        try
                        {
                            repository.Update<Material>(m);
                        }
                        catch (TransactionFailedException)
                        {
                            Console.WriteLine("\n\nERROR: Transaction failed.");
                            Console.ReadKey();
                            Environment.Exit(1);
                        }
                    }
                    else
                    {
                        Console.WriteLine("UNEXPECTED: Can't convert group code to int32 type");
                    }
                }
            }

            sqlConn.Close(); // close conn on table for reading
        }

        private static void resolveMaterialColor(SqlConnection sqlConn)
        {
            if (sqlConn.State != ConnectionState.Open)
                sqlConn.Open();

            SqlCommand sqlCmd = new SqlCommand(@"SELECT m.punaSifra, m.sifBojaGlavna, b.nazBoja FROM dbo.materijal AS m, dbo.boja AS b WHERE m.sifBojaGlavna = b.sifBoja AND b.sifBoja <> '00';", sqlConn);
            SqlDataReader reader = sqlCmd.ExecuteReader(CommandBehavior.Default);

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Material m = repository.GetAll<Material>().Where(x => x.Code == reader.GetString(0)).FirstOrDefault();

                    if (m == null)
                    {
                        Console.WriteLine("UNEXPECTED: encountered null material while resolving material group and category.");
                        continue;
                    }

                    m.Color = repository.GetAll<Color>().Where(x => x.Code == reader.GetString(1)).FirstOrDefault() ??
                        new Color
                        {
                            Code = reader.GetString(1),
                            Name = reader.GetString(2)
                        };

                    try
                    {
                        repository.Update<Material>(m);
                    }
                    catch (TransactionFailedException)
                    {
                        Console.WriteLine("\n\nERROR: Transaction failed.");
                        Console.ReadKey();
                        Environment.Exit(1);
                    }
                }
            }

            sqlConn.Close(); // close conn on table for reading
        }

        private static void setMaterialComposition()
        {
            // Gets only composit materials where ingradients is not yet determined.
            List<string[]> compositeMaterialCodes = repository.GetAll<Material>()
                .Where(x => !x.ComposedOf.Any()).ToList()
                .Select(x => x.Code.Split(new char[] { '#' }))
                .Where(x => x.Length > 1).ToList();

            List<string> logsCollection = new List<string>();

            foreach (string[] codes in compositeMaterialCodes)
            {
                Material m = repository.GetAll<Material>().Where(x => x.Code == string.Join("#", codes)).FirstOrDefault();

                if (m == null)
                {
                    Console.WriteLine("UNEXPECTED: Composit material is no longer in database.");
                    return; // skip and continue execution.
                }

                for (int i = 0; i < codes.Length; i++)
                {
                    Material component = repository.GetAll<Material>().Where(x => x.Code == codes[i].Trim())
                        .FirstOrDefault();
                    
                    if (component == null)
                    {
                        //string msg = string.Format("UNEXPECTED: Material with code '{0}' used in composite {1} do not exists.",
                        //    codes[i], string.Join("#", codes));

                        string msg = codes[i]; // log just full code of missing material.

                        Console.WriteLine(msg);
                        if (!logsCollection.Contains(msg))
                            logsCollection.Add(msg);

                        continue; // skip and continue execution.
                        //TODO: Add material to database if not exists and name it '#####' or read XML.
                    }

                    m.AddComposedOf(component);
                }

                try
                {
                    repository.Update<Material>(m);
                }
                catch(TransactionFailedException)
                {
                    Console.WriteLine("\n\nERROR: Transaction failed.");
                    Console.ReadKey();
                    Environment.Exit(1);
                }
            }


            if (reportError && logsCollection.Any())
            {
                logToFile("----------------- MATERIAL COMPOSED OF ------------------");

                foreach (var log in logsCollection)
                    logToFile(log);

                logToFile("---------------------------------------------------------");
            }
        }

        private static void ConvertProductData(string connString)
        {
            SqlConnection sqlConn = new SqlConnection(connString);

            try
            {
                Console.WriteLine("* Creating product and resolving material declaration ...");
                resolveProductMaterials(sqlConn);
                Console.WriteLine("* Resolving material declaration ...");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void resolveProductMaterials(SqlConnection sqlConn)
        {
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    sqlConn.Open();

                SqlCommand sqlCmd = new SqlCommand(@"SELECT * FROM dbo.tvrtka", sqlConn);
                SqlDataReader reader = sqlCmd.ExecuteReader(CommandBehavior.Default);
                
                if (reader != null && reader.HasRows)
                {
                    while (reader.Read())
                    {
                        /*Company c = repository.GetAll<Company>()
                            .Where(x => x.Name.Trim().ToLower() == reader.GetString(2).Trim().ToLower() &&
                                    reader.GetString(2) != "PRESTANAK RADA")
                            .FirstOrDefault();*/

                        Product p = null; //repository.GetAll<Product>()
                          //  .Where(x => x.Code == reader)

                        if (p == null)
                        {
                            /*c = new Company
                            {
                                CompanyNumber = reader.GetString(1),
                                Name = reader.GetString(2),
                                Phone = reader.GetString(3),
                                FAX = reader.GetString(4),
                                AddressLine = reader.GetString(7),
                            };

                            if (!string.IsNullOrWhiteSpace(reader.GetString(6)) ||
                                !string.IsNullOrWhiteSpace(reader.GetString(5)))
                            {
                                c.City = repository.GetAll<City>()
                                    .Where(x => x.Name.ToLower() == reader.GetString(6).Trim().ToLower())
                                    .FirstOrDefault() ?? new City
                                    {
                                        PostNumber = reader.GetString(5) ?? " ",
                                        Name = reader.GetString(6) ?? " "
                                    };
                            }*/
                        }
                        else
                            continue;

                        try
                        {
                            repository.Update<Product>(p);
                        }
                        catch (TransactionFailedException)
                        {
                            Console.WriteLine("\n\nERROR: Transaction failed.");
                            Console.ReadKey();
                            Environment.Exit(1);
                        }
                    }
                }

                sqlConn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void logToFile(string text)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\Drluigi.Converter logs.txt";

            if (!System.IO.File.Exists(path))
                System.IO.File.Create(path);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(path, true))
            {
                file.WriteLine(text);
            }
        }
    }
}
