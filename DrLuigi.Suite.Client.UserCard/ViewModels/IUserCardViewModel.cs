﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrLuigi.Suite.Infrastructure;
using DrLuigi.Suite.Client.Authentication;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.UserCard
{
    public interface IUserCardViewModel : IViewModel
    {
        UserInfo UserInfo { get; set; }
        string FullName { get; }

        ICommand SignoutCommand {get;}
        ICommand ChangeCredentialsCommand { get; }
        ICommand UpdateUserProfileCommand { get; }
    }
}
