﻿using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Events;
using DrLuigi.Suite.Client.Authentication;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.UserCard
{
    public class UserCardViewModel : ViewModel, IUserCardViewModel
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IAuthenticationService authService;
        private readonly IDialogService dialogService;

        public UserCardViewModel(IEventAggregator eventAggregator, IAuthenticationService authService, IDialogService dialogService)
        {
            this.eventAggregator = eventAggregator;
            this.dialogService = dialogService;
            this.authService = authService;

            // On user login make user data aviable to UI.
            eventAggregator.GetEvent<UserSignedin>().Subscribe(OnUserInfoAviable);
        }

        protected void OnUserInfoAviable(UserInfo userInfo)
        {
            UserInfo = userInfo;
        }

        public UserInfo UserInfo
        {
            get { return GetValue(() => UserInfo); }
            set { SetValue(value, () => UserInfo, OnChanged:() => OnPropertyChanged("FullName")); }
        }
        
        public string FullName 
        {
            get 
            { 
                return UserInfo != null ? UserInfo.Firstname.Trim() + " " + UserInfo.Lastname.Trim() : "";
            } 
        }

        private ICommand signoutCommand;
        public ICommand SignoutCommand
        {
            get 
            {
                if (signoutCommand == null)
                {
                    signoutCommand = new RelayCommand(
                        () => signoutExecute(),
                        () => UserInfo != null);
                }

                return signoutCommand;
            }
        }
        
        private ICommand changeCredentialsCommand;
        public ICommand ChangeCredentialsCommand
        {
            get 
            {
                if (changeCredentialsCommand == null)
                {
                    changeCredentialsCommand = new RelayCommand(
                        () => changeCredentialsExecute(),
                        () => UserInfo != null);
                }

                return changeCredentialsCommand;
            }
        }

        private ICommand updateUserProfileCommand;
        public ICommand UpdateUserProfileCommand
        {
            get
            {
                if (updateUserProfileCommand == null)
                {
                    updateUserProfileCommand = new RelayCommand(
                        () => updateUserProfileExecute(),
                        () => UserInfo != null);
                }

                return updateUserProfileCommand;
            }
        }

        private void signoutExecute()
        {
            authService.SignoutUser(UserInfo.Username);
            UserInfo = null;

            // let the world know user is loged out.
            eventAggregator.GetEvent<UserSignedout>().Publish();
        }

        private void changeCredentialsExecute()
        {
            NavigationParameters p = new NavigationParameters
            {
                { "Username", UserInfo.Username }
            };

            dialogService.ShowDialogView<IChangePasswordViewModel>(
                "Izmjena pristupnih podataka", "default", p,
                onDialogClosing: (dialog, resultVM, e) =>
                {
                    if (dialog.DialogResult.HasValue && dialog.DialogResult.Value)
                    {
                        dialogService.ShowMessageBox("Lozinka je uspješno promjenjena!", "Rezultat",
                        System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
                    }
                });
        }

        private void updateUserProfileExecute()
        {
            
        }

        public override void Dispose()
        {
            eventAggregator.GetEvent<UserSignedin>().Unsubscribe(OnUserInfoAviable);

            base.Dispose();
        }
    }
}
