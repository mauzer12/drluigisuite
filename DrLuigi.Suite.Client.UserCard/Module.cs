﻿using DrLuigi.Suite.Client.Authentication;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.UserCard
{
    public class Module : IModule
    {
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;

        public Module(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            container.RegisterType<IUserCardViewModel, UserCardViewModel>(
                new ContainerControlledLifetimeManager());

            container.RegisterType<IEncriptionProvider, SHA256EncriptionProvider>();
            container.RegisterType<IAuthenticationService, AuthenticationService>();
            container.RegisterType<ISigninViewModel, SigninViewModel>();
            container.RegisterType<IChangePasswordViewModel, ChangePasswordViewModel>();

            regionManager.RegisterViewWithRegion("UserCardRegion", typeof(UserCardView));
        }
    }
}
