﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    public enum AppointmentCategories
    {
        [Description("Drugo")]
        Other = 0,
        [Description("Poziv obavljen")]
        Called,
        [Description("Poziv primljen")]
        GetCall,
        [Description("Nedostupan")]
        NoCall,
        [Description("Isporuka")]
        Delivery,
        [Description("Uzorci")]
        Samples,
        [Description("Povrat")]
        Return
    }
}
