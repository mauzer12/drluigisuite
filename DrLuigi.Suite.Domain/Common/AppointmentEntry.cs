﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    public class AppointmentEntry
    {
        public virtual string Id { get; protected set; }

        public virtual string Subject { get; set; }

        public virtual string Body { get; set; }

        public virtual DateTime StartDate { get; set; }

        public virtual DateTime EndDate { get; set; }

        public virtual bool IsImportant { get; set; }

        public virtual AppointmentCategories Category { get; set; }

        public AppointmentEntry()
        {
        }

        public AppointmentEntry(string id)
        {
            Id = id;
        }
    }
}
