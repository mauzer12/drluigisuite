﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    /// <summary>
    /// City entity.
    /// </summary>
    public class City
    {
        /// <summary>
        /// Gets or sets city identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets city name.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets post number.
        /// </summary>
        public virtual string PostNumber { get; set; }

        /// <summary>
        /// Gets or sets country to which city belongs.
        /// </summary>
        public virtual Country Country { get; set; }

        /// <summary>
        /// Gets companies located in specific city.
        /// </summary>
        public virtual IList<Company> Companies { get; protected set; }

        /// <summary>
        /// Gets partners located in specific city.
        /// </summary>
        public virtual IList<Partner> Partners { get; protected set; }

        public City()
        {
            Companies = new List<Company>();
        }
    }
}
