﻿using System.Collections.Generic;

namespace DrLuigi.Suite.Domain
{
    /// <summary>
    /// Company entity.
    /// </summary>
    public class Company
    {
        /// <summary>
        /// Gets company identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets company unique number. [OIB]
        /// </summary>
        public virtual string CompanyNumber { get; set; }

        /// <summary>
        /// Gets or sets company name.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets company email address.
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// Gets or sets contact phone.
        /// </summary>
        public virtual string Phone { get; set; }

        /// <summary>
        /// Gets or sets fax number.
        /// </summary>
        public virtual string FAX { get; set; }

        /// <summary>
        /// Gets or sets address line.
        /// </summary>
        public virtual string AddressLine { get; set; }

        /// <summary>
        /// Gets or sets address line city.
        /// </summary>
        public virtual City City { get; set; }

        /// <summary>
        /// Gets employees by company.
        /// </summary>
        public virtual IList<Employee> Employees { get; protected set; }

        public virtual IList<Reimbursement> Reimbursements { get; protected set; }

        public virtual IList<Receipt> Receipts { get; protected set; }

        public Company()
        {
            Reimbursements = new List<Reimbursement>();
            Receipts = new List<Receipt>();
            Employees = new List<Employee>();
        }
    }
}
