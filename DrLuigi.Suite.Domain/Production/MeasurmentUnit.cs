﻿using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    public class MeasurmentUnit
    {
        /// <summary>
        /// Gets measurment unit identifier.
        /// </summary>
        public virtual int Id { get; protected set; }
        [System.ComponentModel.DataAnnotations.StringLength(1, ErrorMessage = "Naziv m jed mora biti kraći od 1 slova.")]
        /// <summary>
        /// Gets or sets measurment unit name.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets value bound to specific measurment unit increment steep.
        /// </summary>
        public virtual decimal Step { get; set; }

        /// <summary>
        /// Gets materials measurd in specific unit.
        /// </summary>
        public virtual IList<Material> Materials { get; protected set; }

        public virtual bool AddItem(Material m)
        {
            Materials.Add(m);
            if (m != null)
            {
                m.SetMeasurmentUnit(this);
                return true;
            }
            return false;
        }

        public virtual bool RemoveItem(Material m)
        {
            if (m != null &&
            Materials.Remove(m))
            {
                m.SetMeasurmentUnit(null);
                return true;
            }
            return false;
        }
    }
}
