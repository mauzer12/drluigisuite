﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    /// <summary>
    /// Receipt entity.
    /// </summary>
    public class Receipt
    {
        /// <summary>
        /// Gets receipt identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DateTime CreationDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int BusinessYear
        {
            get { return CreationDate.Year; }
        }

        public virtual string ReceiptNumber { get; set; }

        public virtual Company Company { get; set; }

        public virtual Partner Partner { get; set; }

        public virtual IList<ReceiptItem> ReceiptItems { get; protected set; }

        public Receipt()
        {
            ReceiptItems = new List<ReceiptItem>();
        }
    }
}
