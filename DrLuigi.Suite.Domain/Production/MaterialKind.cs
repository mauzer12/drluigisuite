﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    /// <summary>
    /// Material kind entity. General group of material used on product declaration to describe product composition.
    /// In production concrete material is coosen based on aviability and stock amount.
    /// </summary>
    public class MaterialKind
    {
        /// <summary>
        /// Gets material kind identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets material kind code used to partialy qualify product (model).
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Gets or sets name of material kind.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets materials that can be used in production to satisfy needs declared by product.
        /// </summary>
        public virtual IList<Material> Materials { get; protected set; }

        public MaterialKind()
        {
            Materials = new List<Material>();
        }
    }
}
