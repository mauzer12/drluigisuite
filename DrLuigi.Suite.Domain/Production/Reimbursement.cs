﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    /// <summary>
    /// Reimbursement entity.
    /// </summary>
    public class Reimbursement
    {
        /// <summary>
        /// Gets reimbursement idenfifier.
        /// </summary>
        public virtual int Id { get; protected set; }
        
        /// <summary>
        /// Gets or sets date of creation of this document.
        /// </summary>
        public virtual DateTime CreationDate { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual int BusinessYear
        {
            get { return CreationDate.Year; }
        }

        public virtual string ReimbursementNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual Company Company { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual Partner Partner { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual IList<ReimbursementItem> ReimbursementItems { get; protected set; }

        public Reimbursement()
        {
            ReimbursementItems = new List<ReimbursementItem>();
        }
    }
}
