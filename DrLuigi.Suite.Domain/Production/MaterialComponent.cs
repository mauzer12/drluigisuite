﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    /// <summary>
    /// NOT mapped in NHibernate, just symbolicaly added for diagram drawing purposes. 
    /// </summary>
    public class MaterialComponent
    {
        /// <summary>
        /// Gets or sets entity identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets material on left side of many to many relationship
        /// </summary>
        public virtual Material Material1 { get; set; }

        /// <summary>
        /// Gets or sets material on right side of many to many relationship
        /// </summary>
        public virtual Material Material2 { get; set; }
    }
}
