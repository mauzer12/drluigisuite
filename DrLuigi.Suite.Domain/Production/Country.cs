﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    /// <summary>
    /// Country entity.
    /// </summary>
    public class Country
    {
        /// <summary>
        /// Gets country identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets country code.
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Gets or sets country name.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets flag to determine is country part of EU.
        /// </summary>
        public virtual bool? IsPartOfEU { get; set; }

        /// <summary>
        /// Gets or sets cities used for specific country.
        /// </summary>
        public virtual IList<City> Cities { get; protected set; }

        public Country()
        {
            Cities = new List<City>();
        }
    }
}
