﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    public class ReimbursementItem
    {
        public virtual int Id { get; protected set; }

        public virtual decimal Amount { get; set; }

        public virtual decimal Rebate { get; set; }

        public virtual Reimbursement Reimbursement { get; set; }

        public virtual Material Material { get; set; }
    }
}
