﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    public class ExpenseType
    {
        /// <summary>
        /// Gets Fee type identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets Fee type name.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets all materials by specific expense type.
        /// </summary>
        public virtual IList<Material> Materials { get; protected set; }

        public ExpenseType()
        {
            Materials = new List<Material>();
        }
    }
}
