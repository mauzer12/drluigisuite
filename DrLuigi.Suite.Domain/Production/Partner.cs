﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    public class Partner
    {
        public virtual int Id { get; protected set; }

        public virtual string Name { get; set; }

        public virtual string OIB { get; set; }

        public virtual string TaxNumber { get; set; }

        public virtual string AddressLine { get; set; }
        
        public virtual double? Longitude { get; set; }

        public virtual double? Latitude { get; set; }

        public virtual string ContactName { get; set; }

        public virtual string Phone { get; set; }

        public virtual string FAX { get; set; }

        public virtual string Email { get; set; }

        public virtual string Web { get; set; }

        public virtual City City { get; set; }

        public virtual Country Country
        {
            get { return City.Country; }
        }

        public virtual IList<Reimbursement> Reimbursements { get; protected set; }

        public virtual IList<Receipt> Receipts { get; protected set; }

        public Partner()
        {
            Reimbursements = new List<Reimbursement>();
            Receipts = new List<Receipt>();
        }
    }
}
