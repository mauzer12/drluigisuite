﻿using System.Collections.Generic;

namespace DrLuigi.Suite.Domain
{
    public class MaterialCategory
    {
        /// <summary>
        /// Gets material category identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets material group bottom limit.
        /// </summary>
        public virtual int MinValue { get; set; }

        /// <summary>
        /// Gets or sets material group upper limit.
        /// </summary>
        public virtual int MaxValue { get; set; }

        /// <summary>
        /// Gets or sets material category name.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets collection of groups by category.
        /// </summary>
        public virtual IList<MaterialGroup> Groups { get; protected set; }

        public MaterialCategory()
        {
            Groups = new List<MaterialGroup>();
        }

        public MaterialCategory(int id)
            : this()
        {
            Id = id;
        }
    }
}
