﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    public class MaterialGroup
    {
        /// <summary>
        /// Gets or sets material group identifier
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets material group unique code. [old pkey]
        /// </summary>
        public virtual int Code { get; set; }

        /// <summary>
        /// Gets or sets material group name.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets material group category.
        /// </summary>
        public virtual MaterialCategory Category { get; set; }

        /// <summary>
        /// Gets materials by specified group.
        /// </summary>
        public virtual IList<Material> Materials { get; protected set; }

        public virtual bool AddItem(Material m)
        {
            Materials.Add(m);
            if (m != null)
            {
                m.SetGroup(this);
                return true;
            }
            return false;
        }

        public virtual bool RemoveItem(Material m)
        {
            if (m != null &&
            Materials.Remove(m))
            {
                m.SetGroup(null);
                return true;
            }
            return false;
        }

        public MaterialGroup()
        {
            Materials = new List<Material>();
        }
    }
}
