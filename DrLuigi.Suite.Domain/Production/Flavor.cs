﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    public class Flavor
    {
        /// <summary>
        /// Gets product flavor identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets product flavor name.   DEPRICATED!
        /// </summary>
        public virtual string Name { get; set; } 

        /// <summary>
        /// Gets sole color code.
        /// </summary>
        public virtual string SoleColorCode
        {
            get { return SoleColor.Code; }
        }

        /// <summary>
        /// Gets face color code.
        /// </summary>
        public virtual string FaceColorCode
        {
            get { return FaceColor.Code; }
        }

        /// <summary>
        /// Gets or sets product sole color.
        /// </summary>
        public virtual Color SoleColor { get; set; }

        /// <summary>
        /// Gets or sets product face color.
        /// </summary>
        public virtual Color FaceColor { get; set; }

        /// <summary>
        /// Gets all product flavors per flavor.
        /// </summary>
        public virtual IList<ProductFlavor> ProductFlavors { get; protected set; }
    }
}
