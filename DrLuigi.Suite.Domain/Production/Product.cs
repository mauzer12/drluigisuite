﻿using System.Collections.Generic;

namespace DrLuigi.Suite.Domain
{
    public class Product
    {
        /// <summary>
        /// Gets product identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets product catalog name.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets production status flag.
        /// </summary>
        public virtual bool? IsActive { get; set; }

        /// <summary>
        /// Gets or sets product flavors per product.
        /// </summary>
        public virtual IList<ProductFlavor> ProductFlavors { get; protected set; }

        //public IList<ProductSize> ProductsPerSize { get; set; }
    }
}