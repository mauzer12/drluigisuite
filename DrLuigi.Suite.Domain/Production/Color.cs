﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Domain
{
    public class Color
    {
        /// <summary>
        /// Gets color identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets color code. IDENTITY NUMBER VARCHAR(2)
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Gets or sets hex RGB value - without # sign.
        /// </summary>
        public virtual string RGB { get; set; }

        /// <summary>
        /// Gets or sets standard name of color.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets all materials by color.
        /// </summary>
        public virtual IList<Material> Materials { get; protected set; }

        /// <summary>
        /// Gets all flavors by sole color.
        /// </summary>
        public virtual IList<Flavor> SoleFlavors { get; protected set; }

        /// <summary>
        /// Gets all flavors by face color.
        /// </summary>
        public virtual IList<Flavor> FaceFlavors { get; protected set; }

        public virtual bool AddMaterial(Material m)
        {
            Materials.Add(m);
            if (m != null)
            {
                m.SetColor(this);
                return true;
            }
            return false;
        }

        public virtual bool RemoveMaterial(Material m)
        {
            if (m != null &&
            Materials.Remove(m))
            {
                m.SetColor(null);
                return true;
            }
            return false;
        }

        public Color()
        {
            Materials = new List<Material>();
        }

        public Color(string code)
            :this()
        {
            Code = code;
        }
    }
}
