﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Domain
{
    public class Employee
    {
        /// <summary>
        /// Gets employee identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets employee firstname.
        /// </summary>
        public virtual string Firstname { get; set; }

        /// <summary>
        /// Gets or sets employee lastname.
        /// </summary>
        public virtual string Lastname { get; set; }

        /// <summary>
        /// Gets or sets employee company.
        /// </summary>
        public virtual Company Company { get; set; }
    }
}
