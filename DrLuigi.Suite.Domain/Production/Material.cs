﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Domain
{
    public class Material
    {
        /// <summary>
        /// Gets material identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets material fully qualified name.
        /// </summary>
        [System.ComponentModel.DataAnnotations.StringLength(3, ErrorMessage = "Naziv materijala mora biti kraći od 3 slova.")]
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets material unique code used internally by company 
        /// to distinguish materials. (old pk  in table 'dbo.materijal')
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Gets or sets material stock info.
        /// </summary>
        public virtual decimal StockQuantity { get; set; }

        /// <summary>
        /// Gets or sets material measurment unit.
        /// </summary>
        public virtual MeasurmentUnit MeasurmentUnit { get; set; }

        /// <summary>
        /// Gets or sets expence type ( expense category :P) of material.
        /// </summary>
        public virtual ExpenseType ExpenseType { get; set; }

        /// <summary>
        /// Gets or sets material group.
        /// </summary>
        public virtual MaterialGroup Group { get; set; }

        /// <summary>
        /// Gets or sets material kind. This tells where this concrete material can be used in production.
        /// </summary>
        public virtual MaterialKind Kind { get; set; }
        
        /// <summary>
        /// Gets material category.
        /// </summary>
        public virtual MaterialCategory Category
        {
            get { return (Group != null) ? Group.Category : null; }
        }

        /// <summary>
        /// Gets or sets material color.
        /// </summary>
        public virtual Color Color { get; set; }

        /// <summary>
        /// Gets materials this material is composed of.
        /// </summary>
        public virtual IList<Material> ComposedOf { get; protected set; }

        /// <summary>
        /// Gets materials that specific material composing.
        /// </summary>
        public virtual IList<Material> ContainedIn { get; protected set; }

        /// <summary>
        /// Gets all receipt items in which's this material is referenced.
        /// </summary>
        public virtual IList<ReceiptItem> ReceiptItems { get; protected set; }

        /// <summary>
        /// Gets all reimbursement items in which's this material is referenced.
        /// </summary>
        public virtual IList<ReimbursementItem> ReimbursementItems { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        public virtual void AddComposedOf(Material m)
        {
            ComposedOf.Add(m);
            m.ContainedIn.Add(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        public virtual void AddContainedIn(Material m)
        {
            ContainedIn.Add(m);
            m.ComposedOf.Add(this);
        }

        public virtual void SetMeasurmentUnit(MeasurmentUnit newMu)
        {
            var prevMu = MeasurmentUnit;
            if (newMu == prevMu)
                return;
            MeasurmentUnit = newMu;
            if (prevMu != null)
                prevMu.RemoveItem(this);
            if (newMu != null)
                newMu.AddItem(this);
        }

        public virtual void SetGroup(MaterialGroup newGroup)
        {
            var prevGroup = Group;
            if (newGroup == prevGroup)
                return;
            Group = newGroup;
            if (prevGroup != null)
                prevGroup.RemoveItem(this);
            if (newGroup != null)
                newGroup.AddItem(this);
        }

        public virtual void SetColor(Color newColor)
        {
            var prevColor = Color;
            if (newColor == prevColor)
                return;
            Color = newColor;
            if (prevColor != null)
                prevColor.RemoveMaterial(this);
            if (newColor != null)
                newColor.AddMaterial(this);
        }

        public Material()
        {
            ReceiptItems = new List<ReceiptItem>();
            ReimbursementItems = new List<ReimbursementItem>();
            ComposedOf = new List<Material>();
            ContainedIn = new List<Material>();
        }
    }
}
