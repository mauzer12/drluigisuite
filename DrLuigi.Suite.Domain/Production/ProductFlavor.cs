﻿namespace DrLuigi.Suite.Domain
{
    public class ProductFlavor
    {
        /// <summary>
        /// Gets product flavor map identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets product side map.
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        /// Gets or sets flavor side of map.
        /// </summary>
        public virtual Flavor Flavor { get; set; }

        /// <summary>
        /// Gets or sets image of product flavor.
        /// </summary>
        public virtual byte[] Image { get; set; }


        //TODO: Add logopress.
    }
}
