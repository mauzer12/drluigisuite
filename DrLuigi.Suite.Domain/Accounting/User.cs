﻿using System;

namespace DrLuigi.Suite.Domain
{
    
    // Convention:  default je user razina prava
    //              0 - user
    //              1 - superuser
    //              2 - admin
    /// <summary>
    /// User account entity containing user credentials.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Gets user unique identifier.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets user unique username.
        /// </summary>
        public virtual string Username { get; set; }

        /// <summary>
        /// Gets or sets user password.
        /// </summary>
        public virtual string Password { get; set; }

        /// <summary>
        /// Gets or sets user salt.
        /// </summary>
        public virtual string Salt { get; set; }

        /// <summary>
        /// Gets or sets user access level.
        /// </summary>
        public virtual byte Role { get; set; }

        /// <summary>
        /// Gets or sets user profile image.
        /// </summary>
        public virtual byte[] Image { get; set; }

        /// <summary>
        /// Gets or sets user firstname.
        /// </summary>
        public virtual string Firstname { get; set; }

        /// <summary>
        /// Gets or sets user lastname.
        /// </summary>
        public virtual string Lastname { get; set; }

        /// <summary>
        /// Gets or sets flag determining is user signed in.
        /// </summary>
        public virtual bool? SignedIn { get; set; }

        /// <summary>
        /// Gets or sets user email.
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// Gets or sets user contact phone.
        /// </summary>
        public virtual string Phone { get; set; }

        /// <summary>
        /// Gets or sets user last successful sign in.
        /// </summary>
        public virtual DateTime? LastSignIn { get; set; }

        /// <summary>
        /// Gets or sets date and time of user account activation - first use.
        /// </summary>
        public virtual DateTime? ActivationDate { get; set; }
    }
}
