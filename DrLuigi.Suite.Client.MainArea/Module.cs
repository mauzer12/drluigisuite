﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using DrLuigi.Suite.Client.Authentication;
using DrLuigi.Suite.Client.GeneralModule;
using DrLuigi.Suite.Client.ProductionModule;
using DrLuigi.Suite.Client.Common.Shared;
using DrLuigi.Suite.Client.GlobalServices.AppManagement;
using DrLuigi.Suite.Client.AdminModule;

namespace DrLuigi.Suite.Client.MainArea
{
    public class Module : IModule
    {
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;

        public Module(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            container.RegisterType<IMainAreaViewModel, MainAreaViewModel>(
                new ContainerControlledLifetimeManager());
            
            container.RegisterType<object, MaterialsThreeView>("MaterialsThreeView");
            container.RegisterType<IMaterialsThreeViewModel, MaterialsThreeViewModel>();
            container.RegisterType<IMaterialEditViewModel, MaterialEditViewModel>();

            container.RegisterType<ISuplierDateEvidenceViewModel, SuplierDateEvidenceViewModel>();

            container.RegisterType<IMaterialOffersViewModel, MaterialOffersViewModel>();
            container.RegisterType<IMaterialOrderViewModel, MaterialOrderViewModel>();

            container.RegisterType<object, ProductsThreeViewModel>("ProductsThreeViewModel");
            container.RegisterType<IProductsThreeViewModel, ProductsThreeViewModel>();
            container.RegisterType<IProductEditViewModel, ProductEditViewModel>();

            container.RegisterType<object, CompaniesThreeViewModel>("CompaniesThreeViewModel");
            container.RegisterType<ICompaniesThreeViewModel, CompaniesThreeViewModel>();
            container.RegisterType<ICompanyEditViewModel, CompanyEditViewModel>();

            container.RegisterType<IEncriptionProvider, SHA256EncriptionProvider>();
            container.RegisterType<IUserRegistrationService, UserRegistrationService>();
            container.RegisterType<IAuthenticationService, AuthenticationService>();
            container.RegisterType<ISigninViewModel, SigninViewModel>();

            container.RegisterType<IUserRegistrationViewModel, UserRegistrationViewModel>();
            container.RegisterType<IUsersThreeViewModel, UsersThreeViewModel>();

            container.RegisterType<ILocationViewModel, LocationViewModel>();
            container.RegisterType<ISupplierDetailsViewModel, SupplierDetailsViewModel>();

            /*container.RegisterType<IApplicationService, ApplicationService>(); //register instance on app level - remove globalservices
            container.RegisterType<IClientChannelFactory<TestService.ITestServiceChannel>,
                        ClientChannelFactory<TestService.ITestServiceChannel>>(
                            new InjectionConstructor("BasicHttpBinding_ITestService"));*/

            regionManager.RegisterViewWithRegion("MainAreaRegion", typeof(MainAreaView));
        }
    }
}
