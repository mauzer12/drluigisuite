﻿using DrLuigi.Suite.Infrastructure;
using System.Linq;
using DrLuigi.Suite.Client.Authentication;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Regions;

namespace DrLuigi.Suite.Client.MainArea
{
    public class MainAreaViewModel : ViewModel, IMainAreaViewModel
    {
        private readonly IDialogService dialogService;
        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;

        private IViewModel vm;

        public MainAreaViewModel(GeneralModule.ICompaniesThreeViewModel vm,  IDialogService dialogService, 
                                 IEventAggregator eventAggregator, IRegionManager regionManager)
        {
            this.vm = vm;

            this.dialogService = dialogService;
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;

            eventAggregator.GetEvent<UserSignedout>().Subscribe(OnUserSignout);
            eventAggregator.GetEvent<UserSignedin>().Subscribe(OnUserSignin);

            showSigninDialog();
        }

        public IViewModel ViewModel
        {
            get { return GetValue(() => ViewModel); }
            private set { SetValue(value, () => ViewModel); }
        }

        protected void OnUserSignin(UserInfo userInfo)
        {
            ViewModel = vm;
        }

        protected void OnUserSignout()
        {
            ViewModel = null;
            IRegion region = regionManager.Regions.Where(r => r.Name == "MainAreaRegion").FirstOrDefault();

            if (region != null)
            {
                foreach (var view in region.Views)
                    region.Remove(view);
            }

            showSigninDialog();
        }

        private void showSigninDialog()
        {
            dialogService.ShowDialogView<ISigninViewModel>("Prijava korisnika", "noCRM", null, null, OwnerEffects.None);
        }

        public void OnNavigatedTo(NavigationContext navigationContext) {}

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext) {}

        public bool KeepAlive
        {
            get
            {
                return false;
            }
        }

        public override void Dispose()
        {
            eventAggregator.GetEvent<UserSignedin>().Unsubscribe(OnUserSignin);
            eventAggregator.GetEvent<UserSignedout>().Unsubscribe(OnUserSignout);

            base.Dispose();
        }
    }
}
