﻿using DrLuigi.Suite.Infrastructure;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.MainArea
{
    public interface IMainAreaViewModel : IViewModel, INavigationAware, IRegionMemberLifetime
    {
        IViewModel ViewModel { get; }
    }
}
