﻿using DrLuigi.Suite.Client.Authentication;
using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.AdminModule
{
    public interface IUsersThreeViewModel : IViewModel
    {
        ObservableCollection<UserInfo> UserInfosCollection { get; set; }
        UserInfo SelectedUserInfo { get; set; }
        string SearchPhrase { get; set; }
        bool IsBusy { get; set; }
        ICommand NewUserCommand { get; }
        ICommand RemoveUserCommand { get; }
    }
}
