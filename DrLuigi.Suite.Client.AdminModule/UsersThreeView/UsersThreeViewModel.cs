﻿using DrLuigi.Suite.Client.Authentication;
using DrLuigi.Suite.DAL;
using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.AdminModule
{
    public class UsersThreeViewModel : ViewModel, IUsersThreeViewModel
    {
        private readonly IRepository repository;
        private readonly IDialogService dialogService;

        public UsersThreeViewModel(IRepository repo, IDialogService dialogService)
        {
            this.repository = repo;
            this.dialogService = dialogService;

            reloadUsersExecute();
        }

        public override string Title
        {
            get
            {
                return "Korisnici";
            }
        }

        public ObservableCollection<UserInfo> UserInfosCollection
        {
            get { return GetValue(() => UserInfosCollection); }
            set { SetValue(value, () => UserInfosCollection); }
        }

        public UserInfo SelectedUserInfo
        {
            get { return GetValue(() => SelectedUserInfo); }
            set { SetValue(value, () => SelectedUserInfo); }
        }

        public int Test
        {
            get { return GetValue(() => Test); }
            set { SetValue(value, () => Test); }
        }

        public string SearchPhrase
        {
            get { return GetValue(() => SearchPhrase); }
            set { SetValue(value, () => SearchPhrase, OnChanged: OnSearchPhraseChanged); }
        }

        protected void OnSearchPhraseChanged()
        {
            if (!string.IsNullOrWhiteSpace(SearchPhrase))
            {
                // Filter data by id, firstname, lastname, username or email
            }
        }

        public bool IsBusy
        {
            get { return GetValue(() => IsBusy); }
            set { SetValue(value, () => IsBusy); }
        }

        private ICommand newUserCommand;
        public ICommand NewUserCommand
        {
            get
            {
                if (newUserCommand == null)
                {
                    newUserCommand = new RelayCommand(() => newUserExecute());
                }

                return newUserCommand;
            }
        }

        private ICommand removeUserCommand;
        public ICommand RemoveUserCommand
        {
            get
            {
                if (removeUserCommand == null)
                {
                    removeUserCommand = new RelayCommand(
                        () => removeUserExecute(),
                        () => SelectedUserInfo != null);
                }

                return removeUserCommand;
            }
        }

        private ICommand reloadUsersCommand;
        public ICommand ReloadUsersCommand
        {
            get
            {
                if (reloadUsersCommand == null)
                {
                    reloadUsersCommand = new RelayCommand(
                        () => reloadUsersExecute(),
                        () => !IsBusy);
                }
                return reloadUsersCommand;
            }
        }

        private ICommand saveUserRightsCommand;
        public ICommand SaveUserRightsCommand
        {
            get
            {
                if (saveUserRightsCommand == null)
                {
                    saveUserRightsCommand = new RelayCommand(() => saveUserRightsExecute());
                }

                return saveUserRightsCommand;
            }
        }

        private void reloadUsersExecute()
        {
            IsBusy = true;

            var users = Task.Factory.StartNew(() =>
            {
                return repository.GetAll<User>().ToList().Select(x => (UserInfo)x);
            }).Result;

            UserInfosCollection = new ObservableCollection<UserInfo>(users);

            IsBusy = false;
        }

        private void newUserExecute()
        {
            dialogService.ShowDialogView<IUserRegistrationViewModel>(
                "default", null,
                onDialogClosing: (dialog, resultVM, e) =>
                {
                    if (dialog.DialogResult.HasValue && dialog.DialogResult.Value)
                    {
                        try
                        {
                            repository.Add<User>(resultVM.User);
                            UserInfosCollection.Add((UserInfo)resultVM.User);
                            SelectedUserInfo = (UserInfo)resultVM.User;
                        }
                        catch (TransactionFailedException)
                        {
                            // report concurrency problem
                            dialogService.ShowMessageBox("Nastao je problem pri dodavanju korisničkog računa u bazu. Postoji mogućnost da je isti korisnički račun u međuvremenu dodao neki drugi korisnik ili isti već postoji u bazi", "Greška",
                                System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error); 
                        }
                    }
                });
        }

        private void removeUserExecute()
        {
            try
            {
                var selectedUser = repository.Get<User>(SelectedUserInfo.Id);

                repository.Remove<User>(selectedUser);
                UserInfosCollection.Remove(SelectedUserInfo);
            }
            catch (TransactionFailedException)
            {
                // report concurrency problem
                dialogService.ShowMessageBox("Nastao je problem pri brisanju korisničkog računa iz baze. Postoji mogućnost da je korisnički račun u međuvremenu obrisao neki drugi korisnik ili korisnički račun ne postoji u bazi. Savjet: osvježite prikaz podataka.", "Greška",
                    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
        }

        private void saveUserRightsExecute()
        {
            throw new NotImplementedException();
        }
    }
}
