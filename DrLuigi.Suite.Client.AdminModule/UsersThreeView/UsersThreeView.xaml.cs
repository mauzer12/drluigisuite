﻿using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace DrLuigi.Suite.Client.AdminModule
{
    /// <summary>
    /// Interaction logic for UsersThreeView.xaml
    /// </summary>
    public partial class UsersThreeView : UserControl
    {
        public UsersThreeView()
        {
            InitializeComponent();
            this.Loaded += UsersThreeView_Loaded;
        }

        private void UsersThreeView_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (RadTreeViewItem item in UserRightsThree.Items)
            {
                //item.ExpandAll();
                item.CheckState = System.Windows.Automation.ToggleState.On;
            }
        }
    }
}
