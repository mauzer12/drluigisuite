﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrLuigi.Suite.Infrastructure;
using DrLuigi.Suite.Domain;

namespace DrLuigi.Suite.Client.AdminModule
{
    public class UserRegistrationViewModel : ViewModel, IUserRegistrationViewModel
    {
        private IUserRegistrationService userRegService;

        public UserRegistrationViewModel(IUserRegistrationService userRegService)
        {
            this.userRegService = userRegService;
        }

        public User User
        {
            get { return GetValue(() => User); }
            set { SetValue(value, () => User); }
        }
    }
}
