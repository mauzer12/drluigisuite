﻿using DrLuigi.Suite.Domain;
using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Client.AdminModule
{
    public interface IUserRegistrationViewModel : IViewModel
    {
        User User { get; set; }
    }
}
