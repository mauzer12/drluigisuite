﻿using DrLuigi.Suite.Client.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.AdminModule
{
    public interface IUserRegistrationService
    {
        Task<bool> RegisterUserAsync(UserInfo user, string password);
    }
}
