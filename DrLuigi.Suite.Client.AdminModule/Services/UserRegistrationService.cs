﻿using DrLuigi.Suite.Client.Authentication;
using DrLuigi.Suite.Client.GlobalServices;
using DrLuigi.Suite.DAL;
using DrLuigi.Suite.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.AdminModule
{
    public class UserRegistrationService : IUserRegistrationService
    {
        private readonly IEncriptionProvider encriptionProvider;
        private readonly IInternetConnectionService internetService;
        private readonly IRepository repository;

        public UserRegistrationService(IRepository repo,
                                       IEncriptionProvider encriptionProvider, 
                                       IInternetConnectionService internetService)
        {
            this.encriptionProvider = encriptionProvider;
            this.internetService = internetService;
            this.repository = repo;
        }

        public async Task<bool> RegisterUserAsync(UserInfo user, string password)
        {
            if (user == null)
                throw new Exception("Invalid or incomplite user data provided.");

            if (!IsUsernameUnique(user.Username))
                throw new Exception("Username " + user.Username + " already exists. try anather name");

            byte[] salt = generateSalt(32);
            byte[] plainText = Encoding.ASCII.GetBytes(password);

            try
            {
                User acc = new User
                {
                    Role = user.Role,
                    Username = user.Username,
                    Firstname = user.Firstname,
                    Lastname = user.Lastname,
                    Password = encriptionProvider.GetHash(plainText, salt),
                    Salt = Encoding.ASCII.GetString(salt),
                    Email = user.Email,
                    Phone = user.Phone,
                    ActivationDate = DateTime.Now
                };

                repository.Add<User>(acc);

                return true;
            }
            catch (Exception)
            {
                bool hasConn = await internetService.HasInternetConnectionAsync();

                if (!hasConn)
                    throw new InternetConnectionException();

                return false;
            }
        }

        /// <summary>
        /// Chack is username already exists in user database.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public bool IsUsernameUnique(string username)
        {
            return !repository.GetAll<User>().Where(x => x.Username == username).Any();
        }

        /// <summary>
        /// Helper method that simply create rand paswd salt.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private byte[] generateSalt(int length)
        {
            byte[] salt = new byte[length];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(salt);
            }

            return salt;
        }
    }
}
