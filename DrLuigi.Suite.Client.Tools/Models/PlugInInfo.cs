﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Client.Tools
{
    public class PlugInInfo
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
