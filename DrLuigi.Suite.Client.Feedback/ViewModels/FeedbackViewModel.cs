﻿using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.Feedback
{
    public class FeedbackViewModel : ViewModel, IFeedbackViewModel, IDialogAware
    {
        private readonly IDialogService dialogService;

        public FeedbackViewModel(IDialogService dialogService)
        {
            this.dialogService = dialogService;
        }

        public void OnDialogInitialized(NavigationParameters p)
        {
            Screenshot = p["Screenshot"] as byte[];
            IncludeScreenshot = (bool) p["IncludeScreenshot"];
        }

        private string message;
        public string Message
        {
            get { return GetValue(() => Message); }
            set { SetValue(value, () => Message); }
        }

        private bool includeScreenshot;
        public bool IncludeScreenshot
        {
            get { return GetValue(() => IncludeScreenshot); }
            set { SetValue(value, () => IncludeScreenshot); }
        }

        private byte[] screenshot;
        public byte[] Screenshot 
        {
            get { return GetValue(() => Screenshot); }
            private set { SetValue(value, () => Screenshot); }
        }
    }
}
