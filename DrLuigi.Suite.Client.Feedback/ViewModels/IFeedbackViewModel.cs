﻿using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.Feedback
{
    public interface IFeedbackViewModel : IViewModel, IDialogAware
    {
        string Message { get; set; }
        bool IncludeScreenshot { get; set; }
        byte[] Screenshot { get; }
    }
}
