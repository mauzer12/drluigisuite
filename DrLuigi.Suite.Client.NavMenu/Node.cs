﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Client.NavMenu
{
    public class Node
    {
        public string Name { get; set; }
        public List<Node> Childs { get; protected set; }

        public Node(string name)
        {
            Name = name;
            Childs = new List<Node>();
        }
    }
}
