﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;

namespace DrLuigi.Suite.Client.NavMenu
{
    public class Module : IModule
    {
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;

        public Module(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            container.RegisterType<INavigationMenuViewModel, NavigationMenuViewModel>(
                new ContainerControlledLifetimeManager());

            regionManager.RegisterViewWithRegion("NavigationRegion", typeof(NavigationMenuView));
        }
    }
}
