﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace DrLuigi.Suite.Client.NavMenu
{
    /// <summary>
    /// Interaction logic for NavigationMenuView.xaml
    /// </summary>
    public partial class NavigationMenuView : UserControl
    {
        public NavigationMenuView(INavigationMenuViewModel vm)
        {
            InitializeComponent();
            this.DataContext = vm;
        }

        private void RadPanelBarItem_Selected(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {

            markParentItemsSelected((sender as RadPanelBarItem).ParentItem);
        }

        private void markParentItemsSelected(RadPanelBarItem item)
        {
            if (item != null)
            {
                item.IsSelected = true;
                markParentItemsSelected(item.ParentItem);
            }
        }
    }
}
