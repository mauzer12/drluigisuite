﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Resources;
using System.Xml;
using System.Xml.Linq;

namespace DrLuigi.Suite.Client.NavMenu
{
    public class LinqToXml : List<Node>
    {
        public LinqToXml()
        {
            string resource = "UserPermissions.xml";
            AssemblyName assemblyName = new AssemblyName(typeof(LinqToXml).Assembly.FullName);
            string resourcePath = "/" + assemblyName.Name + ";component/" + resource;
            Uri resourceUri = new Uri(resourcePath, UriKind.Relative);
            StreamResourceInfo resourceInfo = Application.GetResourceStream(resourceUri);
            XmlReader reader = XmlReader.Create(resourceInfo.Stream);
            XDocument doc = XDocument.Load(reader);

            AddRange(GetChild(doc.Element("Definitions")));
        }

        private List<Node> GetChild(XElement xElement)
        {
            List<Node> lEntity = new List<Node>();

            foreach (XElement child in xElement.Elements())
            {
                Node entity = new Node(child.Attribute("Name").Value);
                entity.Childs.AddRange(GetChild(child));

                lEntity.Add(entity);
            }

            return lEntity;
        }
    }
}
