﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.Regions;
using System.Windows.Input;
using DrLuigi.Suite.Infrastructure;
using DrLuigi.Suite.Client.Authentication;
using Microsoft.Practices.Prism.Events;

namespace DrLuigi.Suite.Client.NavMenu
{
    public class NavigationMenuViewModel : ViewModel, INavigationMenuViewModel
    {
        private readonly IRegionManager regionManager;
        private readonly IEventAggregator eventAggregator;

        public NavigationMenuViewModel(IRegionManager regionManager, IEventAggregator eventAggregator)
        {
            this.regionManager = regionManager;
            this.eventAggregator = eventAggregator;

            eventAggregator.GetEvent<UserSignedin>().Subscribe(OnUserSignin);
        }

        protected void OnUserSignin(UserInfo userInfo)
        {
            IRegion region = regionManager.Regions.Where(r => r.Name == "NavigationRegion").FirstOrDefault();

            if (region != null && !region.Views.Any())
                region.Add(typeof(NavigationMenuView));
        }

        private ICommand navigateCommand;
        public ICommand NavigateCommand
        {
            get
            {
                if (navigateCommand == null)
                {
                    navigateCommand = new RelayCommand<string>((s) => navigateExecute(s));
                }

                return navigateCommand;
            }
        }

        private void navigateExecute(string s)
        {
            int i = 0;
            if (!int.TryParse(s, out i))
                return;

            Uri uri = null;
            switch (i)
            {
                case 0:
                default:
                    uri = new Uri("CompaniesThreeView", UriKind.Relative);
                    break;
                case 1:
                    uri = new Uri("MaterialsThreeView", UriKind.Relative);
                    break;
                case 2:
                    uri = new Uri("ProductsThreeView", UriKind.Relative);
                    break;
                case 3:
                    uri = new Uri("MaterialPurchasePlanerThreeView", UriKind.Relative);
                    break;
                case 4:
                    uri = new Uri("UsersThreeView", UriKind.Relative);
                    break;
            }

            // let the world know navigation in 'MainArea' region occured.
            eventAggregator.GetEvent<NavigationEvent>().Publish(uri);

            regionManager.RequestNavigate("WorkspaceRegion", uri);
        }

        public override void Dispose()
        {
            eventAggregator.GetEvent<UserSignedin>().Unsubscribe(OnUserSignin);

            base.Dispose();
        }
    }
}
