﻿using DrLuigi.Suite.Infrastructure;
using Microsoft.Practices.Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.MainToolbar
{
    public class MainToolbarViewModel : ViewModel, IMainToolbarViewModel
    {
        private readonly IEventAggregator eventAggregator;

        private const int navigationBuffCapacity = 5;

        public MainToolbarViewModel(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;

            eventAggregator.GetEvent<NavigationEvent>().Subscribe(OnMainAreaNavigated);
        }

        private void OnMainAreaNavigated(Uri uri)
        {
            // TODO: create ObservableQueue data structure instead of notifying two collections from vm.

            if (navigationHistory.Any() && uri == navigationHistory.Peek())
                return;

            navigationHistory.Push(uri);
            SetValue(navigationHistory.ToList(), () => NavigationHistoryRecent);
            SetValue(navigationHistory.Reverse().ToList(), () => NavigationHistoryOldest);
        }

        private Stack<Uri> navigationHistory = new Stack<Uri>(navigationBuffCapacity);

        public List<Uri> NavigationHistoryRecent
        {
            get { return GetValue(() => NavigationHistoryRecent); }
            private set { SetValue(value, () => NavigationHistoryRecent); }
        }

        public List<Uri> NavigationHistoryOldest
        {
            get { return GetValue(() => NavigationHistoryOldest); }
            private set { SetValue(value, () => NavigationHistoryOldest); }
        }

        private ICommand navigateHistoryBackCommand;
        private ICommand navigateHistoryForwardCommand;

        public ICommand NavigateHistoryBackCommand
        {
            get
            {
                if (navigateHistoryBackCommand == null)
                {
                    navigateHistoryBackCommand = new RelayCommand(
                        () => { },
                        () => NavigationHistoryRecent != null && NavigationHistoryRecent.Any());
                }

                return navigateHistoryBackCommand;
            }
        }

        public ICommand NavigateHistoryForwardCommand
        {
            get
            {
                if (navigateHistoryForwardCommand == null)
                {
                    navigateHistoryForwardCommand = new RelayCommand(
                        () => { },
                        () => NavigationHistoryOldest != null && NavigationHistoryOldest.Any());
                }

                return navigateHistoryForwardCommand;
            }
        }

        private ICommand undoCommand;
        public ICommand UndoCommand
        {
            get
            {
                if (undoCommand == null)
                {
                    undoCommand = new RelayCommand(
                        () => { },
                        () => false);
                }

                return undoCommand;
            }
        }

        private ICommand redoCommand;
        public ICommand RedoCommand
        {
            get
            {
                if (redoCommand == null)
                {
                    redoCommand = new RelayCommand(
                        () => { },
                        () => false);
                }

                return redoCommand;
            }
        }

        public override void Dispose()
        {
            eventAggregator.GetEvent<NavigationEvent>().Unsubscribe(OnMainAreaNavigated);

            base.Dispose();
        }
    }
}
