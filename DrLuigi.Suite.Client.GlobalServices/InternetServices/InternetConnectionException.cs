﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.Client.GlobalServices
{
    public class InternetConnectionException : Exception
    {
        public InternetConnectionException()
        {
        }

        public InternetConnectionException(string message)
            : base(message)
        {
        }

        public InternetConnectionException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
