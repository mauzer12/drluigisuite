﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.GlobalServices
{
    public interface IInternetConnectionService
    {
        Task<bool> HasInternetConnectionAsync();
    }
}
