﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.GlobalServices
{
    public class InternetConnectionService : IInternetConnectionService
    {
        public Task<bool> HasInternetConnectionAsync()
        {
            Ping ping = new Ping();
            IPAddress host = new IPAddress(new byte[] { 8, 8, 8, 8 }); // ping Google public DNS IP address.
            byte[] buffer = new byte[32];
            int timeout = 1000;
            PingOptions pingOptions = new PingOptions();

            return Task<bool>.Factory.StartNew(() => 
            {
                PingReply reply = ping.Send(host, timeout, buffer, pingOptions);

                if (reply.Status == IPStatus.Success)
                {
                    return true;
                }

                return false;
            });
        }
    }
}
