﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.GlobalServices
{
    public interface IMailService
    {
        Task SendMailAsync(string from, string to, string subject, string body);
        Task SendMailAsync(MailMessage mail);

        Task SendMailWithAttachmentAsync(string from, string to, string subject, string body, 
            byte[] attachment, string name, string mediaType);
    }
}
