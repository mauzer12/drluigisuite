﻿using DrLuigi.Suite.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.GlobalServices
{
    public class YahooMailService : IMailService
    {
        private readonly IInternetConnectionService internetService;

        public YahooMailService(IInternetConnectionService internetConnService)
        {
            this.internetService = internetConnService;
        }

        public Task SendMailAsync(string from, string to, string subject, string body)
        {
            MailMessage mail = new MailMessage(from, to);
            mail.Subject = subject;
            mail.Body = body;

            return SendMailAsync(mail);
        }

        public async Task SendMailAsync(MailMessage mail)
        {
            SmtpClient client = new SmtpClient
            {
                Port = 587,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                EnableSsl = true,
                Host = "smtp.mail.yahoo.com"
            };

            client.Credentials = new System.Net.NetworkCredential("gorankozar@yahoo.com", "qsyi.54N");

            try
            {
                await Task.Factory.StartNew(() =>
                {
                    client.Send(mail);
                });
            }
            catch (Exception ex)
            {
                bool hasConn = await internetService.HasInternetConnectionAsync();

                if (!hasConn)
                    throw new InternetConnectionException();

                throw ex;
            }
        }

        public Task SendMailWithAttachmentAsync(string from, string to, string subject, string body, 
            byte[] attachment, string name, string contentType)
        {
            MailMessage mail = new MailMessage(from, to);
            mail.Subject = subject;
            mail.Body = body;

            if (attachment != null)
            {
                // TODO: attachment name and contentType needs better validation mechanism for more general use!
                //       Here I just simplified things.
                name = name ?? "attachment";
                contentType = contentType ?? MediaTypeNames.Application.Octet;
                
                Attachment attach = new Attachment(new MemoryStream(attachment), name, contentType);
                ContentDisposition disposition = attach.ContentDisposition;
                disposition.DispositionType = DispositionTypeNames.Attachment;
                mail.Attachments.Add(attach);
            }

            return SendMailAsync(mail);
        }
    }
}
