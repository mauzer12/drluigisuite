﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace DrLuigi.Suite.Client.GlobalServices.AppManagement
{
    public class ApplicationService : IApplicationService
    {
        public void ExitApplication()
        {
            Environment.Exit(0);
        }
    }
}
