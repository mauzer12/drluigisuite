﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.GlobalServices
{
    public interface IImageProcessor
    {
        byte[] ImageToByteArray(string path);
        Image ByteArrayToImage(byte[] byteArrayIn);
    }
}
