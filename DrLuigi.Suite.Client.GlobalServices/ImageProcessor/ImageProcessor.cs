﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.Client.GlobalServices
{
    public class ImageProcessor : IImageProcessor
    {
        public byte[] ImageToByteArray(string path)
        {
            Image img = Image.FromFile(path);
            byte[] arr;

            using (MemoryStream ms = new MemoryStream())
            {
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                arr = ms.ToArray();
            }

            return arr;
        }

        public Image ByteArrayToImage(byte[] byteArrayIn)
        {
            Image returnImage = null;

            using (MemoryStream ms = new MemoryStream(byteArrayIn))
            {
                returnImage = Image.FromStream(ms);
            }

            return returnImage;
        }
    }
}
