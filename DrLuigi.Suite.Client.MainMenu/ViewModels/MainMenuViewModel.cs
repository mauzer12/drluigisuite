﻿using DrLuigi.Suite.Client.GlobalServices;
using DrLuigi.Suite.Client.Help;
using DrLuigi.Suite.Infrastructure;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using System.Windows.Input;
using System;
using DrLuigi.Suite.Client.Tools;
using DrLuigi.Suite.Client.GlobalServices.AppManagement;

namespace DrLuigi.Suite.Client.MainMenu
{
    public class MainMenuViewModel : ViewModel, IMainMenuViewModel
    {
        private readonly IDialogService dialogService;
        private readonly IScreenCaptureService screenCaptureService;
        private readonly IApplicationService appService;

        public MainMenuViewModel(IDialogService dialogService, 
                                 IApplicationService appService,
                                 IScreenCaptureService screenCaptureService)
        {
            this.dialogService = dialogService;
            this.screenCaptureService = screenCaptureService;
            this.appService = appService;
        }

        private ICommand openPlugInManagerCommand;
        public ICommand OpenPlugInManagerCommand
        {
            get
            {
                if (openPlugInManagerCommand == null)
                {
                    openPlugInManagerCommand = new RelayCommand(() => openPluginManagerExecute());
                }
                return openPlugInManagerCommand;
            }
        }

        private ICommand feedbackCommand;
        public ICommand FeedbackCommand
        {
            get 
            {
                if (feedbackCommand == null)
                {
                    feedbackCommand = new DelegateCommand(
                        () => OnFeedbackExecute());
                }

                return feedbackCommand;
            }
        }

        private ICommand logoutCommand;
        public ICommand LogoutCommand
        {
            get 
            {
                if (logoutCommand == null)
                { 
                    logoutCommand = new DelegateCommand(
                        () => OnLogoutExecute(),    
                        () => true);
                }

                return logoutCommand;
            }
        }

        private ICommand exitCommand;
        public ICommand ExitCommand
        {
            get 
            {
                if (exitCommand == null)
                {
                    exitCommand = new DelegateCommand(
                        () => appService.ExitApplication());
                }

                return exitCommand;
            }
        }

        private void openPluginManagerExecute()
        {
            dialogService.ShowDialogView<IPlugInManagerViewModel>(
                "Plug-in upravitelj", "default", null,
                onDialogClosing: (dialog, resultVM, e) =>
                {
                    // TODO: Try to save.
                });
        }

        protected void OnFeedbackExecute()
        {
            // Capture screenshot before dialog init.
            byte[] screenCapture = screenCaptureService.CaptureScreen();

            NavigationParameters parameters = new NavigationParameters
            {
                {"Screenshot", screenCapture},
                {"IncludeScreenshot", true }
            };

            dialogService.ShowDialogView<IFeedbackViewModel>("Povratna informacija", "default", parameters,
                onDialogClosing: (dialog, resultVM, e) =>
                {
                    if (dialog.DialogResult.HasValue && dialog.DialogResult.Value)
                    {
                        // TODO: push notification to user that mail was sent.
                    }
                });
        }

        protected void OnLogoutExecute()
        {
        }
    }
}
