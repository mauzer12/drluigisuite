﻿using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DrLuigi.Suite.Client.MainMenu
{
    public interface IMainMenuViewModel
    {
        ICommand FeedbackCommand { get; }
        ICommand LogoutCommand { get; }
        ICommand ExitCommand { get; }
    }
}
