﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DrLuigi.Suite.Infrastructure;
using DrLuigi.Suite.Client.Help;

namespace DrLuigi.Suite.Client.MainMenu
{
    /// <summary>
    /// Interaction logic for MainMenuView.xaml
    /// </summary>
    public partial class MainMenuView : UserControl
    {
        public MainMenuView(IMainMenuViewModel vm)
        {
            InitializeComponent();
            this.DataContext = vm;
        }

        private void AboutMenuItem_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            BasicDialogShell dlgShell = new BasicDialogShell();
            dlgShell.Content = new AboutView();
            dlgShell.ShowDialog();
        }
    }
}
