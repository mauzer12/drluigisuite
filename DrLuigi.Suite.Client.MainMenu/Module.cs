﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using DrLuigi.Suite.Client.Help;
using DrLuigi.Suite.Client.GlobalServices;
using DrLuigi.Suite.Client.Tools;

namespace DrLuigi.Suite.Client.MainMenu
{
    public class Module : IModule
    {
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;

        public Module(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            container.RegisterType<IMainMenuViewModel, MainMenuViewModel>(
                new ContainerControlledLifetimeManager());

            container.RegisterType<IFeedbackViewModel, FeedbackViewModel>();
            container.RegisterInstance<IScreenCaptureService>(new ScreenCaptureService());
            container.RegisterType<IInternetConnectionService, InternetConnectionService>();
            container.RegisterType<IMailService, YahooMailService>();

            container.RegisterType<IPlugInManagerViewModel, PlugInManagerViewModel>();

            regionManager.RegisterViewWithRegion("MainMenuRegion", typeof(MainMenuView));
        }
    }
}
