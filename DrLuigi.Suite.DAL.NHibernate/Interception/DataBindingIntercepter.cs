﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.DAL.NHibernate
{
    public class DataBindingIntercepter : EmptyInterceptor
    {
        public ISessionFactory SessionFactory { set; get; }

        public override object Instantiate(string clazz, EntityMode entityMode, object id)
        {
            if (entityMode == EntityMode.Poco)
            {
                // clazz needs to be fully qualified assembly name. This interceptor is bound to DrLuigi.Suite.Domain entities.
                // Further you can make it more flexible.
                string fullyQualifiedName = 
                    clazz + ", " + System.Reflection.Assembly.GetAssembly(typeof(Domain.User)).ToString();

                Type type = Type.GetType(fullyQualifiedName);

                if (type != null)
                {
                    var instance = DataBindingFactory.Create(type);
                    SessionFactory.GetClassMetadata(clazz).SetIdentifier(instance, id, entityMode);
                    return instance;
                }
            }

            return base.Instantiate(clazz, entityMode, id);
        }

        /*public override object Instantiate(string clazz, EntityMode entityMode, object id)
        {
            if (entityMode == EntityMode.Poco)
            {
                string fullyQualifiedName =
                    clazz + ", " + System.Reflection.Assembly.GetAssembly(typeof(Domain.User)).ToString();

                Type clazzType = Type.GetType(fullyQualifiedName);

                if (clazzType != null)
                {
                    var instance = DataBindingFactory.Create(clazzType);

                    PropertyInfo[] propertyInfos = clazzType.GetProperties(BindingFlags.Public)
                                                            .Where(p => p.GetGetMethod().IsVirtual)
                                                            .ToArray();
                
                    foreach (PropertyInfo propertyInfo in propertyInfos)
                    {
                        Type ptype = propertyInfo.GetType();

                        if (propertyInfo.CanWrite && ptype != null)
                        {
                            var pInstance = DataBindingFactory.Create(ptype);
                            SessionFactory.GetClassMetadata(clazz).SetPropertyValue(instance, propertyInfo.Name, pInstance, entityMode);
                            //propertyInfo.SetValue(instance, pInstance, null);
                        }
                    }

                    SessionFactory.GetClassMetadata(clazz).SetIdentifier(instance, id, entityMode);
                    return instance;
                }
            }

            return base.Instantiate(clazz, entityMode, id);
        }*/

        public override string GetEntityName(object entity)
        {
            var markerInterface = entity as DataBindingFactory.IMarkerInterface;

            if (markerInterface != null)
                return markerInterface.TypeName;

            return base.GetEntityName(entity);
        }
    }
}
