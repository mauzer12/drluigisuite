﻿using NHibernate.Linq;
using System;
using System.Linq;

namespace DrLuigi.Suite.DAL.NHibernate
{
    public class Repository : IRepository
    {
        //private readonly IInternetConnectionService internetService;

        public Repository(IUnitOfWork uow) //IInternetConnectionService internetConnService
        {
            this.UoW = uow;
            //this.internetService = internetConnService;
        }

        public IUnitOfWork UoW { get; private set; }

        public TEntity Get<TEntity>(object id, bool stateless = false) where TEntity : class
        {
            if (id == null)
                throw new ArgumentNullException("Identifier not provided as parameter to function.");

            if (stateless)
                return (UoW as UnitOfWork).StatelessSession.Get<TEntity>(id);
            else
                return (UoW as UnitOfWork).Session.Get<TEntity>(id);
        }

        public IQueryable<TEntity> GetAll<TEntity>(bool stateless = false) where TEntity : class
        {
            if (stateless)
                return (UoW as UnitOfWork).StatelessSession.Query<TEntity>();
            else
                return (UoW as UnitOfWork).Session.Query<TEntity>();
        }

        public void Add<TEntity>(TEntity entity, bool stateless = false) where TEntity : class
        {
            if (entity == null)
                throw new ArgumentNullException("Entity not provided as parameter to function.");

            try
            {
                UoW.BeginTransaction();

                if (stateless)
                    (UoW as UnitOfWork).StatelessSession.Insert(entity);
                else
                {
                    UoW.Add<TEntity>(entity);
                }

                UoW.CommitTransaction();
            }
            catch (Exception ex)
            {
                UoW.RollbackTransaction();
                throw new TransactionFailedException("", ex);
            }
        }

        public void Update<TEntity>(TEntity entity, bool stateless = false) where TEntity : class
        {
            if (entity == null)
                throw new ArgumentNullException("Entity not provided as parameter to function.");

            try
            {
                UoW.BeginTransaction();

                if (stateless)
                    (UoW as UnitOfWork).StatelessSession.Update(entity);
                else
                {
                    UoW.Update<TEntity>(entity);
                }

                UoW.CommitTransaction();
            }
            catch (Exception ex)
            {
                UoW.RollbackTransaction();

                /*bool hasConn = internetService.HasInternetConnectionAsync().Result;

                if (!hasConn)
                    throw new InternetConnectionException("Niste spojeni na internet!\n\nProvjerite mrežnu opremu i u slučaju da se kvar ne otkloni obavjestite Vašeg internet operatera.");*/

                throw new TransactionFailedException("", ex);
            }
        }

        public void Remove<TEntity>(TEntity entity, bool stateless = false) where TEntity : class
        {
            if (entity == null)
                throw new ArgumentNullException("Entity not provided as parameter to function.");

            try
            {
                UoW.BeginTransaction();

                if (stateless)
                    (UoW as UnitOfWork).StatelessSession.Delete(entity);
                else
                {
                    UoW.Delete(entity);
                }

                UoW.CommitTransaction();
            }
            catch (Exception ex)
            {
                UoW.RollbackTransaction();
                throw new TransactionFailedException("", ex);
            }
        }
    }
}
