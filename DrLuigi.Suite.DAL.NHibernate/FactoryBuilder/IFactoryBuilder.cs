﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.DAL.NHibernate
{
    public interface IFactoryBuilder
    {
        ISessionFactory SessionFactory { get; }
        event EventHandler<EventArgs> DbSchemaCreated;
    }
}
