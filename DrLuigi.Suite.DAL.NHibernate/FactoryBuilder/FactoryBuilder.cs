﻿using DrLuigi.Suite.DAL.NHibernate.Conventions;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System;

namespace DrLuigi.Suite.DAL.NHibernate
{
    /// <summary>
    /// Wrapper around NHibernate SessionFactory functionality.
    /// </summary>
    public class FactoryBuilder : IFactoryBuilder
    {
        private ISessionFactory sessionFactory;
        private string connString;
        private bool recreateDb;

        public FactoryBuilder(string connString, bool recreateDb = false)
        {
            this.connString = connString;
            this.recreateDb = recreateDb;

            // TODO: Catch exception on sessionFactory.
            sessionFactory = createSessionFactory();
        }

        public event EventHandler<EventArgs> DbSchemaCreated;

        protected void OnDbSchemaCreated()
        {
            if (DbSchemaCreated != null)
                DbSchemaCreated(this, EventArgs.Empty);
        }

        public ISessionFactory SessionFactory 
        { 
            get { return sessionFactory; } 
        }

        protected ISessionFactory createSessionFactory()
        {
            var interceptor = new DataBindingIntercepter();

            // Fluently configure NHibernate.
            ISessionFactory sessionFactory = Fluently.Configure()
                    .Database(MsSqlConfiguration.MsSql2008
                    .ConnectionString(c => c.FromConnectionStringWithKey(connString))
                    .AdoNetBatchSize(100)
                 )
                .Mappings(m =>
                {
                    m.FluentMappings.AddFromAssembly(System.Reflection.Assembly.GetExecutingAssembly())
                                    .Conventions.Add<CustomForeignKeyConvention>();
                })
                .ExposeConfiguration(cfg =>
                {
#if DEBUG   
                    // double check permission to recreate db schema.
                    if (recreateDb)
                    {
                        new SchemaExport(cfg).Create(true, true);
                        OnDbSchemaCreated();
                    }
#endif
                    cfg.SetInterceptor(interceptor);
                })
                .BuildSessionFactory();

            interceptor.SessionFactory = sessionFactory;
            
            return sessionFactory;
        }
    }
}
