﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    internal class AppointmentEntryMap : ClassMap<AppointmentEntry>
    {
        public AppointmentEntryMap()
        {
            Id(x => x.Id);
            Map(x => x.Subject).Length(50);
            Map(x => x.Body).Length(255);
            Map(x => x.StartDate).Not.Nullable();
            Map(x => x.EndDate).Not.Nullable();
            Map(x => x.IsImportant);
            Map(x => x.Category).CustomType<int>();          // not nullable.
        }
    }
}
