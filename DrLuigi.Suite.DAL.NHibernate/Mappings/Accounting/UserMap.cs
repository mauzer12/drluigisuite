﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    internal class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Schema("Acc");
            Id(x => x.Id);
            NaturalId().Not.ReadOnly().Property(x => x.Username);   // db index //TODO: set map length to 30.
            Map(x => x.Password).Not.Nullable();
            Map(x => x.Salt).Not.Nullable();
            Map(x => x.Role).Not.Nullable();
            Map(x => x.Image);
            Map(x => x.Firstname).Length(30).Not.Nullable();
            Map(x => x.Lastname).Length(50).Not.Nullable();
            Map(x => x.SignedIn);
            Map(x => x.Email).Not.Nullable();                       // (255) max length. 
            Map(x => x.Phone).Length(20);
            Map(x => x.LastSignIn);
            Map(x => x.ActivationDate);
        }
    }
}
