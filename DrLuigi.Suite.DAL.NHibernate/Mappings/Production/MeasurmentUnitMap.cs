﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class MeasurmentUnitMap : ClassMap<MeasurmentUnit>
    {
        public MeasurmentUnitMap()
        {
            Schema("Prod");
            Id(x => x.Id);
            NaturalId().Not.ReadOnly().Property(x => x.Name);
            Map(x => x.Step);
            HasMany(x => x.Materials).Inverse();
        }
    }
}
