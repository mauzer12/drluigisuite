﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class ReceiptMap : ClassMap<Receipt>
    {
        public ReceiptMap()
        {
            Schema("Prod");
            Id(x => x.Id);
            Map(x => x.CreationDate).Not.Nullable();
            Map(x => x.ReceiptNumber).Not.Nullable().Unique().Length(11);
            References(x => x.Company).Cascade.All();                        //.Not.Nullable();
            References(x => x.Partner).Cascade.All().Not.Nullable();
            HasMany(x => x.ReceiptItems).Inverse();
        }
    }
}
