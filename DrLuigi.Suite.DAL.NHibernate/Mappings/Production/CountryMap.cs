﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class CountryMap : ClassMap<Country>
    {
        public CountryMap()
        {
            Schema("Prod");
            Id(x => x.Id);
            NaturalId().Not.ReadOnly().Property(x => x.Code);
            Map(x => x.Name).Not.Nullable();
            Map(x => x.IsPartOfEU);
            HasMany(x => x.Cities).Inverse();
        }
    }
}
