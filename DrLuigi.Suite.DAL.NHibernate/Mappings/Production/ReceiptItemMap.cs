﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class ReceiptItemMap : ClassMap<ReceiptItem>
    {
        public ReceiptItemMap()
        {
            Schema("Prod");
            Id(x => x.Id);
            Map(x => x.Amount).CustomSqlType("decimal(9,2)").Not.Nullable().Default("1.0");
            Map(x => x.Rebate).CustomSqlType("decimal(9,2)");
            Map(x => x.UnitPriceKN).CustomSqlType("decimal(13,6)").Not.Nullable();
            Map(x => x.UnitPriceEU).CustomSqlType("decimal(13,6)").Not.Nullable();
            References(x => x.Receipt).Cascade.All().Not.Nullable();
            References(x => x.Material).Cascade.All().Not.Nullable();
        }
    }
}
