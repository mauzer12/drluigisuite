﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using DrLuigi.Suite.Domain;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class ExpenseTypeMap : ClassMap<ExpenseType>
    {
        public ExpenseTypeMap()
        {
            Schema("Prod");
            Id(x => x.Id);
            Map(x => x.Name).Length(40).Not.Nullable();
            HasMany(x => x.Materials);
        }
    }
}
