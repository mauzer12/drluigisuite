﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class CompanyMap : ClassMap<Company>
    {
        public CompanyMap()
        {
            Schema("Prod");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.CompanyNumber).Length(20).Not.Nullable();
            Map(x => x.Name).Length(50).Not.Nullable();
            Map(x => x.Email);
            Map(x => x.Phone).Length(20);
            Map(x => x.FAX).Length(20);
            Map(x => x.AddressLine).Length(60);
            References(x => x.City).Cascade.SaveUpdate();
            HasMany(x => x.Employees);
            HasMany(x => x.Receipts);
            HasMany(x => x.Reimbursements).Inverse();
        }
    }
}
