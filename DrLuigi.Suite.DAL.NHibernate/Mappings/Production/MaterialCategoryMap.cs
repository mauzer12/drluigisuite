﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class MaterialCategoryMap : ClassMap<MaterialCategory>
    {
        public MaterialCategoryMap()
        {
            Schema("Prod");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.MinValue).Unique().Not.Nullable();
            Map(x => x.MaxValue).Unique().Not.Nullable();
            NaturalId().Not.ReadOnly().Property(x => x.Name);
            HasMany(x => x.Groups).Inverse();
        }
    }
}
