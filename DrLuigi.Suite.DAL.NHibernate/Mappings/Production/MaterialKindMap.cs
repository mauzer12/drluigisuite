﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.DAL.NHibernate
{
    public class MaterialKindMap : ClassMap<MaterialKind>
    {
        public MaterialKindMap()
        {
            Schema("Prod");
            Id(x => x.Id);
            NaturalId().Not.ReadOnly().Property(x => x.Code);
            Map(x => x.Name);
            HasMany(x => x.Materials).Inverse();
        }
    }
}
