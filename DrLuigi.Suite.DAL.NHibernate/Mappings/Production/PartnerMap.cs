﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class PartnerMap : ClassMap<Partner>
    {
        public PartnerMap()
        {
            Schema("Prod");
            Id(x => x.Id);
            NaturalId().Not.ReadOnly().Property(x => x.Name);
            Map(x => x.OIB).Length(20);
            Map(x => x.TaxNumber).Length(50);
            Map(x => x.AddressLine).Length(60);
            Map(x => x.Latitude);
            Map(x => x.Longitude);
            Map(x => x.ContactName).Length(50);
            Map(x => x.Email);
            Map(x => x.Phone).Length(30); //20 i omogućiti više kontakata
            Map(x => x.FAX).Length(30); //20 i omogućiti više kontakata
            Map(x => x.Web);
            References(x => x.City).Cascade.All();
            HasMany(x => x.Receipts).Inverse();
            HasMany(x => x.Reimbursements).Inverse();
        }
    }
}
