﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class CityMap : ClassMap<City>
    {
        public CityMap()
        {
            Schema("Prod");
            Id(x => x.Id);
            NaturalId().Not.ReadOnly().Property(x => x.PostNumber);
            Map(x => x.Name).Not.Nullable();
            References(x => x.Country).Cascade.SaveUpdate();
            HasMany(x => x.Companies).Inverse();
            HasMany(x => x.Partners).Inverse();
        }
    }
}
