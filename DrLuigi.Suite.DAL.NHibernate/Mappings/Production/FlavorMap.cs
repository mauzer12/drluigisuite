﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class FlavorMap : ClassMap<Flavor>
    {
        public FlavorMap()
        {
            Schema("Prod");
            Id(x => x.Id);
            Map(x => x.Name).Not.Nullable();
            HasOne(x => x.SoleColor).ForeignKey("SoleColorId");
            HasOne(x => x.FaceColor).ForeignKey("FaceColorId");
            HasMany(x => x.ProductFlavors);
        }
    }
}
