﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class ProductMap : ClassMap<Product>
    {
        public ProductMap()
        {
            Schema("Prod");
            Id(x => x.Id);
            Map(x => x.Name).Length(100).Not.Nullable();
            Map(x => x.IsActive);
            HasMany(x => x.ProductFlavors);
        }
    }
}
