﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class MaterialMap : ClassMap<Material>
    {
        public MaterialMap()
        {
            Schema("Prod");
            Id(x => x.Id);
            NaturalId().Not.ReadOnly().Property(x => x.Code);
            Map(x => x.Name).Length(100).Not.Nullable();
            Map(x => x.StockQuantity).Default("0.00");
            References(x => x.MeasurmentUnit).Cascade.All();                //.Not.Nullable(); -> exception pri brisanju           
            References(x => x.Group).Cascade.All();                         //.Not.Nullable(); -> exception u konverteru
            References(x => x.Kind).Cascade.All();
            References(x => x.Color).Cascade.All();
            HasOne(x => x.ExpenseType);
            HasManyToMany(x => x.ComposedOf)
                .Schema("Prod")
                .Table("MaterialComponent")
                .ParentKeyColumn("Material1Id")
                .ChildKeyColumn("Material2Id")
                .Cascade.All().Inverse();
            HasManyToMany(x => x.ContainedIn)
                .Schema("Prod")
                .Table("MaterialComponent")
                .ParentKeyColumn("Material2Id")
                .ChildKeyColumn("Material1Id")
                .Cascade.All();
        }
    }
}
