﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class ProductFlavorMap : ClassMap<ProductFlavor>
    {
        public ProductFlavorMap() 
        {
            Schema("Prod");
            Id(x => x.Id);
            Map(x => x.Image);
            HasOne(x => x.Product);
            HasOne(x => x.Flavor);
        }
    }
}
