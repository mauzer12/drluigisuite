﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class ColorMap : ClassMap<Color>
    {
        public ColorMap()
        {
            Schema("Prod");
            Id(x => x.Id);
            NaturalId().Not.ReadOnly().Property(x => x.Code);           // Old identity VARCHAR
            Map(x => x.RGB);                                            // NOT NULLABLE CANDIDATE
            Map(x => x.Name).Not.Nullable();
            HasMany(x => x.SoleFlavors);
            HasMany(x => x.FaceFlavors);
            HasMany(x => x.Materials).Inverse();
        }
    }
}
