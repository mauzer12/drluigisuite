﻿using DrLuigi.Suite.Domain;
using FluentNHibernate.Mapping;

namespace DrLuigi.Suite.DAL.NHibernate.Mappings
{
    public class MaterialGroupMap : ClassMap<MaterialGroup>
    {
        public MaterialGroupMap()
        {
            Schema("Prod");
            Id(x => x.Id).GeneratedBy.Identity();
            NaturalId().Not.ReadOnly().Property(x => x.Code);
            Map(x => x.Name).Not.Nullable();
            References(x => x.Category).Cascade.All();
            HasMany(x => x.Materials).Inverse();
        }
    }
}
