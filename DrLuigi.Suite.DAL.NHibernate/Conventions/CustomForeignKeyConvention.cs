﻿using FluentNHibernate;
using FluentNHibernate.Conventions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.DAL.NHibernate.Conventions
{
    internal class CustomForeignKeyConvention : ForeignKeyConvention
    {
        protected override string GetKeyName(Member member, Type type)
        {
            return type.Name + "Id";
        }
    }
}
