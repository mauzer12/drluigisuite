﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.DAL.NHibernate
{
    public class SessionManager : ISessionManager
    {
        private readonly ISessionFactory sessionFactory;
        private Guid id = Guid.NewGuid();

        public SessionManager(IFactoryBuilder builder)
        {
            this.sessionFactory = builder.SessionFactory;
        }

        public Guid Id { get { return id; } }

        private ISession session;
        public ISession Session
        {
            get
            {
                if (session == null)
                    session = sessionFactory.OpenSession();

                return session;
            }
        }

        private IStatelessSession statelessSession;
        public IStatelessSession StatelessSession
        {
            get
            {
                if (statelessSession == null)
                    statelessSession = sessionFactory.OpenStatelessSession();

                return statelessSession;
            }
        }

        // Not in use
        private void ReplaceSessionAfterError()
        {
            if (session != null)
            {
                session.Dispose();
                session = sessionFactory.OpenSession();
            }

            if (statelessSession != null)
            {
                statelessSession.Dispose();
                statelessSession = sessionFactory.OpenStatelessSession();
            }
        }

        public void Dispose()
        {
            if (session != null)
                session.Dispose();

            if (statelessSession != null)
                statelessSession.Dispose();
        }
    }
}
