﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrLuigi.Suite.DAL.NHibernate
{
    public interface ISessionManager : IDisposable
    {
        Guid Id { get; }
        ISession Session { get; }
        IStatelessSession StatelessSession { get; }
    }
}
