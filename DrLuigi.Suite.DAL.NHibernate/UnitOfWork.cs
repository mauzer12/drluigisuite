﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrLuigi.Suite.DAL.NHibernate
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ISessionManager sessionManager;
        private ITransaction tx;
        
        public UnitOfWork(ISessionManager sessionManager)
        {
            this.sessionManager = sessionManager;
        }

        public ISession Session
        {
            get { return sessionManager.Session; }
        }

        public IStatelessSession StatelessSession
        {
            get { return sessionManager.StatelessSession; }
        }

        public void Add<T>(T entity) where T : class
        {
            // mark entity added.
            Session.Save(entity);
        }

        public void Update<T>(T entity) where T : class
        {
            // mark entity as changed.
            Session.SaveOrUpdate(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            // mark entity deleted.
            Session.Delete(entity);
        }

        public void BeginTransaction()
        {
            if (tx == null)
            {
                tx = Session.BeginTransaction();
            }
            else if(tx.IsActive)
            {
                throw new Exception("The transaction is already open.");
            }
        }
        
        public void CommitTransaction()
        {
            if (tx == null)
                throw new Exception("Transaction has not been initialized.");

            tx.Commit();
            tx = null;
        }

        public void RollbackTransaction()
        {
            if (tx == null)
                throw new Exception("The current transaction has not been initialized.");

            tx.Rollback();
            tx = null;
        }
    }
}
